#define _CRT_SECURE_NO_WARNINGS 1
#include "hash.h"


void TestHT1()
{
	int a[] = { 20, 5, 8, 99999, 10, 30, 50 };
	//HashTable<int, int, DefaultHash<int>> ht;
	Hash<int, int> ht;

	if (ht.Find(10))
	{
		cout << "找到了10" << endl;
	}

	for (auto e : a)
	{
		ht.insert(make_pair(e, e));
	}

	// 测试扩容
	ht.insert(make_pair(15, 15));
	ht.insert(make_pair(5, 5));
	ht.insert(make_pair(15, 15));

	if (ht.Find(50))
	{
		cout << "找到了50" << endl;
	}

	if (ht.Find(10))
	{
		cout << "找到了10" << endl;
	}

	ht.erase(10);
	ht.erase(10);

	if (ht.Find(50))
	{
		cout << "找到了50" << endl;
	}

	if (ht.Find(10))
	{
		cout << "找到了10" << endl;
	}
}

void TestHT2()
{
	string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };

	/*string s1("苹果");
	string s2("果苹");
	string s3("果果");
	string s4("萍果");

	string s5("abcd");
	string s6("bcad");
	string s7("aadd");

	StringHash hf;
	cout << hf(s1) << endl;
	cout << hf(s2) << endl;
	cout << hf(s3) << endl;
	cout << hf(s4) << endl << endl;
	cout << hf(s5) << endl;
	cout << hf(s6) << endl;
	cout << hf(s7) << endl;*/


	//HashTable<string, int, StringHash> countHT;
	Hash<string, int> countHT;

	for (auto& str : arr)
	{
		auto ret = countHT.Find(str);
		if (ret)
		{
			ret->_kv.second++;
		}
		else
		{
			countHT.insert(make_pair(str, 1));
		}
	}

	// 对应类型配一个仿函数，仿函数对象实现把key对象转换成映射的整数
	//HashTable<Date, int, DateHash> countHT;
	//HashTable<Student, int, StudentHash> countHT;


	Hash<string, int> copy(countHT);
}
void test2()
{
	Hash<int, int> h;
	h.insert(make_pair(1, 1));
	h.insert(make_pair(2, 2));
	h.insert(make_pair(5, 5));
	h.insert(make_pair(11, 11));
	h.insert(make_pair(12, 12));
	h.insert(make_pair(15, 15));
	h.insert(make_pair(25, 25));
	h.insert(make_pair(16, 16));
}
int main()
{

	TestHT2();


	return 0;
}