#pragma once
#include <iostream>
#include <vector>
using namespace std;

//除留余数法，（直接定址法就不实现了）。
//哈希冲突用线性探测法,2次探测法

enum status { EMPTY, EXIST, DELETE };//三个模式，用状态实现伪删除，不然删除变空之后会找不到后面的了


template <class K,class V>
struct HashNode
{
	pair<K, V> _kv;
	status s = EMPTY;
};

template <class K>
struct Hashdefault
{
	size_t operator()(const K& key)
	{
		return key;
	}
};

template <>//string的特化，把string转换为一个数
struct Hashdefault<string>
{
	size_t operator()(const string& s)
	{
		//return s[0];//首字母ascii可以
		//return (size_t)&s;地址不行，因为一样的string地址也不一样
		size_t sum = 0;
		for (auto& e : s)
		{
			sum = sum * 131 + e;
		}
		return sum;
	}
};

template <class K,class V,class HashFunc = Hashdefault<K>>
class Hash
{
	typedef HashNode< K, V> data;
public:
	Hash() = default;//拷贝构造也是默认，因为没有直接管理资源，vector拷贝调用自己的深拷贝

	bool insert(const pair< K,V>& kv)
	{
		if (Find(kv.first))
		{
			return false;
		}

		//负载因子大于0.7的时候扩容，提前扩容
		if (_v.size() == 0 || _n * 1.0 / _v.size() >= 0.7)
		{
			size_t newsize = _v.size() == 0 ? 10 : _v.size() * 2;
			Hash newHash;
			newHash._v.resize(newsize);
			for (int i = 0; i < _v.size(); i++)//hash慢就慢在扩容之后的映射会变化，故重新插入
			{
				if(_v[i].s==EXIST)
					newHash.insert(_v[i]._kv);//有数才插入
			}
			newHash._v.swap(_v);//指针交换
		}

		//插入
		HashFunc hf;
		//size_t starti = kv.first;//若是能够直接转整型的类型,就可以，其他类型不行就得仿函数获取
		size_t starti = hf(kv.first) % _v.size();
		size_t hashi = starti;//模的是size不是capacity。因为size是初始化过的，能直接访问
		
		//解决hash冲突
		int i = 1;
		while (_v[hashi].s != EMPTY && _v[hashi].s != DELETE)
		{
			//到空或删除就停止，可以插入了,2次探测就是i的平方，正负都行
			hashi = starti + i;
			i++;
			//超过得回来
			hashi = hashi % _v.size();
		}
		_v[hashi]._kv = kv;
		_v[hashi].s = EXIST;
		_n++;

		return true;
	}

	//kv结构，查找支持修改v,返回指针，若是返回引用，无法直接判断没找到
	data* Find(const K& key)
	{
		if (_v.size() == 0)
			return nullptr;
		HashFunc hf;
		size_t starti = hf(key)%_v.size();//这可能除0错误
		size_t hashi = starti;
		size_t i = 1;
		while (_v[hashi].s != EMPTY)//找到空截止，找到delete还得往后找
		{
			if (_v[hashi].s != DELETE && _v[hashi]._kv.first == key)
			{
				return &_v[hashi];
			}
			hashi = starti + i;
			i++;
			hashi %= _v.size();//防止越界
		}
		return nullptr;
	}

	bool erase(const K& key)
	{
		data* node = Find(key);
		if(node!=nullptr)
		{
			_n--;
			node->s = DELETE;
			return true;
		}
		else
			return false;
	}
private:
	vector<data> _v;
	int _n = 0;

};