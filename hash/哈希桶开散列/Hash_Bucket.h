#pragma once
#include <iostream>
#include <vector>
using namespace std;
//hash桶实现，开散列（拉链法）解决哈希冲突，但依然是除留余数法算hash值


template <class K,class V>
struct BucketNode
{
	pair<K,V> _kv;
	BucketNode<K, V>* _next = nullptr;
	BucketNode(const pair<K,V>& kv)
		:_kv(kv)
		,_next(nullptr)
	{}

};

template <class K>
struct Hashdefault
{
	size_t operator()(const K& key)
	{
		return key;
	}
};

template <>//string的特化，把string转换为一个数
struct Hashdefault<string>
{
	size_t operator()(const string& s)
	{
		//return s[0];//首字母ascii可以
		//return (size_t)&s;地址不行，因为一样的string地址也不一样
		size_t sum = 0;
		for (auto& e : s)
		{
			sum = sum * 131 + e;
		}
		return sum;
	}
};

template <class K,class V,class HashFunc=Hashdefault<K> >
struct Bucket 
{
	typedef BucketNode<const K,V> Node;
public :
	bool insert(const pair<K,V>& kv)
	{
		HashFunc hf;
		//哈希桶
		if (_tables.size() == _n)
		{
			//扩容，刚好为0的时候也一起判断了，哈希桶的话一般是负载因子为1扩容的，因为其哈希冲突的影响不大
			size_t newsize = _n == 0 ? 10 : _tables.size() * 2;

			//这里也可以创建一个新的哈希表复用insert依次插入，但是由于这里是节点，移交节点会更好。不用浪费
			vector<Node*> newtables;
			newtables.resize(newsize);
			for (int i = 0; i < _tables.size(); i++)
			{
				if (_tables[i] != nullptr)
				{
					Node* cur = _tables[i];
					while (cur)
					{
						Node* next = cur->_next;
						size_t hashi = hf(cur->_kv.first) % newsize;
						cur->_next = newtables[hashi];
						newtables[hashi] = cur;
						cur = next;
					}
				}
			}
			_tables.swap(newtables);//老的vector会自己析构
		}

		size_t hashi = hf(kv.first) % _tables.size();//扩容后不会有除零错误了
		Node* newNode = new Node(kv);
		newNode->_next = _tables[hashi];
		_tables[hashi] = newNode;
		_n++;
		return true;
	}

	Node* find(const K& key)
	{
		HashFunc hf;
		if (_tables.size() == 0)
		{
			return nullptr;
		}
		//find是通过key来找该节点
		size_t hashi = hf(key) % _tables.size();
		Node* cur = _tables[hashi];
		while (cur)
		{
			if (cur->_kv.first == key)
				return cur;
			else
				cur = cur->_next;
		}
		return nullptr;
	}

	bool erase(const K& key)
	{
		HashFunc hf;
		//前后指针删除，或者替换法删除都可以，前者优先，后者学思维
		if (_tables.size() == 0)
		{
			return false;;
		}
		size_t hashi = hf(key) % _tables.size();
		Node* prev = nullptr;
		Node* cur = _tables[hashi];
		while (cur)
		{
			if (cur->_kv.first == key)
			{
				if (prev == nullptr)
					_tables[hashi] = cur->_next;
				else
					prev->_next = cur->_next;
	
				delete cur;
				_n--;
				return true;
			}
			prev = cur;
			cur = cur->_next;
		}
		return false;

	}
private:
	//vector<Node> _tables;
	vector<Node*> _tables;//指针不会调用自己的构造函数，指针在reszie的模板中初始化为空
	size_t _n = 0;


};
