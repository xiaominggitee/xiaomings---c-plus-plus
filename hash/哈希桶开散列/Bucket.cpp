#define _CRT_SECURE_NO_WARNINGS 1
#include "Hash_Bucket.h"
namespace suzm
{

	void test1()
	{
		Bucket<int, int> b;
		b.insert(make_pair(1, 1));
		//BucketNode<int, int>* a =  BucketNode<int, int>*();注意模板的时候指针是可以这样使用的，默认为空，T x = T()。若为指针默认是空指针
	}
	void test2()
	{
		Bucket<int, int> a;
		a.insert(make_pair(1, 1));
		a.insert(make_pair(2, 2));
		a.insert(make_pair(3, 3));
		a.insert(make_pair(4, 4));
		a.insert(make_pair(5, 5));
		a.insert(make_pair(15, 15));

		a.erase(5);
		a.erase(15);
		a.erase(5);
		BucketNode<const int, int>* temp = a.find(2);//bucket写成const传入没问题，封装map和set要改套一层
		if (temp != nullptr)
		{
			temp->_kv.second = 10;
		}

	}
	void test3()
	{
		string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };


		Bucket<string, int> countHT;

		for (auto& str : arr)
		{
			auto ret = countHT.find(str);
			if (ret)
			{
				ret->_kv.second++;
			}
			else
			{
				countHT.insert(make_pair(str, 1));
			}
		}
	}
}
int main()
{
	suzm::test3();
	return 0;
}