#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
using namespace std;
typedef char BTDatatype;

typedef struct BinaryTreeNode {
	BTDatatype data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
}BTNode;

BTNode* BuyNode(BTDatatype x)
{
	BTNode* temp = (BTNode*)malloc(sizeof(BTNode));
	if (temp == NULL)
	{
		printf("malloc fail\n");
		exit(-1);
	}
	temp->data = x;
	temp->left = temp->right = NULL;
	return temp;
}
// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树
BTNode* BinaryTreeCreate(BTDatatype* a, int n, int* pi)
{
	if (a[*pi] == '#')
	{
		(*pi)++;
		printf("NULL ");
		return NULL;
	}
	else
	{
		BTNode* root = BuyNode(a[(*pi)++]);
		printf("%d ", root->data);
		root->left = BinaryTreeCreate(a, n, pi);
		root->right = BinaryTreeCreate(a, n, pi);
		return root;
	}
	printf("\n");
}