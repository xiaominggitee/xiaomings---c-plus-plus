#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
typedef int(*Func)(int, int);



class person
{
public:
	friend ostream& operator<<(ostream& out, const person& p);
	person(int age)
		:_age(age)
	{}
private:
	int _age;
};

ostream& operator<<(ostream& out, const person& p)
{
	out << p._age << endl;
	return out;
}

int add(int a, int b)
{
	return a + b;
}

int main()
{
	//void* f = add;
	//cout << ((Func)f)(10, 2) << endl;

	cout << person(10) << endl;

	return 0;
}
