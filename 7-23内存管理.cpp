#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
using namespace std;

class stack
{
public:
	//stack直接管理空间，有内置类型，写构造
	stack(int capacity = 10)
	{
		cout << "stack()" << endl;
		_a = new int[capacity];
		_top = 0;
		_capacity = capacity;
	}

	~stack()
	{
		cout << "~stack()" << endl;
		delete[] _a;//单个空间用delete 连续空间还要加一个【】表示数组
		_top = 0;
		_capacity = 0;
	}
private:
	int* _a;
	int _top;
	int _capacity;
};

class MyQueue
{
public:
	//无需构造和拷贝构造，自动生成的默认已经足够
	void Destory()
	{
		//free(this->st1._a);//这里还是私有，访问不了
		cout << " Destory(stack里面的内存_a)" << endl;
	}

private:
	stack st1;
	stack st2;
};

void test6()
{
	//molloc和new的区别
	MyQueue* q1 = (MyQueue*)malloc(sizeof(MyQueue));//malloc只是创造空间，还得自己初始化
	MyQueue* q2 = new MyQueue;//内置类型和malloc一样，new自定义类型会去调用构造函数，构造函数又会调用自定义成员的构造函数，直到初始化完成


	//malloc的释放还需要调用函数去释放里面_a成员malloc的空间之后再去释放q1
	//delete释放会调用析构，有自定义成员还会调用自定义成员的析构，使得里面的也会释放，无需主动调用destory释放

	//Destory(q1->st1);//这里私有无法传参
	q1->Destory();
	q2->Destory();
	free(q1);

	//delete q2;//调用默认构造，默认构造还会自己去调用自定义类型的默认构造，堆里面的对象只能自己释放
	//堆的声明周期不是栈帧控制
}
void test7()
{
	//molloc和new返回值区别
	//malloc失败返回空指针
	void* a = (void*)malloc(1024 * 1024 * 1024);
	cout << a << endl;

	void* b = (void*)malloc(1024 * 1024 * 1024);//返回空指针，故一般要assert判断是否失败
	cout << b << endl;

	void* c = new char[1024 * 1024 * 1024];//失败直接报异常，无需判断
	cout << c << endl;
}

#include<cassert>
void test8()//operator new和operator delete可以自己写专属的，利用内存池提高效率，new在哪个类就写在那，函数调用先找类的，称为定制专属operator new
{
	//new和delete的底层原理，new = operator new +构造函数      delete = operator delete +析构函数

	//operator new是对malloc的封装，加上了一个失败抛异常，底层也是malloc，operator new []底层也是operator new，申请连续空间
	//而new是调用了operator new和类的构造函数的结果，汇编可看出。之所以要用operator new是为了那层封装的报错而不是malloc的返回空指针
	stack* ps1 = (stack*)operator new(sizeof(stack));
	operator delete(ps1);



	stack* ps2 = (stack*)malloc(sizeof(stack));
	assert(ps2);
	free(ps2);//free是_free dbg的宏，而delete是用_free dbg封装的，是为了和operator new匹配，delete也是operator和析构的调用结果

}


void test9()
{
	//定位new，显式使用构造函数,一般不用这么写，特殊场景下采用了别的方式把内存拿出来（如内存池）但是没初始化
	stack* obj = (stack*)operator new(sizeof(stack));
	new(obj)stack(4);//new+指针+构造函数，带不带参数看构造函数

	//stack* obj = (stack*)operator new(sizeof(stack));和malloc基本没有区别，只是失败返回operator new封装了一个报错异常，故需要捕获异常


	
}
//int main()
//{
//	test9();
//
//	//内存泄露，heap leak或者系统资源泄露，是指针丢了，但是资源还在，但是一般进程结束就会归还
//	//但是对于操作系统，服务器等不会关闭的服务进程，内存泄露就会导致系统越来越慢，资源暂用而得不到缓解
//}