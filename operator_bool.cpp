#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

class A
{
public:
	A(int a = 10)
	{
		_a = a;
	}

	explicit operator bool()//无返回值，表调用函数强转,加了explicit可以调用函数根据里面表示判断，但不能直接转去隐式类型转换该值了
	{
		if (_a > 10)
			return false;
		
		else
			return true;
	}
	void print()
	{
		cout << _a << endl;
	}

	operator int()//当然，你用operator int 也能转int，这不是强转，只是用函数实现转。若是加了explicit关键字，那么返回值就不能被赋值了。
	{
		if (_a > 10)
			return 1;
		else
			return 0;
	}

	void set(int a )
	{
		_a = a;
	}
private:
	int _a;
};

int main()
{
	
	A a;
	int x;
	//int y = a;//加了explicit之后不能直接转换了，但是能够用返回值判断，如下面的while a
	while (a)
	{
		cin >> x;
		a.set(x);
		a.print();
	}
	return 0;
}