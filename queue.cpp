#define _CRT_SECURE_NO_WARNINGS 1
#include <deque>
#include <iostream>
using namespace std;
using std::deque;
namespace suzm
{

	template<class T, class container = deque<T>>
	class queue
	{
	public:
		queue()//不用写构造函数
		{}
		void push(const T& x)
		{
			dq.push_back(x);
		}
		void pop()
		{
			dq.pop_front();
		}

		T& front() 
		{
			return dq.front();
		}
		const T& front() const 
		{
			return dq.front();
		}

		int size() const
		{
			return dq.size();
		}
		bool empty() const
		{
			return dq.empty();
		}
		void swap(queue& q)
		{
			dq.swap(q.dq);
		}

	private:
		container dq;//底层用的是deque
	};


}
void test_queue1()
{
	suzm::queue<int> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);

	while (q.size())
	{
		cout << q.front() << " ";
		q.pop();
	}
	cout << endl;
	cout << q.empty() << endl;
	suzm::queue<int> q2;
	q2.push(1);
	q2.push(2);
	q2.push(3);
	q2.push(4);
	q.swap(q2);
}

int main()
{
	test_queue1();
	return 0;
}
