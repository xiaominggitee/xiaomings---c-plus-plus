#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
//浅述继承的概念：基类私有无论什么继承都是无法被访问，其他就是继承方法和基类权限的最小值
//protected权限和private的区别，在普通类没有区别，在继承的基类若是protected类型的话，被公有或保护继承下来才能被派生类使用
//不写继承方法，class默认私有，struct默认公有


class A
{
public:
	A(int a1 = 10, int a2 = 20, int a3 = 30)
		:_a1(a1)
		,_a2(a2)
		,_a3(a3)
	{}
	int _a1;
protected:
	int _a2;
private:
	int _a3;
};

class B :public A
{
public:
	B(int b1 = 40)
		:_b1(b1)
	{}
	void test()
	{
		_a2 = 10;
		//_a3 = 30;//这个无法访问，因为基类中a3是私有的，任何作用域都无法访问到

	}
private:
	int _b1;
};

void test_1()
{
	B b;
	b._a1 = 20;
	//此时在外面只能访问基类是公有的成员。
}



/**************************************************************************分割****************************************************/



//关于公有继承的切割赋值问题
//派生类的对象能够天生被支持赋值给基类的对象，只是吧多余不属于基类的部分类型对象去除，俗称切割或者切片,这是语法支持，没有用到任何类型转换

void test_2()
{
	B b;
	
	A a1 = b;
	A& a2 = b;
	A* pa1 = &b;

	cout << &b << endl;//三个是一样的地址，因为基类是在派生类中是先定义的变量对象，地址一样，但是所涉及的空间不相同，引用是B类中关于A类型对象的引用
	cout << &a2 << endl;
	cout << pa1 << endl;


	//只有公有继承才可以切片赋值，因为公有继承不会涉及权限变化问题，而其他继承可能会影响类型的权限，因为使用的是二者的最小值

	//指针的话是有可能基类指针赋值给派生类的，毕竟对象空间多可以变少，少不能变多，故只能切片，不能反着补充，而指针没有这个问题，但这涉及后面的知识
}
//int main()
//{
//	test_2();
//	return 0;
//}


//设计一个不能被继承的类
//只需要创建一个构造函数是私有的基类即可
//class person
//{
//public:
//	person& init()
//	{
//		_name = "12";
//		_num = 20;
//	}
//private:
//	person(const char* name = " ",int num = 10)//别人继承也无法调用,而自己也无法利用了，也只能通过一个静态成员函数，在堆上new一个给该对象了。方法二，用final，表示不能被继承了
//		:_name(name)
//		,_num(num)
//	{}
//	string _name;
//	int _num;
//};
//class student : public person//无法访问私有成员
//{
//	student()
//		:a(0)
//	{}
//public:
//	int a;
//};
//int main()
//{
//	//student s;
//	//s.a = 10;
//	person 
//}

class person
{
public:
	person(const char* name, int num)//别人继承也无法调用,而自己也无法利用了，也只能通过一个静态成员函数，在堆上new一个给该对象了。方法二，用final，表示不能被继承了
		:_name(name)
		, _num(num)
	{}
	string _name;
	int _num;
};
class student : public person//无法访问私有成员
{
public:
	student()
		:a(0)
		,person("hello",10)//基类没有默认构造的时候可以写出类似调用构造函数的方法显示构造
	{}
public:
	int a;
};


int main()
{
	student s;
	cout << s._name << endl;
	cout << s._num << endl;
	return 0;
}