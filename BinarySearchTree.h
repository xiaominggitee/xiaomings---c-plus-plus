#pragma once
#include <iostream>
using namespace std;

template <class K>
struct BSTreeNode
{
	BSTreeNode(const K& x)
		:_value(x)
		,_left(nullptr)
		,_right(nullptr)
	{}
	K _value;
	BSTreeNode* _left;
	BSTreeNode* _right;
};


//增删查，key版本不能实现改因为一旦改了就可能不再是搜索树了
template <class K>
class BSTree 
{
	typedef BSTreeNode<K> BSNode;
	void InOrder(BSNode* root)
	{
		if (root == nullptr)
			return;
		InOrder(root->_left);
		cout << root->_value << " ";
		InOrder(root->_right);
	}
	BSNode* CopyTree(BSNode* root)
	{
		//前序遍历生成树
		if (root == nullptr)
			return nullptr;
		BSNode* newNode = new BSNode(root->_value);
		//if (_root == nullptr)//不应该直接用_root进行修改，因为_root是直接关联这个树的，下面修改也会导致_root的左右一直更换
		//	_root = newNode;
		//_root->_left = CopyTree(root->_left);
		//_root->_right = CopyTree(root->_right);
		newNode->_left = CopyTree(root->_left);
		newNode->_right = CopyTree(root->_right);
		return newNode;
	}
	void destoryTree(BSNode* root)
	{
		if (root == nullptr)
			return ;
		destoryTree(root->_left);
		destoryTree(root->_right);
		delete root;

	}
public:
	//默认构造就可以了
	//写了拷贝构造就不会有默认构造了，得显示写，或者用default
	BSTree() = default;

	BSTree(const BSTree& bs)
	{
		//这里实现深拷贝
		_root = CopyTree(bs._root);
	}

	BSTree& operator=(const BSTree bs)
	{
		//现代写法不用判断地址是否一样了，因为已经拷贝完成
		swap(_root, bs._root);
		return *this;
	}

	~BSTree()
	{
		//后序析构
		destoryTree(_root);
		_root = nullptr;
	}
	bool Insert(const K key)				//key版本，对于重复的数不能添加。即去重
	{
		BSNode* newNode = new BSNode(key);
		if (_root == nullptr)
		{
			_root = newNode;
		}
		else
		{
			BSNode* parent = nullptr;
			BSNode* cur = _root;
			while (cur)
			{
				if (cur->_value > key)		//左小右大
				{
					parent = cur;
					cur = cur->_left;
				}
				else if (cur->_value < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else
					return false;
			}

			//还需要判断是连接父亲的左孩子还是右孩子
			if (parent->_value > key)
				parent->_left = newNode;
			else
				parent->_right = newNode;
		}
		return true;
	}


	bool Find(const K& key)
	{
		if (_root == nullptr)
			return false;
		BSNode* cur = _root;
		while (cur)
		{
			if (cur->_value == key)
				return true;
			else if (cur->_value > key)
			{
				cur = cur->_left;
			}
			else
				cur = cur->_right;
		}
		return false;
	}


	//删除的话有四种可能型，1.叶子节点，2.有左子树，3.有右子树，4.有左右子树
	bool Erase(const K& key)
	{
		if (_root == nullptr)
			return false;
		BSNode* cur = _root;
		BSNode* parent = nullptr;
		while (cur)
		{
			if (cur->_value > key)		//左小右大
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (cur->_value < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				//进入删除
				/*if (parent == nullptr)
					delete cur;*///这里写错了，无法判断是否有孩子
				if (cur->_left == nullptr)
				{
					if (cur == _root)
					{
						_root = cur->_right;
					}
					else
					{
						if (cur == parent->_left)
							parent->_left = cur->_right;
						else
							parent->_right = cur->_right;
					}
					delete cur;
				}
				else if (cur->_right == nullptr)
				{
					if (cur == _root)
						_root = cur->_left;
					else
					{
						if (cur == parent->_left)
							parent->_left = cur->_left;
						else
							parent->_right = cur->_left;
					}
					delete cur;
				}
				else
				{
					//找cur的左子树中最大的或者右子树中最小的，和cur交换,这个可以解决是root的问题
					BSNode* minright = cur->_right;
					BSNode* minparent = cur;
					while (minright->_left)
					{
						minparent = minright;
						minright = minright->_left;
					}
					swap(cur->_value, minright->_value);


					//删minright，这里不能再次复用Erase删这个单子数或叶子的节点，因为非递归版本都是从_root开始的，不会找到这个节点了，其不是搜索树了
					//这里也得判断下是父亲的左孩子还是右孩子，因为刚开始找的就是最小的，那么是右节点了，其他是左
					if (minright == minparent->_left)
						minparent->_left = minright->_right;
					else
						minparent->_right = minright->_right;
					delete minright;
				} 
				return true;
			}
			
		}//while
		return false;
	}


	//还得来个遍历，中学遍历刚好就是排序，左中右
	void InOrder()
	{
		//由于外面无法直接用私有的_root，所以套一层进行递归，一般类中的递归都是套一层，或者直接get一个私有成员的函数
		InOrder(_root);
		cout << endl;
	}





	//以下是递归写法
	//**********************************************************************************************
	bool InsertR(const K& key)				
	{
		//递归要传递并改变root，所以嵌套一层
		return InsertR(_root, key);
		
	}


	bool FindR(const K& key)
	{
		return FindR(_root, key);
	}


	bool EraseR(const K& key)
	{
		return EraseR(_root, key);
	}

private:
	//递归实现的函数写在私有
	bool InsertR(BSNode*& root, const K& key)//指针的引用j会直接连接parent，也就是上面左孩子或者右孩子的别名
	{
		if (root == nullptr)
		{
			root = new BSNode(key);//创建节点了就成功了，返回
			return true;
		}

		if (root->_value > key)
			InsertR(root->_left, key);
		else if (root->_value < key)
			InsertR(root->_right, key);
		return false;
	}
	bool FindR(BSNode* root, const K& key)
	{
		if (root == nullptr)
			return false;
		if (root->_value > key)
			return FindR(root->_left, key);
		else if (root->_value > key)
			return FindR(root->_right, key);
		else
			return true;
	}
	bool EraseR(BSNode*& root, const K& key)//注意是root的引用，这个是之前的传过来值的引用，代表的就是父亲的左或者右，这就不需要判断了
	{
		//递归只是递归查找而已，找到了也还得普通方法删除
		if (root == nullptr)
			return false;
		if (root->_value > key)
			return EraseR(root->_left, key);
		else if (root->_value < key)
			return EraseR(root->_right, key);
		else
		{
			//找到了，要删除了，也是分四种情况，空，左，右，左右子树

			BSNode* tmp = root;//保留这个节点的内容
			if (root->_left == nullptr)
			{
				root = root->_right;//里面也不需要判断是不是_root了，因为传过来的root也可以是_root的引用
				delete tmp;
			}
			else if (root->_right == nullptr)
			{
				root = root->_left;
				delete tmp;
			}
			else
			{
				//找左边最大的或者右边最小的然后交换即可
				BSNode* cur = root->_right;
				while (cur->_left)
					cur = cur->_left;
				swap(cur->_value, root->_value);

				//非递归版本由于没有root这个用于递归的参数，只是用默认的this，就无法嵌套删除，因为每一次删都是从_root开始的
				EraseR(root->_right, key);
				//交换过来的先前的根，是最小的，而现在所在位置也是其右数最小的那个节点。故其右树是符合二叉搜索树的。整个是不属于的，需要用parent指针找
				//现在的key肯定是只含有右节点或者是叶子的。因为这已经是最左了

			}
			return true;

		}

	}
	BSNode* _root = nullptr;

};