#define _CRT_SECURE_NO_WARNINGS 1
#include "RB_Tree.h"

//检测红黑树,因为插入的规则就是搜索二叉树，所以无论怎么样都是顺序的
//故检查红黑树要测试其几个规则，所有路径黑节点一样，没有连续红

void RB_test1()
{
	const size_t N = 1024;
	vector<int> v;
	v.reserve(N);
	//srand((unsigned)time(0));
	for (size_t i = 0; i < N; ++i)
	{
		v.push_back(rand());
		//v.push_back(i);
	}

	RB_Tree<int, int> t;
	for (auto e : v)
	{
		t.insert(make_pair(e, e));
	}

	//t.levelOrder();
	//cout << endl;
	cout << "高度:" << t.Height() << endl;
	t.InOrder();
	//t.levelOrder();
}

void RB_test2()
{
	//int a[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
	int a[] = { 30,29,28,27,26,25,24,11,8,7,6,5,4,3,2,1 };
	RB_Tree<int, int> t;
	for (auto e : a)
	{
		t.insert(make_pair(e, e));
	}
	t.InOrder();
}

void RB_test3()
{
	const size_t N = 1024*1024;
	vector<int> v;
	v.reserve(N);
	srand(time(0));
	for (size_t i = 0; i < N; ++i)
	{
		v.push_back(rand());
		//v.push_back(i);
	}

	RB_Tree<int, int> t;
	for (auto e : v)
	{
		t.insert(make_pair(e, e));
	}

	//t.levelOrder();
	//cout << endl;
	cout << "是否平衡?" << t.IsBalanceTree() << endl;
	t.Height();

	//t.InOrder();
}
int main()
{
	RB_test3();
	return 0;
}