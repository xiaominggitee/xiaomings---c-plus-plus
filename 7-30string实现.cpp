#define _CRT_SECURE_NO_WARNINGS 1
#include "7-30string实现.h"
//string的实现

void test_string1()
{
	su::string s1 = "nihao";
	su::string s2("hello world");
	su::string s3(s2);


	cout << s2.c_str() << endl;
	cout << s3.c_str() << endl;

	s2[5] += 2;
	s3[5] += 2;
	cout << s2.size() << endl;

	//定位new，对于特殊创建，没有初始化的空间，使用构造函数
	su::string* ps4 = (su::string*)malloc(sizeof(su::string));
	new (ps4) su::string("hello");

	cout << s2.c_str() << endl;
	s2 = s1;
	cout << s2.c_str() << endl;
}



void test_string2()
{
	suzm::string s1("hello");
	s1.reserve(15);
	s1.resize(10, 'x');
	suzm::string s2("world");
	s2.reserve(15);
	s2.reserve(10);



	//比较
	cout << (s1 < s2) << endl;
	cout << (s1 > s2) << endl;
	cout << (s1 == s2) << endl;
}

void test_string3()
{
	suzm::string s1("hello");

	suzm::string s2("world");
	s1.insert(0, 'z');
	s1.insert(1, 'x');
	s1.insert(s1.size(), 'a');
	//s1.insert(11, 'a');

	s2.insert(0, "ni");
	s2.insert(1, "nih");
	s2.insert(s2.size(), "nihao");
	s2.insert(0, s1);
}

void test_string4()
{
	::string s1("https://gitee.com/bithange/class_code/blob/master/class_104/string.h");

	size_t pos = s1.find("://",0);
	if (pos != suzm::string::npos)
	{
		//能找到
		string ps = s1.substr(0, pos);//找子串函数，也是在string里面的
		cout << ps << endl;
	}

}


void test_string5()
{
	suzm::string s1("hello");

	suzm::string s2("world");

	s1.earse(0, 1);
	cout << s1.c_str() << endl;
	s1.earse(s1.size(), 1);//size是下标
	cout << s1.c_str() << endl;

	s2.earse(2);
	cout << s2.c_str() << endl;

}

void test_string6()
{
	suzm::string s1("hello");
	suzm::string s2("world");
	s2 += '\0';
	s2 += '\0';
	s2 += '\0';
	s2 += '\0';
	s2 += '\0';



	cout << s1 << endl;
	cout << s2 << endl;
	s2 += "12";
	cout << s2 << endl;


	cin << s2;
	cout << s2 << endl;


	//std里面有个获取一行字符的函数,这是string的非成员函数，比较函数，以及流插入流提取重载也是非成员函数
	::string s3;
	std::getline(std::cin, s3);
	cout << s3 << endl;

}
int main()
{
	test_string6();

	return 0;
}
