#pragma once
#include <iostream>
using namespace std;



//KV版本，可以连接两个关系不大的对象，用k来查找，但是只能修改V的值

namespace key_value {
	template <class K,class V>
	struct BSTreeNode
	{
		BSTreeNode(const K& x,const V& v)
			:_key(x)
			,_value(v)
			, _left(nullptr)
			, _right(nullptr)
		{}
		K _key;
		V _value;
		
		BSTreeNode<K,V>* _left;
		BSTreeNode<K,V>* _right;
	};


	//增删查，key版本不能实现改因为一旦改了就可能不再是搜索树了
	template <class K,class V>
	class BSTree
	{
		typedef BSTreeNode<K,V> BSNode;

	public:


		bool InsertR(const K& key, const V& value)
		{
			//递归要传递并改变root，所以嵌套一层
			return InsertR(_root, key, value);

		}


		BSNode* FindR(const K& key)
		{
			return FindR(_root, key);
		}


		bool EraseR(const K& key)
		{
			return EraseR(_root, key);
		}
		//还得来个遍历，中学遍历刚好就是排序，左中右
		void InOrder()
		{
			//由于外面无法直接用私有的_root，所以套一层进行递归，一般类中的递归都是套一层，或者直接get一个私有成员的函数
			InOrder(_root);
			cout << endl;
		}
	private:
		void InOrder(BSNode* root)
		{
			if (root == nullptr)
				return;
			InOrder(root->_left);
			cout <<root->_key<<" : "<< root->_value << endl;
			InOrder(root->_right);

		}

		//递归实现的函数写在私有
		bool InsertR(BSNode*& root, const K& key,const V& value)//指针的引用j会直接连接parent，也就是上面左孩子或者右孩子的别名
		{
			if (root == nullptr)
			{
				root = new BSNode(key,value);//创建节点了就成功了，返回
				return true;
			}

			if (root->_key > key)
				InsertR(root->_left, key,value);
			else if (root->_key < key)
				InsertR(root->_right, key,value);
			return false;
		}
		BSNode* FindR(BSNode* root, const K& key)
		{
			if (root == nullptr)
				return nullptr;
			if (root->_key > key)
				return FindR(root->_left, key);
			else if (root->_key < key)
				return FindR(root->_right, key);
			else
				return root;
		}
		bool EraseR(BSNode*& root, const K& key)//删除就依靠key就能删
		{
			//递归只是递归查找而已，找到了也还得普通方法删除
			if (root == nullptr)
				return false;
			if (root->_key > key)
				return EraseR(root->_left, key);
			else if (root->_key < key)
				return EraseR(root->_right, key);
			else
			{
				//找到了，要删除了，也是分四种情况，空，左，右，左右子树

				BSNode* tmp = root;//保留这个节点的内容
				if (root->_left == nullptr)
				{
					root = root->_right;//里面也不需要判断是不是_root了，因为传过来的root也可以是_root的引用
					delete tmp;
				}
				else if (root->_right == nullptr)
				{
					root = root->_left;
					delete tmp;
				}
				else
				{
					//找左边最大的或者右边最小的然后交换即可
					BSNode* cur = root->_right;
					while (cur->_left)
						cur = cur->_left;
					swap(cur->_key, root->_key);

					//非递归版本由于没有root这个用于递归的参数，只是用默认的this，就无法嵌套删除，因为每一次删都是从_root开始的
					EraseR(root->_right, key);
					//交换过来的先前的根，是最小的，而现在所在位置也是其右数最小的那个节点。故其右树是符合二叉搜索树的。整个是不属于的，需要用parent指针找
					//现在的key肯定是只含有右节点或者是叶子的。因为这已经是最左了

				}
				return true;

			}

		}
		BSNode* _root = nullptr;

	};
}