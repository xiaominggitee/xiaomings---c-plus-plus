#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <cassert>
using namespace std;

typedef class stack
{
public:
	stack(int capacity=10)//这个构造函数要写成默认构造，而且只能写了，queue才能调用，构造函数时是实例生成的时候自动调用的
	{
		int* temp = (int*)malloc(sizeof(int) * capacity);
		assert(temp);
		_a = temp;
		_top = 0;
		_capacity = capacity;
	}
	//stack()//一般是写全缺省比较好，但是全缺省和无参数会有歧义。只要有显式的构造函数编译器就不会生成
	//{
	//	_a = nullptr;
	//	_top = _capacity = 0;
	//}

	//析构函数，类似destory，清理内存空间，例如malloc的空间,~stack构造函数的取反,没有参数，没有返回值
	~stack()
	{
		free(_a);
		_a = nullptr;
		//_top = _capacity = 0;
	}
private:
	int* _a;
	int _top;
	int _capacity;
}st;


class queue
{
public://这里不写构造函数的话就会自己生成，而自生成的构造函数不会初始化内置类型，只能初始化自定义类型
	queue()//默认构造函数会自己调用自定义类型的默认构造函数，若自定义类的默认构造是生成的就没什么用
		:st1()
		,st2()
	{}

private:
	st st1;
	stack st2;
};

int main() 
{
	queue Q;
	//queue Q1(Q);//利用Q，拷贝构造一个类型为queue 的Q1对象
	return 0;

}