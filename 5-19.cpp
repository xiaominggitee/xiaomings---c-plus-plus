#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <cassert>
using namespace std;

//六大默认函数都会自己生成，空类里面也包含的
//学习两个方面：1.语法特性：函数名，参数，返回值，什么时候调用，重载
				// （构造函数：可重载，默认有三类，是不需要传参的三类，对象实例化时自动调用，无返回值，参数可缺省）
				// （析构函数：只有一个，无参数无返回值，生命周期结束自己调用，不写都会自己生成）
				// （拷贝构造：是构造函数的重载，传参等拷贝时调用，或自定拷贝。有死循环问题故都是引用原对象拷贝，有且只有一个参数，无返回值）
				
				//2.不写编译器会自动构成，那会做什么？、、、、有自定义类型成员都靠其自己函数，前提是能调用（默认无参）
				//（构造和析构：内置类型不处理，自定义类型的成员才处理（通过调用自定义的默认构造和析构））
				// （拷贝构造：自动生成的对内置类型的成员做值拷贝或浅拷贝，自定义类型成员是调用其自己的拷贝构造函数）
				// 
				
//这些函数都是公有，因为要在别处调用




////构造函数
//class stack
//{
//public:
//	//不写会自动构造一个默认构造函数,默认构造不只是编译器生成才是默认，不用参数的统称默认，有三种，我们不写编译器生成，无参，全缺省
//	stack(int capacity = 10)//隐含了this指针
//	{
//		_a = (int*)malloc(sizeof(int) * capacity);
//		assert(_a);
//		_top = 0;
//		_capacity = capacity;
//	}
//	stack(int capacity )//若不是默认构造，那么queue就无法自动调用，因为它需要参数才可自动调用
//	{
//		_a = (int*)malloc(sizeof(int) * capacity);
//		assert(_a);
//		_top = 0;
//		_capacity = capacity;
//	}
//
//
//private:
//	int* _a;//这只是函数声明，类在对象实例化时才有内存，sizeof只能计算出数据成员的大小，因为成员函数保存在代码区中
//	int _top;
//	int _capacity;
//};

//class queue
//{
//
//public:
//	queue(int size)//这是编译器的默认构造，调用内置类的构造函数
//	//	:_st1()
//	//	, _st2()构造函数会自己调用都会调用自己类的构造函数，我们不写编译器自己创建的构造函数就只会初始化自定义类型，不会初始化内置类型
//	{
//		_size = size;
//	}
//
//
//	//拷贝构造也是构造的一种，写了拷贝构造就不会默认生成一个了，参数必须引用否则会无穷递归，因形参会拷贝一份，又要调用拷贝构造
//	//queue(const queue& q)//拷贝构造无返回值，有且仅有一个参数是被拷贝参数的引用,并const保护原参数不变
//	//{
//	//	_size = q._size;
//	//	_st1 = q._st1;
//	//	_st2 = q._st2;//这是浅拷贝，因为对于指针它无法创建一个空间复制一份，只是指向了原来的空间。
//
//	//}
//private:
//	int _size /*= 10*/;//这个是缺省，没有具体的值，是为了构造的时候吧size初始化，是C++11的补丁。或者如上自己写构造
//	stack _st1;
//	stack _st2;
//};

//class Date
//{
//public:
//	//构造函数一般写成缺省，只有默认构造编译器才会自动调用,但却只会处理自定义类型
//	Date(int year = 2000, int month = 5, int day = 20)
//	{
//		this->_year = year;//this指针也可显式的运用，但是不能自己定义这个this，这是编译器的工作
//		_month = month;
//		_day = day;//利用隐含的this指针辨别
//	}
//
//	//一个类可以有多种初始化的方式，因函数重载。
//	//拷贝构造也算重载
//	//Date(int year, int month, int day = 20)
//	//{
//	//}
//
//	Date(const Date& d)//为什么要有拷贝构造呢，因为c语言的形参拷贝只是浅拷贝，对于指针类型只是创建了一个新指针却指向同一个地方
//						//而无法开辟空间进行深拷贝
//	{
//		_year = d._day;
//		_month = d._month;
//		_day = d._year;
//	}
//
//	void Print()//其他函数需要返回值
//	{
//		cout << _year<<"-"<<_month<<"-"<<_day << endl;
//	}
//	//日期类这种普通类就不需要析构函数，因为它的成员出了作用域就会都会自动销毁
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};

//int main()
//{
//	//stack st1;
//	//stack st2;//st1先构造，st2先析构，因为定义的结构是栈里面定义的。后进先出
//	queue q(10);
//	//stack st1;
//
//	//Date d1(2022, 5, 19);
//	//Date d2;//无参数则没有括号
//	//d1.Print();
//	//d2.Print();
//	return 0;
//}

//构造函数总结：C++的类一般都要自己写构造函数，只有少数情况才可以让编译器自己生成
//1.类成员都是自定义类型且他们自己都有默认构造函数			2.C++11之后支持类数据成员可以缺省。这也可以让其自己生成，因为全都能初始化








//析构函数，不写会自动生成一个默认的，析构函数没有参数，没有返回值。只是作用于销毁动态的内存空间如malloc的空间，按字节空间拷贝的
//析构函数不写也会自己生成，但是对内置类型不做处理，因不知内置类型如何定义，
//自定义类型会去调用它的析构，让自定义的析构处理。因自定义的知道自己要销毁什么

//若未显示定义，系统生成默认的拷贝构造函数。 默认的拷贝构造函数对象按内存存储按字节序完成拷贝
//这种拷贝我们叫做浅拷贝，或者值拷贝
//class stack
//{
//public:
//
//	~stack()
//	{
//		//析构函数只有一个，因无参数无返回值而不像构造一样有重载,析构函数会在类的声明周期结束时自动调用
//		free(_a);
//		_a = nullptr;
//		_top = _capacity = 0;//类的生命周期结束会自动调用
//	}
//	//日期类这种普通类就不需要析构函数，因为它的成员出了作用域就会都会自动销毁
//
//
//private:
//	int* _a;//这只是函数声明，类在对象实例化时才有内存，sizeof只能计算出数据成员的大小（栈中），因为成员函数保存在代码区中
//	int _top;
//	int _capacity;
//};






//拷贝构造也是构造的一种，写了拷贝构造就不会默认生成一个了，参数必须引用否则会无穷递归，因形参会拷贝一份，又要调用拷贝构造
// 自动生成的对内置类型的成员做值拷贝或浅拷贝，自定义类型成员是调用其自己的拷贝构造函数

//原因：c语言的传参都是内置类型的拷贝，可以说是浅拷贝，而对于c++的自定义传参，实参初始化统一类型的形参都要用拷贝构造，因为有空间开辟等深拷贝涉及。
//故自定义的传参拷贝都是要拷贝构造

//浅拷贝不一定是错误的，只能针对那些不指向空间的类型，如int ，float等。对于链表，树等指针不可浅拷贝，
//但是自定义类型中数组是可以的，数组存在对象中，不是指向别处，数组在对象中是有空间的，但是单单是数组传参却传的是首元素的地址了。这个又是指针了
class Date
{
public:

	Date(int year = 2000, int month = 5, int day = 20)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	//一个类可以有多种初始化的方式，因函数重载。
	//拷贝构造也算重载
	//Date(int year, int month, int day = 20)
	//{
	//}

	Date(const Date& d)//为什么要有拷贝构造呢，因为c语言的形参拷贝只是浅拷贝，对于指针类型只是创建了一个新指针却指向同一个地方
						//而无法开辟空间进行深拷贝，拷贝构造无返回值，有且仅有一个参数是被拷贝参数的引用,并const保护原参数不变
	{
		_year = d._day;
		_month = d._month;
		_day = d._year;
		//假如有这个：int* _a = a;//这是浅拷贝，因为对于指针它无法创建一个空间复制一份，只是指向了原来的空间
	}

	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};

void func(Date d)//只要是类的拷贝或传参都会调用拷贝构造
{

}

//构造函数
class stack
{
public:
	
	stack(int capacity = 10)//构造，自动生成的构造，内置成员不会处理，只会靠确定的构造函数处理
	{
		_a = (int*)malloc(sizeof(int) * capacity);
		assert(_a);
		_top = 0;
		_capacity = capacity;
	}

	//析构：自动生成的析构，内置类型的成员不会处理，因为不晓得你用什么方法实现，这里算是最底层的类，没包含子类了。写显式，因有动态的
	~stack()//无参数无返回
	{
		free(_a);
		_a = nullptr;
		cout << "析构stack(const stack& a)" << endl;
	}

	//拷贝构造
	stack(const stack& st)
	{
		//因有指针，要深拷贝。top和capacity可以浅拷贝
		cout << "stack.*a深拷贝" << endl;
		_a = st._a;//这里浅拷贝由于有st1和st2，释放两次同一块空间
		_top = st._top;
		_capacity = st._capacity;
	}



private:
	int* _a;//这只是函数声明，类在对象实例化时才有内存，sizeof只能计算出数据成员的大小，因为成员函数保存在代码区中
	int _top;
	int _capacity;
};

class MyQueue 
{
public:
	//无构造自动生成，调stack的构造
	//无析构，调用自动生成的

	//拷贝构造，内置类型成员浅拷贝，自定义类型成员调它的拷贝构造（无论它是否显式）

private:
	stack st1;
	stack st2;
};
int main()
{
	Date d1(2022, 5, 19);
	Date d2(d1);
	//func(d2);//这个也会调用拷贝构造


/*	stack st1(10);
	stack st2(st1);*///值拷贝对于指针，栈，链表，树等类型等会崩溃，1.两个对象指向同一块空间，析构会进行两次。崩
					//2.数据指向同一块空间，更新会一起更新。但stack的top指挥变一个，那么下一次访问另一个就会覆盖原来的数据，导致数据被修改
	//这便是浅拷贝的问题，原因就是指向同一块空间，解决方法就是自己写拷贝构造（因自定生成的是浅拷贝），写深拷贝

	MyQueue q1;
	MyQueue q2(q1);//这就会报错，因为释放了两次，指针浅拷贝

}