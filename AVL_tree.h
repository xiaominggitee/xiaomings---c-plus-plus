#pragma once
#include <iostream>
#include <assert.h>
#include <vector>
#include <queue>

using namespace std;

template <class K,class V>
struct TreeNode
{
	pair<K, V> _kv;
	int _bf;//balance factor平衡因子，STL规定不是一定要有，但是有了是方便对高度差进行管理，右减左

	TreeNode* _left;
	TreeNode* _right;
	TreeNode* _parent;

	//构造函数
	TreeNode(const pair<K,V>& kv)
		:_kv(kv)
		,_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_bf(0)
	{}
};

template <class K,class V>
class AVL_Tree
{
	typedef TreeNode<K, V> Node;
public:
	//AVL树，平衡搜索二叉树，只需要了解平衡规则，树的构成即可，懂得插入的原理，了解删除的原理（删除可以不用管），查找的话和普通搜索二叉树没有区别
	bool Insert(const pair<K, V>& kv)
	{
		//先找到插入的位置
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
				return false;//相等的模型先不管，multiset/multimap
		}
		//现在cur为空，就是插入的位置
		cur = new Node(kv);
		if (cur->_kv.first > parent->_kv.first)
			parent->_right = cur;
		else
			parent->_left = cur;
		cur->_parent = parent;


		//...
		//插入前是AVL树，现在更新插入后的树的平衡因子，平衡因子是右边减左边
		while (parent)
		{
			//若中途没有遇到0，一直更新到根节点，这时候的cur就是新的节点
			if (parent->_left == cur)
				parent->_bf--;
			else if (parent->_right == cur)
				parent->_bf++;
			else
				assert(false);

			//..判断是否还需要更新
			if (parent->_bf == 0)
				break;											//为0就不需要往上面更新了，说明已经平衡了

			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				cur = parent;
				parent = cur->_parent;							//是1或者-1继续更新
			}


			//..不符合AVL了，需要调整
			else if (parent->_bf == 2 || parent->_bf == -2)
			{

				if (parent->_bf == 2 && cur->_bf == 1)
				{
					RotateL(parent);								//都是右边子树高，需要parent左单旋
				}
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					RotateR(parent);								//都是左边子树高，需要parent右单旋
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					RotateRL(parent);								//向右折，在爷爷的右子树，父亲的左子树，右左双旋，先调整为一边高的情况
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					RotateLR(parent);								//向左折，在爷爷的左子树，父亲的右子树，左右双旋，先调整为一边高的情况
				}
				//调整完毕之后，不用向上走了
				break;
			}
			else
			{
				cout << "平衡因子异常11" << endl;
				assert(false);									// 插入之前AVL就存在不平衡子树，|平衡因子| >= 2的节点
			}
		}
		return true;
	}
	void InOrder()
	{
		InOrder(_root);
		cout << endl;
	}
	int Height()
	{
		return Height(_root);
	}
	bool isBalance()
	{
		return isBalance(_root);
	}

	vector<vector<int>> levelOrder()
	{
		vector<vector<int>> vv;
		if (_root == nullptr)
			return vv;

		queue<Node*> q;
		int levelSize = 1;
		q.push(_root);

		while (!q.empty())
		{
			// levelSize控制一层一层出
			vector<int> levelV;
			while (levelSize--)
			{
				Node* front = q.front();
				q.pop();
				levelV.push_back(front->_kv.first);
				if (front->_left)
					q.push(front->_left);

				if (front->_right)
					q.push(front->_right);
			}
			vv.push_back(levelV);
			for (auto e : levelV)
			{
				cout << e << " ";
			}
			cout << endl;

			// 上一层出完，下一层就都进队列
			levelSize = q.size();
		}
		return vv;
	}


private:
	Node* _root = nullptr;

	void RotateL(Node* parent)
	{
		//左单旋
		//（1）：中间那个节点(subR)是否为空， （2）：parent是否是根节点
		Node* ppNode = parent->_parent;
		Node* subRL = parent->_right->_left;
		Node* subR = parent->_right;

		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;			//（1）
		subR->_left = parent;
		parent->_parent = subR;

		if (parent == _root)					//（2）
		{
			_root = subR;
			_root->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subR;
			else
				ppNode->_right = subR;
			subR->_parent = ppNode;
		}

		//节点调整结束，更新平衡因子
		parent->_bf = subR->_bf = 0;

	}
	void RotateR(Node* parent)
	{
		//右单旋，同样的问题，中间节点是否为空，parent是否是根节点
		Node* ppNode = parent->_parent;
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		
		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;
		subL->_right = parent;
		parent->_parent = subL;

		if (parent ==_root)
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subL;
			else
				ppNode->_right = subL;
			subL->_parent = ppNode;
		}
		parent->_bf = subL->_bf = 0;

	}	
	void RotateRL(Node* parent)
	{
		//双旋问题的旋转不难，难的是调整后的平衡因子，因为末端节点的左右子树会分别给旋给两边，末端节点最后是根，共分三种情况
		//（末端节点就是新增节点）(末端节点新增在左子树，左旋给在之后的左节点的右子树)（末端节点新增在右子树，右旋给在之后右节点的左子树）

		//根据末端节点的平衡因子判断新增节点在哪,先保存下来，因为上面实现的复用会改变bf
		int bf = parent->_right->_left->_bf;
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		RotateR(parent->_right);
		RotateL(parent);

		if (bf == 0)
		{
			parent->_bf = 0;
			subR->_bf = 0;
			subRL->_bf = 0;
		}
		else if (bf == 1)
		{
			//新增在右边，之后在右节点的左子树,故右子树持平，左子树的右子树少一个
			parent->_bf = -1;
			subR->_bf = 0;
			subRL->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 0;
			subR->_bf = 1;
			subRL->_bf = 0;
		}
		else
			assert(false);
	}	
	void RotateLR(Node* parent)
	{
		//旋转后同样考虑调整平衡因子的三个可能性：原末端节点bf（0，-1，1）
		//（末端节点就是新增节点）(末端节点新增在左子树，左旋给在之后的左节点的右子树)（末端节点新增在右子树，右旋给在之后右节点的左子树）
		int bf = parent->_left->_right->_bf;
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		RotateL(parent->_left);
		RotateR(parent);

		if (bf == 0)
		{
			parent->_bf = 0;
			subL->_bf = 0;
			subLR->_bf = 0;
		}
		else if (bf == 1)
		{
			//新增在右边，之后在右节点的左子树,故右子树持平，左子树的右子树少一个
			//要清楚没添加节点前。根节点是-1，其余两个是0（或其中一个没有），添加之后是-2，1，0
			parent->_bf = 0;
			subL->_bf = -1;
			subLR->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			subL->_bf = 0;
			subLR->_bf = 0;
		}
		else
			assert(false);
	}
	void InOrder(Node* root)
	{
		if (root == nullptr)
			return;
		InOrder(root->_left);
		cout << root->_kv.first << " ";
		InOrder(root->_right);
	}

	int Height(Node* root)
	{
		if (root == nullptr)
			return 0;

		int left = Height(root->_left);//先存起来，之后取高的那一个
		int right = Height(root->_right);
		return left > right ? left + 1 : right + 1;
	}
	bool isBalance(Node* root)
	{
		//如何判断平衡？平衡因子没有超过2的，且每个节点的左右子树相见等于bf
		if (root == nullptr)
			return true;
		int left_h = Height(root->_left);
		int right_h = Height(root->_right);
		if (right_h - left_h != root->_bf)
		{
			cout << "平衡因子异常" << endl;
			return false;
		}
		if (abs(root->_bf) > 2)
		{
			cout << "不符合平衡树规则" << endl;
			return false;
		}
		return isBalance(root->_left) && isBalance(root->_right);
	}
};