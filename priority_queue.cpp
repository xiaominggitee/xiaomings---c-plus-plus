#define _CRT_SECURE_NO_WARNINGS 1
#include "8-6vector实现.h"
#include <deque>
namespace suzm
{
	//写个指针函数，代替仿函数

	bool less_compare(int a,const int b) 
	{
		return a < b;
	}


	
//仿函数，仿照函数用法的类
template <class T>
struct less
{
	bool operator()(const T& a, const T& b) const
	{
		return a < b;
	}
};//仿函数原理，用这个类定义一个对象，通过这个模板类来为模板传递类型参数，重载（）模拟函数的使用

template <class T>
struct greater
{
	bool operator()(const T& a, const T& b) const
	{
		return a > b;
	}
};


	template<class T, class container = su::vector<T>, class compare = suzm::less<T>>
	class priority_queue//堆,less大堆，greater小堆，为了和库保持一致
	{
	public:
		priority_queue(const compare& com = compare())//为了支持函数指针,给传进来的函数指针当作比较函数，没有仿函数用的方便
			:_com(com)
		{}

		template<class InputIterator>
		priority_queue(InputIterator first, InputIterator end,const compare& com = compare())
			:_com(com)
		{
			//区间建堆，就无需一个个插入了，对于topk问题比较好用
			while (first != end)
			{
				_pq.push_back(*first);
				first++;
			}
			for (int i = (_pq.size() - 2) / 2; i >= 0; i--)
			{
				Adjustdown(i);
			}

		}
		//析构函数不需要，拷贝构造、赋值重载按需要写
		void Adjustup(int child)
		{
			//若不是为了支持函数指针，可以直接在这里创建一个compare的对象，就无需每个queue都自己携带一个compare对象了
			//库里面携带了个compare对象

			int parent = (child - 1) / 2;
			while (child > 0)
			{
				if (_com(_pq[parent],_pq[child]))//仿函数或者函数指针的比较方式
				{
					swap(_pq[child], _pq[parent]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					break;
				}
			}
		}
		void Adjustdown(int parent)
		{
			int child = parent * 2 + 1;//默认左孩子
			int n = _pq.size();
			while (child < n)
			{
				if (child + 1 < n && _pq[child] < _pq[child + 1])
				{
					child++;
				}
				if (_com(_pq[parent], _pq[child]))
				{
					swap(_pq[child], _pq[parent]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					break;
				}
			}
		}
		//建堆，向下调整建堆较快，为O(N),上为O(N*logN)
		void push(const T& x)
		{
			_pq.push_back(x);
			Adjustup(_pq.size() - 1);
		}

		void pop()
		{
			assert(_pq.size() > 0);
			std::swap(_pq[0], _pq[_pq.size() - 1]);
			_pq.pop_back();

			Adjustdown(0);
		}
		
		int size() const
		{
			return _pq.size();
		}

		bool empty() const
		{
			return _pq.size() == 0;
		}

		const T& top()
		{
			return _pq.front();
		}

		const T& top() const 
		{
			return _pq.front();
		}
	private:
		compare _com;
		container _pq;
	};
}

void test_priority_queue1()
{
	//suzm::priority_queue<int,deque<int>, suzm::less<int>> pq1;
	//suzm::priority_queue<int> pq1;
	//suzm::priority_queue<int, su::vector<int>, greater<int>> pq1;
	suzm::priority_queue<int, su::vector<int>, bool (*) (int, int)> pq1(suzm::less_compare);//函数指针的使用，对于堆的比较，十分的繁杂

	pq1.push(1);
	pq1.push(5);
	pq1.push(3);
	pq1.push(8);
	while (!pq1.empty())
	{
		cout << pq1.top() << " ";
		pq1.pop();
	}
	cout << endl;

}
int main()
{

	int a[] = { 1,2,3,4,5,6,7 };
	suzm::priority_queue<int, su::vector<int>, bool (*) (int, int)> pq1(a,a+7,suzm::less_compare);//函数指针的使用，对于堆的比较，十分的繁杂

	while (!pq1.empty())
	{
		cout << pq1.top() << " ";
		pq1.pop();
	}
	cout << endl;

	return 0;
}