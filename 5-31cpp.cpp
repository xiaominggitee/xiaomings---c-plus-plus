#define _CRT_SECURE_NO_WARNINGS 1
#include "5-31.h"
////extern A;
 //extern int c;

//extern b;,静态不可
//友元函数的定义在另一个文件才可以
std::istream& operator>>(std::istream& in, B& b)
{
	in >> b._c >> b._d;
	return in;//为了多次提取
}

std::ostream& operator<<(std::ostream& out, const  B& b)//流插入运算符要加const防止改变
{
	out << b._c << b._d << endl;
	return out;
}


//int main()
//{
//	A aa(10, 2,10,20);
//	//const a;//常量和引用都只能在定义的时候初始化，因为都不可修改了的
//	cout << c;
//}

int func()
{
	cout << 1 << endl;
	return 1;
}
int main()
{
	func();
}