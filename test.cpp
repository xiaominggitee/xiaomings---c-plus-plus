#define _CRT_SECURE_NO_WARNINGS 1
#include "航map.h"
#include <vector>

//map是kv结构，对于联系两个不相关对象的，如水果计数，如汉英字典等有很大用处

void su_test_1()//测试航哥代码的pair中key可以被修改的问题
{
	su::map<int, int> a;
	a.insert(make_pair(1, 1));
	su::map<int, int >::iterator it = a.begin();

	//(*it).first = 10;//map的第二个参数是pair，里面用const key就能保证key不被修改了
}




//map和set的使用，其insert需要返回bool值，但是为了实现一些需求，其还需要返回插入成功的iterator
//故使用pair进行保存两个返回值


void su_test_2()
{
	int N = 100;

	su::map<int, int> a;
	srand(time(0));
	vector<int> v;
	for (int i = 0; i < N; i++)
	{
		v.push_back(rand());
	}
	for (int i = 0; i < N; i++)
	{
		a.insert(make_pair(v[i], v[i]));
	}

	int i = 0;
	for (auto e : v)
	{
		cout << a[v[i++]] << endl;//map实现的operator【】是用作于利用key来寻找value的。
	}

}
template<class K, class V>
V& su::map<K, V>::operator[](const K& key)
{
	//先查找后插入，这代码的复用性就不强，因为插入本来就是先寻找的。故insert返回pair<iterator,bool>这是其中的原因之一
	//实现的方法是用insert查找，找到返回该找到的迭代器，找不到返回新插入的迭代器。
	pair<iterator, bool> ret = insert(make_pair(key, V()));
	return ret.first->second;
}

//int main()
//{
//	test_2();
//	return 0;
//}


//库中map和set的使用，注意multimap没有operator【】的使用，只有普通map，因为multi无法准确对应一个值
#include <iostream>
#include <map>
#include <set>
using namespace std;
void std_test_1()
{
	map<int, int>  a;
	int N = 10;
	srand(time(0));
	vector<int> v;
	for (int i = 0; i < N; i++)
	{
		v.push_back(rand());
	}
	for (int i = 0; i < N; i++)
	{
		a.insert(make_pair(v[i], v[i]));
	}

	a.at(v[0]) = 10;
	cout << a[v[0]] << endl;//at的用法和【】的用法是一样的

}

//另外erase和边界控制的lower_bound以及upper_bound 的使用
void std_test_2()
{
	std::map<char, int> mymap = { {'a', 20}, {'b', 40}, {'c', 60}, {'d', 80}, {'e', 100} };
	auto itstart = mymap.lower_bound('a');//第一个大于等于a的数   【】闭区间
	auto itend = mymap.upper_bound('c');//第一个大于c的数   （）开区间,就是d

	cout << "删前" << endl;

	for (auto e : mymap)
	{
		cout << e.first << ":" << e.second << endl;
	}

	//mymap.erase('c');//erase的三种删除，迭代器pos，搜索key删除,迭代器范围
	//mymap.erase(mymap.begin());
	mymap.erase(itstart, itend);//a-d，左闭右开
	cout << "删后" << endl;

	for (auto e : mymap)
	{
		cout << e.first << ":" << e.second << endl;

	}

}
int main()
{
	std_test_2();

	return 0;
}

