#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
using namespace std;

//string是一个basic_string的typedef，是为了容纳各种char类型的东西，支持不同编码如unicode的u-16char和32char以及微软的wchar都是不同大小的


void test1()
{
	//构造

	//常用的构造方式，空构造，字符串构造，拷贝构造
	string s1;
	string s2("hello world");
	string s3(s2);
	string s4 = s2;
	//string s4 = "hello world";//隐式类型转换，构造
	string s5 = s2;//拷贝构造


	//不常用的构造方式
	string s6("https://cplusplus.com/reference/", 4);//取前几个构造
	string s7(10, 'x');//10个x构造

	string s8(s2, 6, 3);//从pos 6位置处，取npos 3个字符，即拷贝构造子串
						//其中npos是string类的一个静态静态成员变量，是size_t -1,即整型最大值，意味着拷贝pos后面全部，遇到结尾停止

	cout << s1 << endl;
	cout << s2 << endl;
	cout << s3 << endl;
	cout << s4 << endl;
	cout << s5 << endl;
	cout << s6 << endl;
	cout << s7 << endl;
	cout << s8 << endl;



	cout << s1.size() << endl;
	//析构，string是一个动态增长的字符数组，是new或malloc出来的，可以表示任意长度的字符串,也是\0结尾，size不包括包括\0了，不可用sizeof计算，有size()成员函数
	//析构不用怎么学，他是自己调用的。

}

void test2()
{
	string s1;
	string s2 = "xxx";
	//赋值
	s1 = s2;
	s1 = "yyy";
	s1 = "y";
	cout << s1 << endl;
	cout << s2 << endl;

	//[]运算符重载,取的值是数组的引用，是为了支持修改返回值
	cout << s1.size() << endl;
	for (size_t i = 0; i < s1.size(); i++)
	{
		//调用的是运算符重载，s1.operator[](i);
		cout << s1[i] << " ";
	}
	cout << endl;

	//而内置类型是语法支持的解引用，自定义类型是运算符重载
	 
	const char* s3 = "world";//s3里面就能直接看到\0
	char a = s3[3];//*（s3+3），是解引用
}


void test3()
{
	string s1;
	string s2 = "hello world";


	//遍历访问1,下标循环
	for (size_t i = 0; i < s1.size(); i++)
	{
		//调用的是运算符重载，s1.operator[](i);
		cout << s1[i] << " ";
	}
	cout << endl;



	//遍历访问2，迭代器
	//迭代器：想指针一样的东西，并不完全是指针，利用begin和end成员函数可遍历
	//迭代器是在类里面的内嵌类型，利用类域打开
	string::iterator it = s2.begin();
	while (it != s2.end())//end是有效数据的下一个，这里即是\0，才可以使全部数据被访问
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

	//begin和end的返回值是迭代器的值



	//遍历访问3，范围for,c++11之后的语法，范围for底层替换迭代器
	for (auto& ch : s1)
	{
		cout << ch << " ";//ch自动++，自动识别结尾
	}
	cout << endl;

	
}

void test4()
{
	//[]重载以及at函数的使用,[]有两个重载，一个const一个非，作为区分。
	string s1("hello");//string的size是不包含结尾\0的大小，但是下标访问却不会报错
	const string s2("hello");

	//s1[5] = 'x';
	//s2[5] = 'x';//const不能改


	//at的使用
	//s1.at(5) = 'y';//作用一样

	//区别，【】断言检查越界，at用异常，异常可以捕获
	//s1.at(6);
	//s1[6];

	//s2.at(6);
	//s2[6];//越界会报错
	

	//[]重载和普通数组的[]访问区别
	//[]数组读和写的越界检查不一定能够检查出来，因为编译器对普通数组的检查是抽查
	//int a[10] = {0};
	//a[100] = 10;
	//a[111];

	//但是，string的【】重载一定会检查出来，因为实现的时候都是直接用assert去判断的
	s1[5] = 0;//  到/0的位置可以，assert（i<=s1.size()）
	//s1[6];//越界了

}


void test5()
{
	string s1;
	string s2 = "hello world";

	//正向迭代器
	string::iterator it = s2.begin();
	while (it != s2.end())//
	{
		*it += 1;
		cout << *it << " ";
		it++;
	}
	cout << endl;

	//正向迭代器可以反着用，但是对于一些只能正着走的结构是不可以的，故还需正管正，反管反
	string::iterator ite = s2.end()-1;
	while (ite != s2.begin())//
	{
		cout << *ite << " ";
		ite--;
	}
	cout << endl;

	//反向迭代器
	string::reverse_iterator rit = s2.rbegin();
	while (rit != s2.rend())//反向的迭代器的开始和结尾都有一个r函数，表示rbegin和rend函数
	{
		*rit -= 1;//可加减
		cout << *rit << " ";
		rit++;
	}
	cout << endl;



	//同时正反迭代器都可读可写，且只能单向进行，就有了const正向和const反向迭代器
	//c++11对const添加了cbegin和cend，规范了const对象的使用，但是原本的也没错，因为写了const的begin const重载，自动识别
	
	
	//正向迭代器
	string::const_iterator cit = s2.begin();//这是对于const对象的迭代
	while (cit != s2.end())//
	{
		//*cit += 1;//不可加减

		cout << *cit << " ";
		cit++;
	}
	cout << endl;

	string::const_reverse_iterator crit = s2.rbegin();//这里aotu就可以用了
	while (crit != s2.rend())//反向的迭代器的开始和结尾都有一个r函数，表示rbegin和rend函数
	{
		//*crit += 1;//不可加减
		cout << *crit << " ";
		crit++;
	}
	cout << endl;
}


void test6()
{
	//小知识，reserve，resize，length，size，max_szie,clear

	string s1("hello world");

	//length和szie
	//string比stl早，二者是一样的，只是stl采用了size而不是length，因为还有树形结构，对length的话不是很合适
	//他们都是用来计算大小，且不算\0，但是string依然是有\0的

	cout << s1.size() << endl;
	cout << s1.length() << endl;
	cout << s1.max_size() << endl;//最大支持的容量
	cout << s1.capacity() << endl;//扩容后的容量
	

	//对于扩容，vs大概每次1.5倍，linux大概2倍，扩容有消耗，对于已知的大小可以采用reserve（保存）提前开辟空间，
	s1.reserve(1000);
	//s1.resize(1000);//开空间加初始化，默认为0，可以修改，如s1.resize(1000，'x')


	s1.resize(1000,'x');//不会改变原有的数据,这都是提前开空间，并不是扩容，只有realloc可以扩，resize初始化了，所以size大小就会变
	cout << s1.capacity() << endl;//至少先开1000，可能对齐的问题有一点点偏差，1007


	s1.reserve(10);//不会缩容
	s1.resize(10);//也不会缩小容量，但是初始化的值缩小了，故size变小


	//总结，reserve和resize都是为了减少扩容的消耗，提前开辟的空间，并不是可以实现扩容。要初始化就用resize，不初始化可以用reserve提前开辟


	s1.clear();//清数据，不改变容量

}

void test7()
{
	//插入，因为是数组，所以最舒服的是尾插

	string s1("hello world");
	s1.push_back('x');//尾插
	s1.append(" fsaas");//附加
	s1.append("s");//附加只能加串，双引号


	//最舒服的是+=，因为是尾插
	s1 += " fasfdas";
	s1 += 'x';//而+=什么都可以
}

int main()
{
	test5();
}