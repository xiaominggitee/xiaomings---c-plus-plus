#define _CRT_SECURE_NO_WARNINGS 1
#include "AVL_tree.h"
#include <vector>
void AVL_test1()
{
	const size_t N = 10;
	vector<int> v;
	v.reserve(N);
	//srand((unsigned)time(0));
	for (size_t i = 0; i < N; ++i)
	{
		v.push_back(rand());
		//v.push_back(i);
	}

	AVL_Tree<int, int> t;
	for (auto e : v)
	{
		t.Insert(make_pair(e, e));
	}

	//t.levelOrder();
	//cout << endl;
	cout << "�Ƿ�ƽ��?" << t.isBalance() << endl;
	//cout << "�߶�:" << t.Height() << endl;
	t.InOrder();
	//t.levelOrder();
	
}
void AVL_test2()
{
	//int a[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
	int a[] = { 30,29,28,27,26,25,24,11,8,7,6,5,4,3,2,1 };
	AVL_Tree<int, int> t;
	for (auto e : a)
	{
		t.Insert(make_pair(e, e));
	}
	t.levelOrder();
}
int main()
{
	AVL_test1();
	
	return 0;
}