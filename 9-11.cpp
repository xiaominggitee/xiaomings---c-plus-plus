#define _CRT_SECURE_NO_WARNINGS 1
#include <string>
#include <iostream>
using namespace std;


class A
{
public:
	explicit A(const char* str)//类里面str的参数也是能用const char*类型的变量去初始化string 的
		:_str(str)
	{
		cout << "A" << endl;
	}
	~A()
	{
		cout << "~A" << endl;
	}

private:
	string _str;
};
class B
{
public:
	B(int a = 10, const char* str = "")
		:_a(a)
		,_ab(str)
	{
		cout << "B" << endl;
	}
	~B()
	{
		cout << "~B" << endl;
		//析构只需要处理类所管理的空间就可以，不用显示实现对自定义类的析构，在~B结束前即}前实现自定义类型的析构，先定义的后析构，即栈的属性
	}
private:
	A _ab;
	int _a;
};

int main()
{
	const char* s = "sfad";
	//A a(s);
	B b;
	return 0;
}

