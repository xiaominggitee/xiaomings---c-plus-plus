#define _CRT_SECURE_NO_WARNINGS 1
//#include <vector>
#include <iostream>
using namespace std;
#include "8-6vector实现.h"
using namespace su;
vector<vector<int>> generate(int numRows) {
    //二维数组
    su::vector<su::vector<int>> vv;//这时候已经调用默认构造了
    vv.resize(numRows);//创建空间加初始化
    for (int i = 0; i < numRows; i++)
    {
        vv[i].resize(i + 1, 0);//先初始化为0；  

        vv[i][0] = 1;
        vv[i][i] = 1;   //赋值       
    }
    for (int i = 0; i < numRows; i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (vv[i][j] == 0)
            {
                vv[i][j] = vv[i - 1][j] + vv[i - 1][j - 1];
            }

        }
    }
    return vv;
}
int main()
{
    vector<vector<int>> vv = generate(5);
    for (size_t i = 0; i < vv._size(); i++)
    {
        for (size_t j = 0; j < vv[i]._size(); j++)
        {
            cout << vv[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}

class Solution {
    string a[10] = { "","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz" };
public:
    void combine(string digits, int i, string tmps, vector<string>& vs)
    {

        if (tmps.size() == digits.size())
        {
            vs.push_back(tmps);
            return;
        }
        int num = digits[i] - '0';

        for (auto e : a[num])
        {
            combine(digits, i + 1, tmps + e, vs);
        }

    }

    vector<string> letterCombinations(string digits) {
        //循环递归，实现多路递归。二叉树是固定两路，直接写死两个
        vector<string>  vs;
        if (digits.empty())
        {
            return vs;
        }
        int i = 0;
        string s;
        combine(digits, i, s, vs);

        return vs;
    }
};


//int main()
//{
//    int* a = new int[10];
//    int* b = nullptr;
//    delete[] b;
//    delete[] a;
//    return 0;
//}