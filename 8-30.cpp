#include <iostream>
using namespace std;
//int main()
//{
//    int n = 0;
//    cin >> n;
//    int count = 0;
//    for (int i = 2; i < n; i++)
//    {
//        int flag = 0;
//        for (int j = 2; j < i; j++)
//        {
//            if (i % j == 0)
//            {
//                flag = 1;//不是质数，退出
//                break;
//            }
//        }
//        if (flag == 0)
//            count++;
//    }
//    cout << count;
//    return 0;
//}

#include <stdio.h>
void fun(const char** p)
{
    int i;
    for (i = 0; i < 4; i++)
        printf("%s", p[i]);
} 
//int main()
//{
//    const char* s[6] = { "ABCD", "EFGH", "IJKL", "MNOP", "QRST", "UVWX" };
//    fun(s);
//    //printf("%s\n", s);
//    //printf("%s\n", *s);
//    //printf("%s\n", *(s+1));
//
//
//    printf("\n");
//    return 0;
//}



//方法错误，无法找到第一个。
#include <string.h>
class Solution {
public:
    int FirstNotRepeatingChar(string str) {
        //一共26个字母，和大小写
        int s[54] = {0};
        std::string::iterator it = str.begin();
        while (it != str.end())
        {
            if (*it < 97)//大写
            {
                s[*it - 65]++;
            }
            else
            {
                s[*it - 97 + 26]++;
            }
            it++;
        }
        int i;
        for (i = 0; i < 54; i++)
        {
            if (s[i] == 1)
            {
                break;
            }
        }
        if (i != 54)
        {

            char a;
            if (i >= 26)
                a = i + 97-26;
            else
                a = i + 65;
            for (size_t j = 0; j < str.size(); j++)
            {
                if (str[j] == a)
                    return j;
            }
        }
        return -1;

    }
};

int main()
{
    const string str("google");
    Solution s1;
    s1.FirstNotRepeatingChar(str);
}