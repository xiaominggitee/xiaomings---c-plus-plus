#define _CRT_SECURE_NO_WARNINGS 1

//#include "../stackCPP/stack.h"
#include <iostream>
using namespace std;


//#include "stack.h"
//int main()
//{
//	int a = func();
//	cout << a;
//}

//int main()
//{
//	ST s;//调用什么只需要有这函数的入口即可，无需看里面具体实现
//	StackInit(&s);
//}
//extern "C"的底层原因是因为c++的函数重载，因为C++函数重载的底层是C和C++的函数符号表的名称由函数名长度+函数名+类型，和C是不一样的。
//于是为了C++和C之间的相互调用，引入了extern "C"

inline int add(int a, int b);
int add(int a, int b)
{
	return a + b;
}



void test1()
{
	int a = 10;//这是拷贝赋值
	int& b = a;//引用是取别名，b代表a，引用才会设计权限放大和缩小


	double c = 2.2;
	int d = c;//隐式类型转换，但是依然是拷贝赋值.中间有个临时变量
	//int& e = c;//错误，临时变量的常属性
	const int& f = c;

	double g = a;
	//double& i = a;
	const double& h = a;//这也是一样，但这个不会丢失数据，临时变量的具有常属性

	const int x = 10;//对常量的引用
	//int& y = x;//权限的放大不可。
}


void test2()
{
	add(1, 2);
	int ret = add(1, 2);
}


//void testfor(int arr[])
//{
//	for (auto e : arr)//范围for只适用于数组名，传参只是受元素的地址。范围for也是和szieof类似看数组名的，只是元素的地址是不可的
//	{
//		cout << e << " ";
//	}
//}

void test3()
{
	int a = 10;
	auto b = 10;//自动识别

	auto c = 11, d = 20;//同类型才可识别。
	cout << typeid(c).name() << endl;

	//auto的范围for
	int arr[] = { 1,2,3,4,5,6,7,8,9 };
	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		arr[i] *= 2;
		cout << arr[i]<<" ";
	}
	cout << endl;

	//testfor(arr);范围for只适用于数组名，传参只是受元素的地址。范围for也是和szieof类似看数组名的，只是元素的地址是不可的

	//for (auto e : arr)
	//{
	//	e /= 2;	
	//}
	//cout << endl;

	for (/*int*/auto& e : arr)//当然e的类型也是可以写成具体的某个类型
	{
		e /= 2;
	}
	cout << endl;

	for (auto e : arr)//范围for是拷贝赋值，并没有改变arr的值吗，利用引用即可
	{
		cout << e << " ";
	}
	cout << endl;

	//auto不代表具体类型，必须定义的时候就指定了。也不可返回值和定义数组，这写都需要具体的类型
	//参数和返回值需要明确，，不然auto会滥用，用户和程序员无法具体明白函数怎么使用
}



void test4()
{
	//c++里面的null是0，在c里面定义的是（（void*）0），也就是说是会有风险

#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif

	//c里面就没有这个问题，故需要用nullptr，可能是c++早期的失误，在c++11里面补坑了，nullptr是关键字，不会有这种情况了，其等价于（（void*） 0）
}


class Date
{

public:
	Date(int year,int month,int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	void print()
	{
		cout << _year << " " << _month << " " << _day;
	}


private:
	int _year;
	int _month;
	int _day;
};
int main()
{
	Date d1(2000,05,20);
	Date d2(2002,03,62);

	d1.print();
	//d2.print(&d2);//不可自己坐编译器的工作

	return 0;
}

