#define _CRT_SECURE_NO_WARNINGS 1
#include "Date_class.h"

//成员函数在类里面定义默认是内联函数，而


// 获取某年某月的天数
int Date::GetMonthDay(int year, int month)
{
	//可以定义为const，且static还可以节省创建时间,那么下面就不能修改二月++了，要直接返回数据
	const static int MonthDayarray[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))//二月为前提条件
		return 29;//不修改数据，
	return MonthDayarray[month];
}



//// 拷贝构造函数，编译器自己生成的也是const的引用
//// d2(d1)
//Date::Date( Date& d)//拷贝构造只有一个参数,是被拷贝对象的引用，引用对象不改变的话尽量用const，
//{																	//1.防止数据被修改，2.防止d是函数返回的临时对象,导致权限放大
//	_year = d._year;
//	_day = d._day;
//	_month = d._month;
//}


// 赋值运算符重载
// d2 = d3 -> d2.operator=(&d2, d3)
Date& Date::operator=(const Date& d)
{
	if (this != &d)//两个对象一样就不需要了，赋值是两个已经存在的对象，拷贝构造是利用一个对象构建一个新的对象
	{
		//一般用地址判断是否是同一个元素
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	return *this;//由于出了作用域对象还存在，就可使用引用返回
}


// 析构函数
Date::~Date()
{
	//不需要啥
}



//+和+=的相互复用
// 日期+=天数
Date& Date::operator+=(int day)
{
	//*this = *this + day;
	//return *this;//+=复用+，但是这种会导致多用了两次+函数里面的拷贝构造，相对于不是很优，但是也是可以的

	//处理负数问题
	if (day < 0)
	{
		return *this -= (-day);
	}

	_day += day;
	int curday = GetMonthDay(_year, _month);
	while (_day > curday)
	{
		_day -= curday;//减去原来这个月的天数，由于day不确定大小，故可能超过几个月，需要循环
		if (++_month > 12)
		{
			_month %= 12;
			_year++;
		}
		curday = GetMonthDay(_year, _month);//获取新的月份的天数
	}
	return *this;
}


// 日期+天数
Date Date::operator+(int day) const//a+b，返回的是一个新的对象
{
	//this->operator+=(),这个是错误的，const的成员函数的this是一个const Date* const this ,而+=里面的是一个非const的this，权限就放大了
	
	//对象的元素不会发生变化，变化的是新的对象
	Date temp(*this);
	temp += day;//这样复用+=,*this还是不变的，这个复用会相对较好

	//temp._day += day;
	//int curday = GetMonthDay(temp._year, temp._month);
	//while (temp._day > curday)
	//{
	//	temp._day -= curday;//减去这个月的天数，由于day不确定大小，故可能超过几个月，需要循环
	//	if (++temp._month > 12)
	//	{
	//		temp._month %= 12;//可以直接等于1
	//		temp._year++;
	//	}
	//	curday = GetMonthDay(temp._year, temp._month);//新的月份的天数
	//}
	return temp;

}


// 日期-天数
Date Date::operator-(int day) const
{
	//减法复用-=,能复用尽量复用，因为保证数据安全
	Date temp(*this);
	temp -= day;

	//temp._day -= day;
	//while (temp._day <= 0)
	//{
	//	//int curday = GetMonthDay(temp._year, temp._month);要月份先减了得出上个月的月份
	//	//temp._day += curday;
	//	if (--temp._month == 0)
	//	{
	//		temp._year--;
	//		temp._month = 12;
	//	}
	//	temp._day += GetMonthDay(temp._year, temp._month);//借位借的是上一个月的天数，因为是加回来，你这个月的天数正在用
	//	
	//}
	return temp;

}

// 日期-=天数
Date& Date::operator-=(int day)
{
	//处理负数问题
	if (day < 0)
		return *this += (-day);//因为减法的循环条件是小于零继续循环，而这个day是负数就直接大于零了

	_day -= day;
	while (_day <= 0)
	{
		if (--_month == 0)
		{
			_year--;
			_month = 12;
		}
		_day += GetMonthDay(_year, _month);
	}
	return *this;
}


// 前置++
Date& Date::operator++()//无参数的是前置++
{
	*this += 1;
	return *this;

	//_day++;//++，减去的是原来的天数
	//int curday = GetMonthDay(_year, _month);
	//if (_day > curday )
	//{
	//	_day -= curday;
	//	if (++_month > 12)
	//	{
	//		_month = 1;
	//		_year++;
	//	}
	//}
	//return *this;
}

// 后置++，					//这个不需要知道穿过来是什么就可以不用写，规定用int区分，其他类型不可以，
Date Date::operator++(int /*i*/)//由于前置++和后置++参数和名字相同无法重载，故C++语法规定后置要多一个参数用来区分
{
	Date temp(*this);
	//复用
	*this += 1;
	return temp;

	//_day++;
	//int curday = GetMonthDay(_year, _month);
	//if (_day > curday)
	//{
	//	_day -= curday;//++，减去的是原来的天数
	//	if (++_month > 12)
	//	{
	//		_month = 1;
	//		_year++;
	//	}
	//}
	//return temp;//*this的值改变，返回原来的值
}

// 后置--

Date Date::operator--(int)
{
	Date temp(*this);
	*this -= 1;
	return temp;


	//Date temp(*this);
	//_day--;
	//if (_day <= 0)
	//{
	//	if (--_month <= 0)
	//	{
	//		_month = 12;
	//		_year--;
	//	}
	//	_day += GetMonthDay(_year, _month);//--，加的是变化后的天数
	//}
	//return temp;
}

// 前置--

Date& Date::operator--()
{
	*this -= 1;
	return *this;

	//_day--;
	//if (_day <= 0)
	//{
	//	if (--_month <= 0)
	//	{
	//		_month = 12;
	//		_year--;
	//	}
	//	_day += GetMonthDay(_year,_month);//--，加的是变化后的天数
	//}
	//return *this;
}


//硬减很麻烦，除了硬减，可以用循环计数法
// 日期-日期 返回天数
int Date::operator-(const Date& d) const
{
	//需要判断大小，因为循环需要知道加或者减
	int flag = 1;
	Date max(*this);
	Date min(d);
	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;//控制正负
	}
	int n = 0;
	while (min != max)
	{
		n++;
		min++;
	}
	return n * flag;
}

//流提取运算符
std::istream& operator>>(std::istream& in, Date& d)
{
	
	in >> d._year >> d._month >> d._day;
	//判断时期是否合法
	assert(d._year >= 1 && d._month >= 1 && d._month <= 12 && d._day >= 1 && d._day <= d.GetMonthDay(d._year, d._month));
	return in;
}

//流插入运算符
std::ostream& operator<<(std::ostream& out, const Date& d)
{
	out << d._year << '-' << d._month << '-' << d._day << endl;
	return out;
}
