

    class J_SuperClass {
        void mb_method() {
            System.out.println("J_SuperClass::mb_method");
        }
        void mb_methodStatic() {
            System.out.println("J_SuperClass::mb_methodStatic");
        }
    }
    public class Exam2 extends J_SuperClass {
        void mb_method() {
            System.out.println("J_Test::mb_method");
        }
        void mb_methodStatic() {
            System.out.println("J_Test::mb_methodStatic");
        }
        public static void main(String args[]) {
            J_SuperClass a = new Exam2();
            a.mb_method();
            a.mb_methodStatic();
            Exam2 b = new Exam2();
            b.mb_method();
            b.mb_methodStatic();
        }

}
