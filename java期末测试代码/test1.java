interface Shape{
    double PI = Math.PI;
    double area();
}
    class Cycle implements Shape{
        private double r;
        public Cycle(double r){
            this.r=r;
        }
        public double area(){
            return PI*r*r;
        }
    }
    public class test1 {
        public static void main(String[] args) {
//            Cycle c = new Cycle(1.5);
//            System.out.println("面积为："+c.area());
            boolean a = "hello" == (new String("hello"));//比较的是地址空间
            boolean b= "hello".equals(new String("hello"));
            boolean c= "hello".equals(new StringBuffer("hello"));
            boolean d= "hello"== (new StringBuffer("hello").toString());
//            String str  = String.valueOf(new StringBuffer("je"));
            System.out.println(a);
            System.out.println(b);
            System.out.println(c);//类型不同无法比较。一比较就是错，若是用==比较。则编译报错
            System.out.println(new StringBuffer("hello").toString());

        }
    }

