class TestDemo {
    int m=5;
    public void some(int x){
        m=x;
    }
}
public class Demo extends TestDemo{
    int m=8;
    public void some(int x){
        super.some(x);
        System.out.println(m);
    }
    public static void main(String args[]){
        new Demo().some(7);
    }

}