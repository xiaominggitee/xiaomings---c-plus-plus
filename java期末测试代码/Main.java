public class Main {
    public static void main(String[] args) {
        String str1 = "java";
        String str2 = "java";
        String str3 = new String("java");
        StringBuffer str4 = new StringBuffer("java");

        boolean a = str1==str2;
//        boolean b = str1==str4;//类型不同无法比较
        boolean c =  str2.equals(str3);
        boolean d = str3.equals(str4.toString());
        System.out.println(a);
        System.out.println(c);
        System.out.println(d);

    }
}