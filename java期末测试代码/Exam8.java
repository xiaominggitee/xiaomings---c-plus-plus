import java.awt.*;
import javax.swing.*;

public class Exam8 {
    public static void main(String[] args) {
        JFrame app = new JFrame("FlowLayou");
        Container container = app.getContentPane();
        container.setLayout(new FlowLayout());
        JButton btns[] = new JButton[5];

        for (int i = 0; i < btns.length; i++) {
            String s = "Button " + i;
            btns[i] = new JButton(s);
            btns[i].setSize(100, 50);
            container.add(btns[i]);
        }
        app.setSize(350,150);
        app.setVisible(true);
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}