#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;


//class Base {
//public:
//	virtual void func1()
//	{
//		cout << "Base::func1" << endl; 
//	}
//	virtual void func2() 
//	{ 
//		cout << "Base::func2" << endl;
//	}
//private:
//	int a;
//};
//class Derive :public Base {
//public:
//	virtual void func1() 
//	{ 
//		cout << "Derive::func1" << endl;
//	}
//	virtual void func3()
//	{ 
//		cout << "Derive::func3" << endl; 
//	}
//	virtual void func4()
//	{
//		cout << "Derive::func4" << endl;
//	}
//private:
//	int b;
//};
//int main()
//{
//	Derive d;
//	Base b;
//	return 0;
//}



class person
{
public:
	virtual person* func()//构成重写的函数必须完全相同（三同）或者协变，而重载可以不看重返回值
	{
		cout << "普通人全价" << endl;
		return this;
	}
	void Buy()
	{
		cout << "person::Buy()" << endl;
	}
};

class student :public person//没有成员变量会固定一个字节的大小
{
public:
	student* func()
	{
		cout << "学生半价" << endl;//协变完成重写，指的是返回值可以不同的特殊重写，但是返回值必须是父子类关系的指针或引用
		return this;
	}
	void Buy()
	{
		cout << "student" << endl;
	}
};
void test1() {
	person* ptr1 = new person();
	person* ptr2 = new student();

	ptr1->func();//多态在是父类切片，重写并覆盖了虚表才可以使指针或者引用去该虚表中找自己需要的函数地址
	ptr2->func();

	ptr1->Buy();
	ptr2->Buy();//不构成多态，那么看的就是你的类型，类型是什么调的就是什么，只有在子类中调父类才会有隐藏}
}
//int main()
//{
//	test1();
//}
//


//多态形成的条件：（1）虚函数的重写，（2）父类的引用或者指针调用(子类传过来会切片)
//虚函数的函数继承下来是接口继承，即只是把函数声明的那一部分继承下来，也就是说即使子类不写virtual也会有，缺省也算接口的内容
//实现继承：普通函数的继承，即全部包括实现都继承下来，用的都是同一个
//接口继承：虚函数继承

//重写和覆盖，形象的说就是把虚表拷贝过去子类按照自己的重写，然后覆盖回这个位置

//class A
//{
//public:
//	virtual void func(int val = 1) { std::cout << "A->" << val << std::endl; }
//
//	virtual void test() { func(); }
//};
//class B : public A
//{
//public:
//	virtual void func(int val = 0) { std::cout << "B->" << val << std::endl; }
//};
//int main(int argc, char* argv[])
//{
//	B* p = new B;
//	p->test();
//	//p->func();//这里没有构成多态，只是B调用B的函数而已，多态是不同子类调用父类的指针或者引用构成的，B->0
//	return 0;
//}
//A: A->0 B : B->1 C : A->1 D : B->0 E : 编译出错 F : 以上都不正确
//答案是B，接口继承，B在test中被切片位A*对象，即使B写没写virtual都会被接口继承下来

//运行时决议：运行时确定函数地址，是多态中运行后去找虚函数表才得到的函数地址
//编译时决议：编译时确定函数地址，如汇编形成的符号表，虚函数也会在符号表存在，只要有函数地址就能存




//****************************************************************************************final、override、纯虚函数（后面=0即可，可只写声明），都是c++11的概念
// 
// 
//final修饰函数和类
//class A final
//{
//public:
//	int _a;
//};
//class b:public A  final修饰的类不能被继承了
//{
//};

//class B
//{
//public:
//	virtual void print() final		//final修饰的函数只能是虚函数，表示不能被重写了
//	{
//	}
//	virtual void func() = 0;//纯虚函数可以直接写声明就行，实现也可写但是没用处，含有纯虚函数的类为抽象类，自己不能被实例化了，只有子类重写了这个函数，函数有了实现，子类才能被实例化
//	virtual void func1()
//	{
//		cout << "B::func1" << endl;
//	}
//private:
//	int _b;
//};
//
//class C :public B
//{
//public:
//	//virtual void print()无法重写被final修饰的函数
//
//	virtual void func()
//	{
//		cout << "func" << endl;
//	}//有了纯虚函数的重写，C类才内被实例化，否则纯虚函数也在C类中
//};
//
//class D:public C
//{
//public:
//	virtual void func1() override			//B的func1也在C中，继承下来也能重写,override的作用是检查重写是否完成，没完成报错，如把父类的virtual去掉
//	{
//		cout << "D::func1已完成重写" << endl;
//	}
//};
//int main()
//{
//	D d;
//	d.func1();
//	return 0;
//}
//


//******************************监视的虚表可能不可见，即被编译器隐藏掉了。可以通过函数指针数组查看虚表,或者又建一个新类继承该子类，才能查看该子类的完整虚表（只要是虚函数就会在虚表）
//有地址的函数，都会在符号表中保留。vs2013的虚表设置以空指针结束

class B
{
public:

	virtual void func1()
	{
		cout << "B::func1" << endl;
	}
	void func2()
	{
		cout << "B::func2" << endl;

	}
private:
	int _b;
};

class C :public B
{
public:
	C()
		:a(0)
	{}
	virtual void func1()
	{
		cout << "C::func1" << endl;
	}
	virtual void func3()//这个虚函数不构成重写，vs的编译器做的感觉多余的优化，把他在监视中隐藏掉了。但是内存依然有
	{
		cout << "C::func3" << endl;
		cout << a << endl;
		cout << this << endl;
	}

	int a;
};

//typedef void(*)() V_func;错误的定义方法
typedef void(*V_func)();		//没有参数的类型在里面定义,函数指针数组

void print(V_func* a)//传函数指针的地址过来
{
	printf("%p\n", a);

	for (int i = 0; i<2; i++)//
	{
		printf("a[%d]:%p", i, a[i]);
		V_func f = a[i];//此时把虚表的地址作为该类的地址this
		f();
	}
}

void test2()
{
	C c;
	//c.func1();
	//c.func3();
	print((V_func*)*((int*)&c));//利用了int*解引用刚好是4个字节的原理，32位的地址刚好4个字节适配int*

}


//******************************************基类的析构函数一般写成虚函数
class car
{
public:
	car()
		:_a(10)
	{}


	virtual ~car()
	{
		cout << "car" << endl;
	}
public:
	int _a;
};
class BWM:public car
{
public:
	BWM(const char* name ="")
	{
		strcpy(_name, name);
	}

	~BWM()
	{
		delete[] _name;
		cout << "~BMW" << endl;//析构
	}

	char* _name = new char[10];

};
int main()
{


	car* ptr1 = new car;
	car* ptr2  =new BWM;

	delete ptr1;//
	delete ptr2;//若不构成重写的话，就会导致编译时决议，按照指针类型去析构了（若都是父类的指针），若子类有动态开辟的空间就内存泄露了

	//故子类若有管理空间，那么析构函数必须写成重写虚函数。否则将会内存泄露
	//这也是为什么有接口继承的原因，也是为什么析构函数要统一名字为destructor的原因

}

