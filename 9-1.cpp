#include <string>
using namespace std;

class A
{
public:
    A(int a = 10, int b = 20)
        :_a(a)
        ,_b(b)
    {}
private:
    int _a;
    int _b;
};
class Solution {
public:
    int FirstNotRepeatingChar(string str) {
        //数组改进，利用下标存储，再重新通过str的顺序找到第一个为1的字母，返回该数字的位置
        int *a = new int[123]();
        for (int i = 0; i < str.size(); i++)
        {
            a[str[i]]++;
        }
        //重新遍历数据
        for (int i = 0; i < str.size(); i++)
        {
            if (a[str[i]] == 1)
                return i;
        }
        return -1;
    }

    bool isUnique(string astr) {
        int* a = new int[123]();//int的底层构造是无参的
        for (int i = 0; i < astr.size(); i++)
        {
            a[astr[i]]++;
            if (a[astr[i]] == 2)
                return false;
        }
        return true;
    }
};



int main()
{
    Solution s;
    s.isUnique("abc");
    A* c = new A[21];//数组不能整体指定初始化，只能默认构造
}
