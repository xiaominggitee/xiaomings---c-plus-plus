#define _CRT_SECURE_NO_WARNINGS 1
//https://www.nowcoder.com/questionTerminal/8869d99cf1264e60a6d9eff4295e5bab

#include <string>
#include <iostream>
using namespace std;
void myreverse(string& str, int p1, int p2)//库中肯定是深拷贝，浅拷贝就直接代表原来string 了
{
	while (p1 < p2)
	{
		swap(str[p1], str[p2]);
		p1++;
		p2--;
	}
}
int main()
{
	string str;
	int p1 = 0;
	int p2 = p1;
	getline(cin, str);
	while (p1 < str.size())
	{
		while (p2 < str.size() && str[p2] != ' ')
		{
			p2++;
		}
		myreverse(str, p1, p2 - 1);
		p1 = p2 + 1;
		p2 = p1;
	}
	myreverse(str, 0, str.size() - 1);
	cout << str << endl;
	return 0;
}
