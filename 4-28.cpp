#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
//命名空间可以展开
//使用using

//namespace szm //命名空间（全局中定义），在自己的作用域中，命名空间中的变量依然是全局变量，他只是隔离了命名冲突
//{				//而他的生命周期依然是全局的
//
//	int add(int a, int b)
//	{
//		return a + b;
//	}
//	struct ListNode
//	{
//		int val;
//		struct ListNode* next;
//	};
//}
////using namespace szm;//先要有命名空间，才可以放开他的使用范围
//
//namespace czl//同一个工程里面（不管是否相同文件）的命名空间是会合并的，于是为了区分不同文件的命名空间，命名空间是支持嵌套的
//{
//	struct ListNode {
//		int val;
//		struct ListNode* next;
//	};
//	//namespace czl {//而嵌套的命名空间名字也可以相同，但是这样就不会合并，但是这样使用using放开就会有歧义
//	//	int a;
//	//}
//	namespace xiao {
//		int c = 0;
//	}
//}
////using namespace czl;		//using就说明把里面的命名放出来了
////using namespace xiao;
//using namespace czl::xiao;//使用这个就只是放开了czl中的xaio的空间
//
//int main()
//{
//	szm::ListNode* n1 = NULL;
//	czl::ListNode* n2 = NULL;
//
//	//c语言是struct，名字是属于不同命名空间的（域）,故结构体关键字写在前面，而c++里面结构体升级成类了，可以不写
//	struct szm::ListNode* n3 = NULL;
//	struct czl::ListNode* n4 = NULL;
//	
//	//czl::ListNode* n1 = NULL;				//同一个作用域的变量还是变量，而他们的类型不同但变量名称相同也是会冲突的
//	//czl::czl::ListNode* = NULL;    这样是不可以的，因为里面的命名空间就算相同也不会和外面的合并
//
//	//由于使用了两层using，就放开了指定命名空间变量的限定
//	//ListNode* a = NULL;
//	printf("%d", c);
//	int a = 0;
//	int c = 0;
//}


#include <stdlib.h>
namespace szm 
{
	int f = 0;
	int rand = 0;//库函数中有个rand
}
//
//int main()
//{
//	//f += 1;这样用没有定义f变量
//	szm::f += 1;
//	//printf("%d ", f);//这个f也要用命名空间去区分，因为main局部中没有
//	printf("%d ", szm::f);
//}


////但是全部放出来的话rand也要写成szm::rand,
////为了区分可以用什么放什么
////using namespace szm;
////f是变量，直接用就行，而上面是使用命名空间里面的命名空间，他前提是命名空间，就要使用using namespace czl::xiao
//using szm::f;
//int main()
//{
//	f += 1;
//	printf("%d ", f);
//	return 0;
//}


using namespace std;	//std 是c++库中的命名空间，包含大量关键字，全放虽然方便，但是对重名的关键字有也是有冲突风险
//故为了安全可以把常用的放出来
using std::cout;//因为cout不是命名空间，放的是关键字（函数），不能写namespace
//using std::cout,endl;//不能连着放，只能一个一个放

//
//int main()
//{
//	int a;
//	double b;
//
//
//	//流提取运算符
//	cin >> a;
//	//流插入运算符,经过
//	cout << a;
//}
