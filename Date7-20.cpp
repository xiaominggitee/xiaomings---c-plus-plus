#define _CRT_SECURE_NO_WARNINGS 1
#include "Date7-21.h"


//流插入和流提取运算符重载

//流提取
//由于this指针的顺序是固定的，我们需要用cout这对象去调用，cout.operator>>(const& Date d),故写成友元函数
//但是又不能去修改iostream类里面的函数，故写在外面用operator>>(std::ostream out ,const& Date d)
std::ostream& operator<<(std::ostream& out, const Date& d)
{
	out << d._year << "-" << d._month << "-" << d._day << endl;
	return out;
}
//流插入
std::istream& operator>>(std::istream& in, Date& d)
{
	
	in >> d._year >> d._month >> d._day;
	assert(d._year > 0 || d._month > 0 || d._month <= 12 || d._day <= d.Getmonthsday(d._year, d._month) || d._day > 0);//假则报错
	return in;
}

void test1()
{
	Date d1;
	Date d2(2022, 5, 20);

	Date d3(d1);
	Date d4 = 2022;//隐式类型转换，再拷贝构造

	Date d5 = (2021, 4, 3, 5);//逗号表达式，year变5
}

void test2()
{
	Date d1;
	Date d2(2022, 05, 20);
	Date d3(2022,05,21);
	d1.Print();
	d2.Print();
	d3.Print();
	cout << (d2 >= d3) << endl;


	(d2 += 10000).Print();
	(d2 -= 10000).Print();
}

void test3()
{
	Date d1;
	cout << d1;
	cin >> d1;
	cout << d1;
	cout << "day" << endl;
}

int main()
{
	test3();

}