package sx;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
//import java.util.EventListener;
import java.sql.*;

//登录页面
public class DLFrame extends JFrame implements ActionListener,ItemListener{
	JPanel p1,p2,p3;
	Font f1,f2,f3,f4;
   
    JLabel head1=new JLabel("用户登录  ");
  
	JLabel usename=new JLabel("用户ID：");
	JTextField usenametext=new JTextField(10);
	
	JLabel password=new JLabel("密 码：");
	JPasswordField txtPwd=new JPasswordField(10);
	
	JLabel role=new JLabel("角色：");
	JComboBox boxrole=new JComboBox();
	
	//创建三个选择按钮
	JButton a=new JButton("登录");
	JButton b=new JButton("重置");
	JButton c=new JButton("取消");
	
	//连接数据库和调用数据库
	Connection conn;
	Statement stmt;
	ResultSet rs;
	
	//定义连接字符
//	String driver=com.mysql.cj.jdbc.Driver;
	String dbURL = "jdbc:mysql://localhost:3306/教务系统?useSSL=false";
	String userName = "root";
	String userPwd = "Ss3255155.";
	
	int index=0;
	String index1;
	
	static int ok=1;
	static int cancel=0;
	int actionCode=0;
	
	public DLFrame(){        
		super("物联网20-1苏子铭");
		setLayout(new FlowLayout());
		p1=new JPanel();
		p2=new JPanel();
		p3=new JPanel();
		f1=new Font("宋体",Font.BOLD,30);
	    
	    //设置面板1的标签
	    head1.setFont(f1);
	    head1.setForeground(Color.BLACK);
	    p1.setBackground(null);
	    p1.setOpaque(false);
		p1.add(head1);

	    //面板2为4行2列的网格布局管理器
	    p2.setLayout(new GridLayout(4,2));

	    //下拉列表中添加数据
	    boxrole.addItem("管理员");
	    boxrole.addItem("教师");
	    boxrole.addItem("学生");
	    
	    p2.setBackground(null);
	    p2.setOpaque(false);
	    boxrole.setOpaque(false);
	    usenametext.setOpaque(false);
	    txtPwd.setOpaque(false);
	    p2.add(role);
	    p2.add(boxrole);
	    p2.add(usename);
	    p2.add(usenametext);
	    p2.add(password);
	    p2.add(txtPwd);	   	    
	
	    //将3个按钮添加进面板3中

	    p3.setBackground(null);
	    p3.setOpaque(false);//设置透明
	    a.setContentAreaFilled(false);
	    b.setContentAreaFilled(false);
	    c.setContentAreaFilled(false);
	    
	    p3.add(a);
	    p3.add(b);
	    p3.add(c);
	    
	    //将三个面板添加进框架容器中
	    this.add(p1);
	    this.add(p2);
	    this.add(p3);
    
	    //设置顶层容器的大小、位置、可见性及close功能
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setSize(350,300);
	    setLocationRelativeTo(null);
	    setVisible(true);
	    
	    //注册事件监听器

	    boxrole.addItemListener(this);  
	    a.addActionListener(this);
	    b.addActionListener(this);
	    c.addActionListener(this);
	}
	
	public void connDB() {   //连接数据库方法

		try {
			//连接数据库
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(dbURL,userName,userPwd);
			stmt=conn.createStatement();
		}catch(Exception e) {
			e.printStackTrace();

		}
	}
	
	public void closeDB() {  //关闭数据库方法
		try {
			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		
		}
	}
	
	//对下拉列表进行事件处理
	public void itemStateChanged(ItemEvent e) {
		if(e.getStateChange()==ItemEvent.SELECTED) {  
			JComboBox j=(JComboBox)e.getSource();  
			index=j.getSelectedIndex();
		}
	}

	//该方法用来确认是否在数据库中找到学号
	public boolean searchsno(String str) {
		boolean x=false;
		this.connDB();
		try {
			rs=stmt.executeQuery("select * from 学生表");
			while(rs.next()) {
				if(rs.getString("学号").trim().equals(str)) {
					x=true;
				}
			}

		}catch(Exception e) {
			e.printStackTrace();
		}
		return x;
	}
	
	//该方法用来确认是否在数据库中找到教师号
	public boolean searchtno(String str) {
		boolean x=false;
		this.connDB();
		try {
			rs=stmt.executeQuery("select * from 教师表");
			while(rs.next()) {
				if(rs.getString("教师号").trim().equals(str)) {  
					x=true;
				}
			}
	
		}catch(Exception e) {
			e.printStackTrace();
		}
		return x;
	}
	
	//该方法用来确认是否能在数据库中找到管理员ID
	public boolean searchmanagerno(String str) {//比对管理员列表中比对是否存在该管理员
		boolean x=false;
		this.connDB();
		try {
			rs=stmt.executeQuery("select * from 管理员");
			while(rs.next()) {
				if(rs.getString("管理员号").trim().equals(str)) {  
					x=true;
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return x;
	}
	
	//对三个按钮进行事件处理
	public void actionPerformed(ActionEvent e){
		Object source=e.getSource();
//		String un;
		String pw;
		boolean success= false;  
		if(source==a) {  
			if(usenametext.getText().equals("") || txtPwd.getText().equals("")) {  
				
				JOptionPane.showMessageDialog(null,"登录名和密码不能为空！");
			}else {
				this.connDB();
				try {
					if(index==0) {  //管理员登录，根据管理员的id判断密码
						
						if(!searchmanagerno(usenametext.getText().trim())) {
							JOptionPane.showMessageDialog(null,"对不起，此用户不存在！请重新登录!");
							usenametext.setText("");
							txtPwd.setText("");
						}else {
							rs=stmt.executeQuery("select * from 管理员  where 管理员号="+ usenametext.getText().trim());
							while(rs.next()) {
								pw=rs.getString("管理员密码").trim();//指定列的值，设置一个管理员号，管理员密码，密码在第二列
								if(txtPwd.getText().equals(pw)) {			//核对密码
									JOptionPane.showMessageDialog(null,"管理员登录成功！");
									this.setVisible(false);
									new GLFrame();  //进入管理员界面
								}else {
									JOptionPane.showMessageDialog(null,"密码错误！请重试");
									txtPwd.setText("");
								}
							}
						}
					}

					if(index==1) {  //教师登录
						if(!searchtno(usenametext.getText().trim())) {
							JOptionPane.showMessageDialog(null,"对不起，此用户不存在！请重新登录!");
							usenametext.setText("");
							txtPwd.setText("");
						}else {
							rs=stmt.executeQuery("select * from 教师表  where 教师号="+ usenametext.getText().trim());
							while(rs.next()) {
								pw=rs.getString("登录密码").trim();
								if(txtPwd.getText().equals(pw)) {
									JOptionPane.showMessageDialog(null,"教师登录成功！");
									this.setVisible(false);
									new TeacherFrame(usenametext.getText());  //进入教师界面
								}
								else {
									JOptionPane.showMessageDialog(null,"密码错误！请重试");
									txtPwd.setText("");
								}
							}
						}
					}
					if(index==2) {  //学生登录
						if(!searchsno(usenametext.getText().trim())) {
							JOptionPane.showMessageDialog(null,"对不起，此用户不存在！请重新登录!");
							usenametext.setText("");
							txtPwd.setText("");
						}else {
							rs=stmt.executeQuery("select * from 学生表  where 学号="+ usenametext.getText().trim());
							while(rs.next()) {
								pw=rs.getString("密码").trim();
								if(txtPwd.getText().equals(pw)) {
									JOptionPane.showMessageDialog(null,"学生登录成功！");
									this.setVisible(false);
									new StudentFrame(usenametext.getText());  //进入学生界面
								}
								else {
									JOptionPane.showMessageDialog(null,"密码错误！请重试");
									txtPwd.setText("");
								}
							}
						}
					}
				}catch(Exception e1) {
					e1.printStackTrace();
	
				}
				closeDB();
			}
		}
		if(source==b) {   //如果事件源是“重置”按钮
			usenametext.setText("");    
			txtPwd.setText("");
		}
		if(source==c) {   //如果事件源是“取消”按钮
		    //System.exit(0);
			this.dispose();
			new DLFrame();
		}
	}
	
	public static void main(String[] args){
		new DLFrame();
	}
}