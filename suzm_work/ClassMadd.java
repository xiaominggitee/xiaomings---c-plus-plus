package sx;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.sql.*;

public class ClassMadd extends JFrame implements ActionListener{
	//定义面板
	Panel p=new Panel();
	Panel p1=new Panel();
	Panel p2=new Panel();

	String num;
	JLabel csno=new JLabel("班级:");
	JTextField csno1=new JTextField(10);
//	JLabel cnum=new JLabel("人数");
//	JTextField cnum1=new JTextField(20);
	JLabel cm=new JLabel("班主任:");
	JTextField cm1=new JTextField(20);
	//定义字体
	Font f1=new Font("宋体",Font.BOLD,20);

    //定义按钮
    JButton btnsure=new JButton("确定");
    JButton btnagain=new JButton("重置");
    JButton btncancel=new JButton("取消");
    
    //定义连接字符
    String dbURL="jdbc:mysql://localhost:3306/教务系统?useSSL=false";
    String userName="root";
    String userPwd="Ss3255155.";
  	Connection conn;
  	Statement stmt;
  	ResultSet rs;
  	
  	public void connDB() {   //连接数据库方法
		try {
			//连接数据库
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(dbURL,userName,userPwd);
			stmt=conn.createStatement();
		}catch(Exception e) {
			e.printStackTrace();
			
		}
	}
	
	public void closeDB() {  //关闭数据库方法
		try {
			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		
		}
	}
	
	public ClassMadd() {  //构造方法
		super("班级信息添加页面");
		setLayout(new FlowLayout());
		
		//设置字体
		csno.setFont(f1);
//		cnum.setFont(f1);
		cm.setFont(f1);
		//按钮字体、颜色
		btnsure.setFont(f1);
		btnsure.setBackground(new Color(131,175,155)); //淡草色
		btncancel.setFont(f1);
		btncancel.setBackground(new Color(131,175,155)); //淡草色
		btnagain.setFont(f1);
		btnagain.setBackground(new Color(100,175,155)); //淡草色
		//设置面板的网格布局管理
		p1.setLayout(new GridLayout(6,10));
		//将标签和文本框添加进面板
		p1.add(csno);
		p1.add(csno1);
//		p1.add(cnum);
//		p1.add(cnum1);
		p1.add(cm);
		p1.add(cm1);
		
		
		//将按钮添加进面板
		p.add(btnsure);
		p.add(btnagain);
		p.add(btncancel);
		
		//添加面板
		this.add(p1);
		this.add(p);
		
		//设置顶层容器的大小、位置、可见性及close功能
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setSize(500,350);
	    setLocationRelativeTo(null);
	    setVisible(true);
	    
	    //注册监听器
	    btnsure.addActionListener(this);
	    btnagain.addActionListener(this);
	    btncancel.addActionListener(this);
	}
	
	public ClassMadd(String s1,String s2,String s3) {
		super("班级信息修改页面");
		setLayout(new FlowLayout());
		
		csno1.setText(s1);
		csno1.setEditable(false);
//		cnum1.setText(s2);
		
		//设置字体
		csno.setFont(f1);
//		cnum.setFont(f1);
		cm.setFont(f1);
		//按钮字体、颜色
		btnsure.setFont(f1);
		btnsure.setBackground(new Color(131,175,155)); //淡草色
		btncancel.setFont(f1);
		btncancel.setBackground(new Color(131,175,155)); //淡草色
		btnagain.setFont(f1);
		btnagain.setBackground(new Color(131,175,155)); //淡草色
		//设置面板的网格布局管理
		p1.setLayout(new GridLayout(6,2));
		//将标签和文本框添加进面板
		p1.add(csno);
		p1.add(csno1);
//		p1.add(cnum);
//		p1.add(cnum);
		p1.add(cm);
		p1.add(cm1);
		
		//将按钮添加进面板
		p.add(btnsure);
		p.add(btnagain);
		p.add(btncancel);
		
		//添加面板
		this.add(p1);
		this.add(p);
		
		//设置顶层容器的大小、位置、可见性及close功能
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setSize(500,350);
	    setLocationRelativeTo(null);
	    setVisible(true);
	    
	    //注册监听器
	    btnsure.addActionListener(this);
	    btnagain.addActionListener(this);
	    btncancel.addActionListener(this);
	}

	//计算当前班级的人数
	public void getnum()
	{
		try{
			this.connDB();
			rs = stmt.executeQuery("SELECT count(班级) from 学生表 where 班级="+csno1.getText().trim());
			if(rs.next())
				num=rs.getString(1);
			this.dispose();
		}catch(Exception e) {
			e.printStackTrace();
		}

	}

	public void insert() {  //插入方法
		try {
			this.connDB();
			getnum();
			stmt.executeUpdate("insert into 班级表  values("+csno1.getText().trim()+","+"'"+num.trim()+"'"+","+cm1.getText().trim()+")");
			JOptionPane.showMessageDialog(null,"班级信息添加成功！");
		    this.dispose();
		    new ClassM().display();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	//用来寻找班级是否存在
	public boolean searchtest(String str) {
  		boolean x=false;
  		this.connDB();
  		try {
  			rs=stmt.executeQuery("select * from 班级表");
  			while(rs.next()) {
  				if(rs.getString("班级").trim().equals(str)) { 
  					x=true;
  				}
  			}
  		}catch(Exception e) {
  			e.printStackTrace();
  		}
  		return x;
  	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btnsure) {
			if(csno1.getText().equals("") ||  cm1.getText().equals("") ) {
				JOptionPane.showMessageDialog(null,"信息不能为空！");
			}else if(searchtest(csno1.getText())){
				JOptionPane.showMessageDialog(null,"该班级已存在，请重新添加！");
				csno1.setText("");
//				cnum1.setText("");
				cm1.setText("");
			}else {
				this.insert();
			}

		}
		if(e.getSource()==btnagain) {
			csno1.setText("");
//			cnum1.setText("");
			cm1.setText("");
		}
		if(e.getSource()==btncancel) {
			this.dispose();
			new ClassM().display();;
		}
	}


	public static void main(String[] args) {
		new ClassMadd();
    }

}