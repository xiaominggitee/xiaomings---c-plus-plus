package sx;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;

public class TM extends JFrame implements ActionListener,ItemListener{
	//定义按钮，添加进JMenuBar
	JButton btnadd=new JButton("添加");
	JButton btndelete=new JButton("删除");
	JButton btnupdate=new JButton("修改");
	JButton btnsearch=new JButton("查询");
	JButton btndisplay=new JButton("刷新显示");
	JButton btnreturn=new JButton("返回");
	//定义菜单栏
	JMenuBar mb=new JMenuBar();
	//定义字体
	Font f1=new Font("宋体",Font.BOLD,30);
	
	
	JTable tb;
	JTable tb1;
	JScrollPane jsp;
	JScrollPane scroll1;
	
	Connection conn;
	Statement stmt;
	ResultSet rs;
	
	//定义连接字符串
  	String dbURL = "jdbc:mysql://localhost:3306/教务系统?useSSL=false";
  	String username = "root";
  	String userpwd = "Ss3255155.";
	
	Object[][] arr;
	//连接数据库
	public void connDB(){  //连接数据库方法
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(dbURL,username,userpwd);
			stmt=conn.createStatement();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeDB() {  //关闭数据库方法
		try {
			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public TM() {  
		super("教师信息管理系统");
		//设置按钮颜色和字体
		btnadd.setFont(f1);
		btnadd.setBackground(new Color(131,175,155));
		btndelete.setFont(f1);
		btndelete.setBackground(new Color(131,175,155));
		btnupdate.setFont(f1);
		btnupdate.setBackground(new Color(131,175,155));
		btnsearch.setFont(f1);
		btnsearch.setBackground(new Color(131,175,155));
		btndisplay.setFont(f1);
		btndisplay.setBackground(new Color(131,175,155));
		btnreturn.setFont(f1);
		btnreturn.setBackground(new Color(131,175,155));
		
		//将按钮添加进菜单栏中
		mb.add(btnadd);
		mb.add(btndelete);
		mb.add(btnupdate);
		mb.add(btnsearch);
		mb.add(btndisplay);
		mb.add(btnreturn);
		
		//连接数据库
		this.connDB();
		//给按钮注册监听器
		btnadd.addActionListener(this);
		btndelete.addActionListener(this);
		btnupdate.addActionListener(this);
		btnsearch.addActionListener(this);
		btndisplay.addActionListener(this);
		btnreturn.addActionListener(this);
		
		setSize(650,300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		this.setJMenuBar(mb);
	}
	
	public void display() {
		//this.connDB();
		String sql="select * from 教师表";
		ArrayList ar=new ArrayList();  
		int i=0,j=0;
		try {
			rs=stmt.executeQuery(sql);
			while(rs.next()) {
				ar.add(rs.getString("教师号"));
				ar.add(rs.getString("教师姓名"));
				ar.add(rs.getString("性别"));
				ar.add(rs.getString("登录密码"));
				i++;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		arr=new Object[i][7];
		String[] list= {"教师号","教师姓名","性别","登录密码"};
		try {
			rs=stmt.executeQuery("select * from 教师表 order by 教师号");
			while(rs.next()) {
				arr[j][0]=rs.getString("教师号");
				arr[j][1]=rs.getString("教师姓名");
				arr[j][2]=rs.getString("性别");
				arr[j][3]=rs.getString("登录密码");
				j++;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		tb=new JTable(arr,list);
		jsp=new JScrollPane(tb);
		this.add(jsp);
	}
	
	public void delete() {   
		String tno=null;  
		int row=tb.getSelectedRow();  
		if(row==-1) {  
			JOptionPane.showMessageDialog(null,"请选择要删除的记录！");
		}else {
			int x=0;
			try {
				rs=stmt.executeQuery("select * from 教师表");
				while(rs.next() && x<=row) {
					tno=rs.getString("教师号");
					x++;
				}
				stmt.executeUpdate("delete  from 课程表  where 教师号="+tno);
				stmt.executeUpdate("delete  from 教师表  where 教师号="+tno);  
				JOptionPane.showMessageDialog(null,"删除成功！");
				this.dispose();
				new TM().display();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
			
	public void update() {
		String sno1=null; 
		int row=tb.getSelectedRow();  
		if(row==-1) {
			JOptionPane.showMessageDialog(null,"请选择要修改的信息！");
		}else {
			int x=0;
			try {
				rs=stmt.executeQuery("select * from 教师表");
				while(rs.next() && x<=row) {
					sno1=rs.getString("教师号");
					x++;
				}
				this.dispose();
				new TMupdate(sno1);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void show(String str) {   //查询结果方法
		JFrame f=new JFrame("查询结果");
		f.setLayout(new FlowLayout());
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setSize(500,300);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		JButton btnrt=new JButton("返回");
		btnrt.setFont(f1);
		btnrt.setBackground(new Color(131,175,155));

		this.connDB();
		arr=new Object[1][7];
		try {
			rs=stmt.executeQuery("select * from 教师表  where 教师号="+str);
			while(rs.next()) {
				arr[0][0]=rs.getString("教师号");
				arr[0][1]=rs.getString("教师姓名");
				arr[0][2]=rs.getString("性别");
				arr[0][3]=rs.getString("登录密码");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String[] list= {"教师号","教师姓名","性别","登录密码"};
		tb1=new JTable(arr,list); 
		scroll1=new JScrollPane(tb1);

		f.add(btnrt);
		f.add(scroll1); 

		btnrt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
			}
		});
	}
	
	//该方法用来确认是否在数据库中找到教师号
	public boolean searchtest(String str) {
		boolean x=false;
		this.connDB();
		try {
			rs=stmt.executeQuery("select * from 教师表");
			while(rs.next()) {
				if(rs.getString("教师号").trim().equals(str)) {  
					x=true;
				}
			}

		}catch(Exception e) {
			e.printStackTrace();
		}
		return x;
	}
	
	//查找方法
	public void search() {  
		JFrame f=new JFrame("查询");
		f.setLayout(new FlowLayout());
		f.setSize(240,180);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		JLabel stuno=new JLabel("输入教师号：");
		JTextField stuno1=new JTextField(10);
		JButton ok=new JButton("确定");
		JButton cancel=new JButton("取消");
		p1.add(stuno);
		p1.add(stuno1);
		p2.add(ok);
		p2.add(cancel);
		f.add(p1);
		f.add(p2);
		//为组件注册监听器
		ok.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(stuno1.getText().equals("")) {
					JOptionPane.showMessageDialog(null,"请输入教师号");
				}else {
					if(!searchtest(stuno1.getText().trim())) {
						f.dispose();
						JOptionPane.showMessageDialog(null,"对不起，该教师不存在！");
					}else {
					    f.dispose();
				
					    show(stuno1.getText());
					}
				}
			}
		});
		cancel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				
			}
		});
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				Window w=(Window)e.getComponent();  
				w.dispose();
			}
		});
	}
	
	public void itemStateChanged(ItemEvent e) {
		
	}
	
    public void actionPerformed(ActionEvent e) {
    	if(e.getSource()==btnadd) {
    		this.dispose();
    		new TMadd();
    	}
    	if(e.getSource()==btndelete) {
    		JOptionPane.showMessageDialog(null,"删除操作，谨慎操作！");
    		this.delete();
    	}
    	if(e.getSource()==btnupdate) {
    		this.update(); 
    	}
    	if(e.getSource()==btnsearch) {
    		this.search();
    	}
    	if(e.getSource()==btndisplay) {
    		this.dispose();
    		new TM().display();
    	}
    	if(e.getSource()==btnreturn) {
    		this.dispose();
        	new GLFrame();
    	}
    }
    
	public static void main(String[] args) {
		new TM();
	}	
}