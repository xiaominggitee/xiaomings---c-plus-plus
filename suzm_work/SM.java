package sx;

import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class SM extends JFrame implements ActionListener{   //学生管理系统
	//定义面板
	//创建5个按钮
	JButton btnAdd=new JButton("添加");
	JButton btnDelete=new JButton("删除");
	JButton btnUpdate=new JButton("修改");
	JButton btnSearch=new JButton("查询");
	JButton btnDisplay=new JButton("刷新显示");
	JButton btnCancel=new JButton("返回");
	//设置字体类型
	Font f2=new Font("宋体",Font.BOLD,30);

	//创建菜单栏
	JMenuBar mb=new JMenuBar();
	//定义表格
	JTable stable;
	JTable stable1;
	//定义滚动栏
	JScrollPane scroll;
	JScrollPane scroll1;
	
	//定义连接数据库对象
	//连接数据库和调用数据库
	Connection conn;
	Statement stmt;
	ResultSet rs;
		
	//定义连接字符
  	String dbURL = "jdbc:mysql://localhost:3306/教务系统?useSSL=false";
  	String userName = "root";
  	String userPwd = "Ss3255155.";
	

	Object[][] arr; 
	
	public void connDB() {   //连接数据库方法
		try {
			//连接数据库
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(dbURL,userName,userPwd);
			stmt=conn.createStatement();
		}catch(Exception e) {
			e.printStackTrace();
			
		}
	}
	
	public void closeDB() {  //关闭数据库方法
		try {
			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public SM() {
		super("学生信息管理页面");
		//布局管理
		//设置字体
		btnAdd.setFont(f2);
		btnAdd.setBackground(new Color(131,175,155)); //淡草色
		btnDelete.setFont(f2);
		btnDelete.setBackground(new Color(131,175,155));
		btnUpdate.setFont(f2);
		btnUpdate.setBackground(new Color(131,175,155));
		btnSearch.setFont(f2);
		btnSearch.setBackground(new Color(131,175,155));
		btnDisplay.setFont(f2);
		btnDisplay.setBackground(new Color(131,175,155));
		btnCancel.setFont(f2);
		btnCancel.setBackground(new Color(131,175,155));
		
		//将按钮添加进菜单栏中，与直接放进面板的按钮不同
		mb.add(btnAdd);
		mb.add(btnDelete);
		mb.add(btnUpdate);
		mb.add(btnSearch);
		mb.add(btnDisplay);
		mb.add(btnCancel);
		
		//连接数据库
		this.connDB();
		
		//注册时间监听器
		btnAdd.addActionListener(this);
		btnDelete.addActionListener(this);
		btnUpdate.addActionListener(this);
		btnSearch.addActionListener(this);
		btnDisplay.addActionListener(this);
		btnCancel.addActionListener(this);

		//设置窗口的大小和位置
		setSize(650,300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setJMenuBar(mb); 
		this.setVisible(true);
	}
	
	
	public void display() {  //显示所有学生的信息
		int i=0,j=0,k=0;
	
		ArrayList al=new ArrayList();
		

		try {
			rs=stmt.executeQuery("select * from 学生表  ");
			while(rs.next()) {  
			
				al.add(rs.getString("学号").trim());
				al.add(rs.getString("姓名").trim());
				al.add(rs.getString("性别").trim());
				al.add(rs.getInt("年龄"));
				al.add(rs.getString("班级").trim());
				al.add(rs.getString("密码").trim());
				i++;  
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		//定义二维数组
		arr=new Object[i][6];
	
		String[] columnNames= {"学号","姓名","性别","年龄","班级","密码"};

		try {
			rs=stmt.executeQuery("select * from 学生表  order by 学号");
			while(rs.next()) {   
				arr[j][0]=rs.getString("学号").trim();
				arr[j][1]=rs.getString("姓名").trim();
				arr[j][2]=rs.getString("性别").trim();
				arr[j][3]=rs.getInt("年龄");
				arr[j][4]=rs.getString("班级").trim();
				arr[j][5]=rs.getString("密码").trim();
				j++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		stable=new JTable(arr,columnNames); 
		scroll=new JScrollPane(stable);
		this.add(scroll);  
	}
	
	public void delete() {   //删除信息方法
		String sno=null;  
		int row=stable.getSelectedRow();  
		if(row==-1) { 
			JOptionPane.showMessageDialog(null,"请选择要删除的记录！");
		}else {
			int x=0;
			try {
				rs=stmt.executeQuery("select * from 学生表");
				while(rs.next() && x<=row) {
					sno=rs.getString("学号");
					x++;
				}
				stmt.executeUpdate("delete  from 学生表  where 学号="+sno);  
				stmt.executeUpdate("delete  from 成绩表  where 学号="+sno);
				JOptionPane.showMessageDialog(null,"删除成功！");
				this.dispose();
				new SM().display();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void update() {
		String sno1=null; 
		int row=stable.getSelectedRow();  
		if(row==-1) {  
			JOptionPane.showMessageDialog(null,"请选择要修改的信息！");
		}else {
			int x=0;
			try {
				rs=stmt.executeQuery("select * from 学生表");
				while(rs.next() && x<=row) {
					sno1=rs.getString("学号");
					x++;
				}
				this.dispose();
				new SMupdate(sno1);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	public void show(String str) {   //查询结果方法
		JFrame f1=new JFrame("查询结果");
		f1.setLayout(new FlowLayout());
		f1.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f1.setSize(500,300);
		f1.setVisible(true);
		f1.setLocationRelativeTo(null);
		JButton btnrt=new JButton("返回");
		btnrt.setFont(f2);
		btnrt.setBackground(new Color(131,175,155));

		this.connDB();
		arr=new Object[1][6];
		try {
			rs=stmt.executeQuery("select * from 学生表  where 学号="+str);
			while(rs.next()) {
				arr[0][0]=rs.getString("学号");
				arr[0][1]=rs.getString("姓名");
				arr[0][2]=rs.getString("性别");
				arr[0][3]=rs.getInt("年龄");
				arr[0][4]=rs.getString("班级");
				arr[0][5]=rs.getString("密码");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String[] columnNames= {"学号","姓名","性别","年龄","班级","密码"};
		stable1=new JTable(arr,columnNames); 
		scroll1=new JScrollPane(stable1);
		f1.add(btnrt);
		f1.add(scroll1);  
		btnrt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f1.dispose();
			}
		});
	}
	
	//该方法用来确认是否在数据库中找到学号
	public boolean searchtest(String str) {
		boolean x=false;
		this.connDB();
		try {
			rs=stmt.executeQuery("select * from 学生表");
			while(rs.next()) {
				if(rs.getString("学号").trim().equals(str)) {
					x=true;
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return x;
	}
	
	public void search() {  
		JFrame f=new JFrame("查询");
		f.setLayout(new FlowLayout());
		f.setSize(240,180);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		JLabel stuno=new JLabel("输入学号：");
		JTextField stuno1=new JTextField(10);
		JButton ok=new JButton("确定");
		JButton cancel=new JButton("取消");
		p1.add(stuno);
		p1.add(stuno1);
		p2.add(ok);
		p2.add(cancel);
		f.add(p1);
		f.add(p2);
		//为组件注册监听器
		ok.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(stuno1.getText().equals("")) {
					JOptionPane.showMessageDialog(null,"请输入学号");
				}else {
					if(!(searchtest(stuno1.getText().trim()))) {
						f.dispose();
						JOptionPane.showMessageDialog(null,"对不起，该学生不存在！");
					}else {
					    f.dispose();            
					    show(stuno1.getText());
					}
				}
			}
		});
		cancel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
	
			}
		});
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				Window w=(Window)e.getComponent();  
				w.dispose();
			}
		});
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btnAdd) {
			this.dispose();
			new SMadd();
		}
		if(e.getSource()==btnDelete) {
            JOptionPane.showMessageDialog(null,"删除，请谨慎操作！");
			this.delete();
		}
        if(e.getSource()==btnUpdate) {
			this.update(); 
		}
        if(e.getSource()==btnSearch) {
        	this.search(); 
        }
        if(e.getSource()==btnDisplay) {
        	this.dispose();
        	new SM().display();
        }
        if(e.getSource()==btnCancel) {
        	this.dispose();
        	new GLFrame();
        }
	}

	public static void main(String[] args) {
		new SM().display();
	}

}