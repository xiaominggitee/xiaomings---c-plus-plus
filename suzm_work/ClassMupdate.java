package sx;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.sql.*;

public class ClassMupdate extends JFrame implements ActionListener{
	//定义面板
	Panel p=new Panel();
	Panel p1=new Panel();
	Panel p2=new Panel();

	JLabel csno=new JLabel("班级:");
	JTextField csno1=new JTextField(10);
	JLabel csname=new JLabel("人数:");
	JTextField csname1=new JTextField(20);
//	JTextField sum1=new JTextField(10);
	JLabel cm=new JLabel("班主任:");
	JTextField cm1=new JTextField(20);
	//定义字体
	Font f1=new Font("宋体",Font.BOLD,20);

    //定义按钮
    JButton btnsure=new JButton("确定");
    JButton btnagain=new JButton("重置");
    JButton btncancel=new JButton("取消");
    
    //定义连接字符
    String dbURL="jdbc:mysql://localhost:3306/教务系统?useSSL=false";
    String userName="root";
    String userPwd="Ss3255155.";
  	Connection conn;
  	Statement stmt;
  	ResultSet rs;
  	
  	public void connDB() {   //连接数据库方法
		try {
			//连接数据库
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(dbURL,userName,userPwd);
			stmt=conn.createStatement();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeDB() {  //关闭数据库方法
		try {
			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		
		}
	}
	
	public ClassMupdate(String s1,String s2) {
		super("班级信息修改页面");
		setLayout(new FlowLayout());
		
		csno1.setText(s1);//接收并获取
//		csno1.setEditable(false);
//		csname1.setText(s2);
//		csname1.setEditable(false);
		cm1.setText(s2);

		//设置字体
		csno.setFont(f1);
		csname.setFont(f1);
		cm.setFont(f1);
		//按钮字体、颜色
		btnsure.setFont(f1);
		btnsure.setBackground(new Color(131,175,155)); //淡草色
		btncancel.setFont(f1);
		btncancel.setBackground(new Color(131,175,155)); //淡草色
		btnagain.setFont(f1);
		btnagain.setBackground(new Color(131,175,155)); //淡草色
		//设置面板的网格布局管理
		p1.setLayout(new GridLayout(6,2));
	
		//将标签和文本框添加进面板
		p1.add(csno);
		p1.add(csno1);
//		p1.add(csname);
//		p1.add(csname1);
//		p1.add(sum);
//		p1.add(sum1);
		p1.add(cm);
		p1.add(cm1);
		
		//将按钮添加进面板
		p.add(btnsure);
		p.add(btnagain);
		p.add(btncancel);
		
		//添加面板
		this.add(p1);
		this.add(p);
		
		//设置顶层容器的大小、位置、可见性及close功能
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setSize(500,350);
	    setLocationRelativeTo(null);
	    setVisible(true);
	    
	    //注册监听器
	    btnsure.addActionListener(this);
	    btnagain.addActionListener(this);
	    btncancel.addActionListener(this);
	}
	
	public void delete(String s) {   //删除信息方法
		try {
			
			stmt.executeUpdate("delete  from 班级表  where 班级="+"'"+s+"'");
			
		}catch(Exception e) {
			e.printStackTrace();
		}
    }
	
	public void insert() {  //插入方法
		try {
			this.connDB();
			//插入数据
			PreparedStatement ps = null;
			String sql = "update 班级表 班主任=? where 班级= ?";
			ps = conn.prepareStatement(sql);
//			ps.setString(1, csname1.getText().trim());
			ps.setString(1, cm1.getText().trim());
			ps.setString(2, csno1.getText().trim());

			ps.executeUpdate();
//			stmt.executeUpdate("insert into 班级表  values("+"'"+csno1.getText().trim()+"'"+","+"'"+csname1.getText().trim()+"'"+","+","+","+sum1.getText().trim()+","+"'"+cm1.getText().trim()+"'"+")");
	
			JOptionPane.showMessageDialog(null,"班级信息添加成功！");
			ps.close();
		    this.dispose();
		    new ClassM().display();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btnsure) {			//sum1.getText().equals("") ||
			if(csno1.getText().equals("") ||  cm1.getText().equals("") ) {
				JOptionPane.showMessageDialog(null,"信息不能为空！");
			}else {
				this.delete(csno1.getText());
				this.insert();
			}
		}
		if(e.getSource()==btnagain) {
			csno1.setText("");
//			csname1.setText("");
//			sum1.setText("");
			cm1.setText("");
		}
		if(e.getSource()==btncancel) {
			this.dispose();
			new ClassM().display();;
		}
	}


	public static void main(String[] args) {
		new ClassMupdate("","");
    }
}