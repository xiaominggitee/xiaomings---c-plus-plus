package sx;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.sql.*;
import java.util.ArrayList;

public class SCMadd extends JFrame implements ActionListener{
	//定义面板
	Panel p=new Panel();
	Panel p1=new Panel();
	Panel p2=new Panel();
	
	//定义属性标签及文本框
	JLabel sno=new JLabel("学号:");
	JTextField sno1=new JTextField(10);
	JLabel cno=new JLabel("课程号:");
	JTextField cno1=new JTextField(20);
	JLabel sc=new JLabel("成绩:");
	JTextField sc1=new JTextField(10);
	//定义字体
	Font f1=new Font("宋体",Font.BOLD,20);

    //定义按钮
    JButton btnsure=new JButton("确定");
    JButton btnagain=new JButton("重置");
    JButton btncancel=new JButton("取消");
    
    //定义连接字符
  	String dbURL = "jdbc:mysql://localhost:3306/教务系统?useSSL=false";
  	String userName = "root";
  	String userPwd = "Ss3255155.";
  	Connection conn;
  	Statement stmt;
  	ResultSet rs;
  	
  	public void connDB() {   //连接数据库方法
		try {
			//连接数据库
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(dbURL,userName,userPwd);
			stmt=conn.createStatement();
		}catch(Exception e) {
			e.printStackTrace();

		}
	}
	
	public void closeDB() {  //关闭数据库方法
		try {
			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
	
		}
	}
	
	public SCMadd() {  
		super("成绩信息添加页面");
		setLayout(new FlowLayout());
		
		//设置字体
		sno.setFont(f1);
		cno.setFont(f1);
		sc.setFont(f1);
		//按钮字体、颜色
		btnsure.setFont(f1);
		btnsure.setBackground(new Color(131,175,155)); 
		btncancel.setFont(f1);
		btncancel.setBackground(new Color(131,175,155)); 
		btnagain.setFont(f1);
		btnagain.setBackground(new Color(131,175,155)); 
		//设置面板的网格布局管理
		p1.setLayout(new GridLayout(8,2));
	
		//将标签和文本框添加进面板
		p1.add(sno);
		p1.add(sno1);
		p1.add(cno);
		p1.add(cno1);
		p1.add(sc);
		p1.add(sc1);
		
		
		//将按钮添加进面板
		p.add(btnsure);
		p.add(btnagain);
		p.add(btncancel);
		
		//添加面板
		this.add(p1);
		//this.add(p2);
		this.add(p);
		
		//设置顶层容器的大小、位置、可见性及close功能
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setSize(500,350);
	    setLocationRelativeTo(null);
	    setVisible(true);
	    
	    //注册监听器
	    btnsure.addActionListener(this);
	    btnagain.addActionListener(this);
	    btncancel.addActionListener(this);
	}

	//补充插入剩余元素
	public void upinsert(Object a,Object b,Object c,Object id)
	{
		try {
			this.connDB();
			//插入数据
//			stmt.executeUpdate("insert into 成绩表 (学号,课程号,成绩)  values("+"'"+sno1.getText().trim()+"'"+","+"'"+cno1.getText().trim()+"'"+","+"'"+sc1.getText().trim()+"'"+")");
			stmt.executeUpdate("update 成绩表 set 姓名=" +"'"+a.toString().trim()+"'"+",课程名="+"'"+b.toString().trim()+"'" +",教师姓名="+"'"+c.toString().trim()+"'"+"where 学号="+"'"+id.toString().trim()+"'");

//			JOptionPane.showMessageDialog(null,"成绩信息添加成功！");
			this.dispose();
			new SCM().display();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void update(String str)
	{
		Object a,b,c;
		try {
			rs=stmt.executeQuery("SELECT a.学号,a.姓名,c.课程号, c.课程名,b.成绩,d.教师姓名 FROM 学生表 a, 成绩表 b,课程表 c, 教师表 d WHERE b.课程号 = c.课程号 AND d.教师号 = c.教师号 AND a.学号 = b.学号 and a.学号="+str);
			if(rs.next())
			{
				a = rs.getString("姓名");
				b =rs.getString("课程名");
				c =rs.getString("教师姓名");
				upinsert(a,b,c,str);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void insert() {  //插入方法
		try {
			this.connDB();
			//插入数据
			stmt.executeUpdate("insert into 成绩表 (学号,课程号,成绩)  values("+"'"+sno1.getText().trim()+"'"+","+"'"+cno1.getText().trim()+"'"+","+"'"+sc1.getText().trim()+"'"+")");

			update(sno1.getText().trim());

			JOptionPane.showMessageDialog(null,"成绩信息添加成功！");
		    this.dispose();
		    new SCM().display();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean searchtest(String s1,String s2) {
  		boolean x=false;
  		this.connDB();
  		try {
  			rs=stmt.executeQuery("select * from 成绩表");
  			while(rs.next()) {
  				if(rs.getString("学号").trim().equals(s1) && rs.getString("课程号").trim().equals(s2)) {
  					x=true;
  				}
  			}
  		
  		}catch(Exception e) {
  			e.printStackTrace();
  		}
  		return x;
  	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btnsure) {
			if(sno1.getText().equals("") || cno1.getText().equals("") || sc1.getText().equals("") ) {
				JOptionPane.showMessageDialog(null,"信息不能为空！");
			}else if(searchtest(sno1.getText(),cno1.getText())){
				JOptionPane.showMessageDialog(null,"该条成绩记录已经存在！请重新添加！");
				sno1.setText("");
				cno1.setText("");
				sc1.setText("");
			}else {
				this.insert();
			}
		}
		if(e.getSource()==btnagain) {
			sno1.setText("");
			cno1.setText("");
			sc1.setText("");
		}
		if(e.getSource()==btncancel) {
			this.dispose();
			new SCM().display();;
			
		}
	}


	public static void main(String[] args) {
		new SCMadd();
    }

}
