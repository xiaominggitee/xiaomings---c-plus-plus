package sx;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;

import java.util.*;

public class TeacherFrame extends JFrame implements ActionListener{
	JLabel l=new JLabel("--教师页面--");
	//定义面板容器
	JPanel p1=new JPanel();


	JPanel p2=new JPanel();
	//设置字体类型
	Font f1=new Font("宋体",Font.BOLD,30);//字体大小

	
	//设置6个按钮，以便管理员操作
	JButton btnTmg=new JButton("个人信息管理");

	JButton btnSsh=new JButton("学生信息查询");
	JButton btnCsh=new JButton("课程信息查询");
	JButton btnSCmg=new JButton("成绩信息管理");
	JButton btnCSsh=new JButton("班级信息查询");
	JButton btnEXIT=new JButton("退出");
	
	String tno;
  	String dbURL = "jdbc:mysql://localhost:3306/教务系统?useSSL=false";
  	String userName = "root";
  	String userPwd = "Ss3255155.";
	
	Connection conn;
	Statement stmt;
	ResultSet rs;
	
	Object[][] arr;
	String tpwd;
	JScrollPane scroll1,scroll2,scroll3,scroll4,scroll5,scroll6;
	JTable tb1,tb2,tb3,tb4,tb5,tb6; 
	int row;


	String str;

	public TeacherFrame(String str) {
		super("教师页面");
		this.str = str;

		setLayout(new FlowLayout());
		//设置标签的颜色
	    l.setFont(f1);
	    l.setForeground(Color.BLACK);
	    //设置按钮字体和颜色
	    btnTmg.setFont(f1);
	    btnTmg.setContentAreaFilled(false);
	   
	    btnSsh.setFont(f1);
	    btnSsh.setContentAreaFilled(false);
	    btnCsh.setFont(f1);
	    btnCsh.setContentAreaFilled(false);
	    btnSCmg.setFont(f1);
	    btnSCmg.setContentAreaFilled(false);
	    btnCSsh.setFont(f1);
	    btnCSsh.setContentAreaFilled(false);
	    btnEXIT.setFont(f1);
	    btnEXIT.setContentAreaFilled(false);
	    
	    this.tno=str;
	    
		p1.add(l);	
		p1.setOpaque(false);
		p2.setOpaque(false);
		p2.setLayout(new GridLayout(3,2,10,10));
		p2.add(btnTmg);
		p2.add(btnSsh);
		p2.add(btnCsh);
		p2.add(btnSCmg);
		p2.add(btnCSsh);
		p2.add(btnEXIT);
		 
		//布局管理器
		this.add(p1);
		this.add(p2);
		
		//设置顶层容器的大小、位置、可见性及close功能
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600,300);
		setLocationRelativeTo(null);
		setVisible(true);
		
		this.connDB();
		
		btnTmg.addActionListener(this);
		btnSsh.addActionListener(this);
		btnCsh.addActionListener(this);
		btnSCmg.addActionListener(this);
		btnCSsh.addActionListener(this);
		btnEXIT.addActionListener(this);
	}
	
	public void connDB() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(dbURL,userName,userPwd);
			stmt=conn.createStatement();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeDB() {
		try {
			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void tmg(String str) {   //个人信息管理方法
		JFrame f=new JFrame("个人信息");
        System.out.println(str);
		f.setLayout(new FlowLayout());
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setSize(500,300);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		JButton btnchange=new JButton("修改密码");
		JButton btnrt=new JButton("返回");
		btnchange.setFont(f1);
		btnchange.setBackground(new Color(131,175,155));
		btnrt.setFont(f1);
		btnrt.setBackground(new Color(131,175,155));


		this.connDB();
		arr=new Object[1][7];
		try {
			rs=stmt.executeQuery("select * from 教师表  where 教师号="+str);
			while(rs.next()) {
				arr[0][0]=rs.getString("教师号");
				arr[0][1]=rs.getString("教师姓名");
				arr[0][2]=rs.getString("性别");
				arr[0][3]=rs.getString("登录密码");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String[] list= {"教师号","姓名","性别","登录密码"};
		tb1=new JTable(arr,list); 
		scroll1=new JScrollPane(tb1);
	
		f.add(btnchange);
		f.add(btnrt);
		f.add(scroll1); 
	
		btnrt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				new TeacherFrame(tno);
			}
		});
		btnchange.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				change();
			}
		});
	}
	
	public void change() {  //创建修改密码页面，新窗口用于修改密码
		this.connDB();
		JFrame f=new JFrame("修改密码");
		f.setLayout(new FlowLayout());
		JPanel p=new JPanel();
		JPanel p1=new JPanel();
		p.setLayout(new GridLayout(3,2));
		JLabel btn1=new JLabel("初始密码：");
		btn1.setFont(f1);
		JTextField tf1=new JTextField(10);
		JLabel btn2=new JLabel("修改密码：");
		btn2.setFont(f1);
		JTextField tf2=new JTextField(10);
		JButton ok=new JButton("确定");
		ok.setBackground(new Color(131,175,155));
		JButton cancel=new JButton("取消");
		cancel.setBackground(new Color(131,175,155));
		p.add(btn1);
		p.add(tf1);
		p.add(btn2);
		p.add(tf2);
		p1.add(ok);
		p1.add(cancel);
		f.add(p);
		f.add(p1);
		
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setLocation(530,300);

		f.setSize(300,300);
		f.setVisible(true);
		
		ok.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				try {   //获取初始密码
					rs=stmt.executeQuery("select * from 教师表 where 教师号="+tno);
					while(rs.next()) {
						tpwd=rs.getString("登录密码").trim();
					}
				}catch(Exception e1) {
					e1.printStackTrace();
				}
				
				if(tf2.getText().equals("") || tf1.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "密码不能为空！请重新修改！");
				}else{
					if(!tpwd.equals(tf1.getText().trim())) {    
					    JOptionPane.showMessageDialog(null,"初始密码错误，请重新输入密码！"); 
					    tf1.setText("");
					    tf2.setText("");
					}else {
						try {
							stmt.executeUpdate("update 教师表 set 登录密码 ="+tf2.getText().trim()+"where 教师号="+tno);
						}catch(Exception e1) {
							e1.printStackTrace();
						}
						JOptionPane.showMessageDialog(null,"密码修改成功！请重新登录！");
						f.dispose();
						new DLFrame();
					}
					
				}
			}
		});
		cancel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				new TeacherFrame(tno);
			}
		});
	}
	
	public void ssh(String str) {  //学生信息查询
		JFrame f=new JFrame("学生信息");

		f.setLayout(new FlowLayout());
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setSize(500,300);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		JButton btnrt=new JButton("返回");
		btnrt.setFont(f1);
		btnrt.setBackground(new Color(131,175,155));
	

		this.connDB();
		int i=0,j=0;
		ArrayList al=new ArrayList();
		try {
//			rs=stmt.executeQuery("select * from 学生信息表  where 教师号="+str);
			rs=stmt.executeQuery("select a.学号,a.姓名,a.性别,a.年龄,a.班级 from 学生表 a,教师表 b ,班级表 c where a.班级=c.班级 and b.教师号=c.班主任 and 教师号="+str);

			while(rs.next()) {
				al.add(rs.getString("学号"));
				al.add(rs.getString("姓名"));
				al.add(rs.getString("性别"));
				al.add(rs.getInt("年龄"));
				al.add(rs.getString("班级"));
//				al.add(rs.getString("课程名"));
				i++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		arr=new Object[i][6];
		try {
			rs=stmt.executeQuery("select a.学号,a.姓名,a.性别,a.年龄,a.班级 from 学生表 a,教师表 b ,班级表 c where a.班级=c.班级 and b.教师号=c.班主任 and 教师号="+str);
			while(rs.next()) {
				arr[j][0]=rs.getString("学号");
				arr[j][1]=rs.getString("姓名");
				arr[j][2]=rs.getString("性别");
				arr[j][3]=rs.getString("年龄");
				arr[j][4]=rs.getString("班级");
//				arr[j][5]=rs.getString("课程名");
				j++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String[] list= {"学号","姓名","性别","年龄","班级"};
		tb2=new JTable(arr,list); 
		scroll2=new JScrollPane(tb2);
	
		f.add(btnrt);
		f.add(scroll2); 

		btnrt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				new TeacherFrame(tno);
			}
		});
	}
	
	public void csh(String str) {  //课程信息查询
		JFrame f=new JFrame("课程信息");
	
		f.setLayout(new FlowLayout());
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setSize(500,300);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		JButton btnrt=new JButton("返回");
		btnrt.setFont(f1);
		btnrt.setBackground(new Color(131,175,155));


		this.connDB();
		int i=0,j=0;
		ArrayList al=new ArrayList();
		try {
//			rs=stmt.executeQuery("select c.班级,人数,b.课程号,b.课程名 from 课程表 b,班级表 c where c.班主任=b.教师号 and 教师号="+str);
			rs=stmt.executeQuery("select a.教师姓名,c.班级,人数,b.课程号,b.课程名 from 教师表 a,课程表 b,班级表 c where c.班主任=b.教师号 and a.教师号=b.教师号 and a.教师号="+str);
			while(rs.next()) {
				al.add(rs.getString("班级"));
				al.add(rs.getString("人数"));
				al.add(rs.getString("课程号"));
				al.add(rs.getString("课程名"));
				al.add(rs.getString("教师姓名"));
				i++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		arr=new Object[i][5];
		try {
			rs=stmt.executeQuery("select a.教师姓名,c.班级,人数,b.课程号,b.课程名,a.教师号 from 教师表 a,课程表 b,班级表 c where c.班主任=b.教师号 and a.教师号=b.教师号 and a.教师号="+str);
//			rs=stmt.executeQuery("select  distinct 班级  ,人数,课程号,课程名  from 学生信息表  where 教师号="+str);
			while(rs.next()) {
				arr[j][0]=rs.getString("班级");
				arr[j][1]=rs.getString("人数");
				arr[j][2]=rs.getString("课程号");
				arr[j][3]=rs.getString("课程名");
				arr[j][4]=rs.getString("教师姓名");

				j++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String[] list= {"班级","人数","课程号","课程名","教师姓名"};
		tb3=new JTable(arr,list); 
		scroll3=new JScrollPane(tb3);

		f.add(btnrt);
		f.add(scroll3);  
		
		btnrt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				new TeacherFrame(tno);
			}
		});
	}

	public void scmg_display()
	{
		this.connDB();
		int i=0,j=0;
		ArrayList al=new ArrayList();
		try {
			rs=stmt.executeQuery("select a.学号,a.性别,a.姓名,a.年龄,b.课程名,c.成绩 from 学生表 a,课程表 b ,成绩表 c ,教师表 d " +
					"where a.学号=c.学号 and c.课程号=b.课程号 and b.教师号=d.教师号 and d.教师号="+str);
			while(rs.next()) {
				al.add(rs.getString("学号"));
				al.add(rs.getString("姓名"));
				al.add(rs.getString("性别"));
				al.add(rs.getInt("年龄"));
//				al.add(rs.getString("人数"));
				al.add(rs.getString("课程名"));
				al.add(rs.getString("成绩"));
				i++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		arr=new Object[i][9];
		try {
			rs=stmt.executeQuery("select a.学号,a.性别,a.姓名,a.年龄,b.课程名,c.成绩 from 学生表 a,课程表 b ,成绩表 c ,教师表 d " +
					"where a.学号=c.学号 and c.课程号=b.课程号 and b.教师号=d.教师号 and d.教师号="+str);
			while(rs.next()) {
				arr[j][0]=rs.getString("学号");
				arr[j][1]=rs.getString("姓名");
				arr[j][2]=rs.getString("性别");
				arr[j][3]=rs.getString("年龄");
//				arr[j][4]=rs.getString("人数");
				arr[j][4]=rs.getString("课程名");
				arr[j][5]=rs.getString("成绩");
				j++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String[] list= {"学号","姓名","性别","年龄","课程名","成绩"};//人数
		tb4=new JTable(arr,list); //创建表格
		scroll4=new JScrollPane(tb4);


	}
	public void scmg(String str) {  //成绩信息管理
		JFrame f=new JFrame("学生成绩信息");

		f.setLayout(new FlowLayout());
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setSize(500,300);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		JButton btnrt=new JButton("返回");
		JButton btnchange=new JButton("修改成绩");
		JButton btndisplay=new JButton("刷新");
		btndisplay.setFont(f1);
		btndisplay.setBackground(new Color(131,175,155));
		btnchange.setFont(f1);
		btnchange.setBackground(new Color(131,175,155));
		btnrt.setFont(f1);
		btnrt.setBackground(new Color(131,175,155));

		scmg_display();

		f.add(btnchange);
		f.add(btnrt);
		f.add(btndisplay);
		f.add(scroll4);
	
		btnrt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				new TeacherFrame(tno);
			}
		});
		btnchange.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				scchange();
			}
		});
		btndisplay.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				scmg_display();
			}
		});
	}
	
	public void scchange() {  //修改成绩页面
		JFrame f=new JFrame("修改成绩");
		f.setLayout(new FlowLayout());
		JLabel l=new JLabel("输入班级");
		JTextField tf=new JTextField(10);
		JButton jb1=new JButton("确定");
		jb1.setFont(f1);
		jb1.setBackground(new Color(131,175,155));
		JButton jb2=new JButton("返回");
		jb2.setFont(f1);
		jb2.setBackground(new Color(131,175,155));
		
		f.add(l);
		f.add(tf);
		f.add(jb1);
		f.add(jb2);
		
		jb1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				new SCmg(tno,tf.getText());
			}
		});
		jb2.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				scmg(tno);  
			}
		});
		
		f.setSize(600,300);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setVisible(true);
	}
	
	
	public void cssh(String str) {  //班级信息查询
		JFrame f=new JFrame("班级信息");

		f.setLayout(new FlowLayout());
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setSize(500,300);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		JButton btnrt=new JButton("返回");
		btnrt.setFont(f1);
		btnrt.setBackground(new Color(131,175,155));

		this.connDB();
		int i=0,j=0;
		ArrayList al=new ArrayList();
		try {
			rs=stmt.executeQuery("select 班级 ,人数,教师姓名 from 班级表 a,教师表 b where a.班主任=b.教师号 and 教师号="+str);

			while(rs.next()) {
				al.add(rs.getString("班级"));
				al.add(rs.getString("人数"));
				al.add(rs.getString("教师姓名"));
				i++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		arr=new Object[i][3];
		try {
			rs=stmt.executeQuery("select 班级 ,人数,教师姓名 from 班级表 a,教师表 b where a.班主任=b.教师号 and 教师号="+str);
			while(rs.next()) {
				arr[j][0]=rs.getString("班级");
				arr[j][1]=rs.getString("人数");
				arr[j][2]=rs.getString("教师姓名");
				j++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String[] list= {"班级","人数","班主任"};
		tb6=new JTable(arr,list); 
		scroll6=new JScrollPane(tb6);
	
		f.add(btnrt);
		f.add(scroll6);  
		 
		btnrt.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				new TeacherFrame(tno);
			}
		});
	}
	
	public void actionPerformed(ActionEvent e) {
		//按钮为“教师信息管理”
		if(e.getSource().equals(btnTmg)) {
			this.dispose();
			this.tmg(tno);
		}
		//按钮为“学生信息管理”
		if(e.getSource().equals(btnSsh)) {
			//new SM().display();
			this.dispose();
			this.ssh(tno);
		}
		//按钮为“课程信息管理”
		if(e.getSource().equals(btnCsh)) {
			this.dispose();
			this.csh(tno);
		}
		//按钮为“成绩信息管理”
		if(e.getSource().equals(btnSCmg)) {
			this.dispose();
			this.scmg(tno);
		}
		//按钮为“班级信息管理”
		if(e.getSource().equals(btnCSsh)) {
			this.dispose();
			this.cssh(tno);
		}
		//按钮为“退出管理系统”
		if(e.getSource().equals(btnEXIT)) {
   
			this.dispose();
			new DLFrame();
		}
	}

	public static void main(String[] args) {	
		new TeacherFrame("1");
	}

}