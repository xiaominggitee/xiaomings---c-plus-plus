package sx;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;

public class CMadd extends JFrame implements ActionListener{
	//定义面板
	Panel p=new Panel();
	Panel p1=new Panel();
	Panel p2=new Panel();
	
	JLabel no=new JLabel("课程号:");
	JTextField cno=new JTextField(10);
	JLabel name=new JLabel("课程名:");
	JTextField cname=new JTextField(20);
	JLabel tno=new JLabel("教师号:");
	JTextField ctno=new JTextField(10);
	//定义字体
	Font f1=new Font("宋体",Font.BOLD,20);

    //定义按钮
    JButton btnsure=new JButton("确定");
    JButton btnagain=new JButton("重置");
    JButton btncancel=new JButton("取消");
    
    //定义连接字符
  	String dbURL = "jdbc:mysql://localhost:3306/教务系统?useSSL=false";
  	String userName = "root";
  	String userPwd = "Ss3255155.";
  	Connection conn;
  	Statement stmt;
  	ResultSet rs;
  	
  	public void connDB() {   //连接数据库方法
		try {
			//连接数据库
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(dbURL,userName,userPwd);
			stmt=conn.createStatement();
		}catch(Exception e) {
			e.printStackTrace();
			
		}
	}
	
	public void closeDB() {  //关闭数据库方法
		try {
			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
			
		}
	}
	
	public CMadd() {  
		super("课程信息添加页面");
		setLayout(new FlowLayout());
		
		//设置字体
		no.setFont(f1);
		name.setFont(f1);
		tno.setFont(f1);
		//按钮字体、颜色
		btnsure.setFont(f1);
		btnsure.setBackground(new Color(131,175,155)); 
		btncancel.setFont(f1);
		btncancel.setBackground(new Color(131,175,155)); 
		btnagain.setFont(f1);
		btnagain.setBackground(new Color(131,175,155)); 
		//设置面板的网格布局管理
		p1.setLayout(new GridLayout(8,2));
	
		//将标签和文本框添加进面板
		p1.add(no);
		p1.add(cno);
		p1.add(name);
		p1.add(cname);
		p1.add(tno);
		p1.add(ctno);

		//将按钮添加进面板
		p.add(btnsure);
		p.add(btnagain);
		p.add(btncancel);
		
		//添加面板
		this.add(p1);
		//this.add(p2);
		this.add(p);
		
		//设置顶层容器的大小、位置、可见性及close功能
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setSize(500,350);
	    setLocationRelativeTo(null);
	    setVisible(true);
	    
	    //注册监听器
	    btnsure.addActionListener(this);
	    btnagain.addActionListener(this);
	    btncancel.addActionListener(this);
	}
		
	public void insert() {  //插入方法
		try {
			this.connDB();
			//插入数据
			stmt.executeUpdate("insert into 课程表  values("+"'"+cno.getText().trim()+"'"+","+"'"+cname.getText().trim()+"'"+","+"'"+ctno.getText().trim()+"'"+")");
			
			JOptionPane.showMessageDialog(null,"课表信息添加成功！");
		    this.dispose();
		    new CM().display();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//判断是否找到课程号
	public boolean searchtest(String str) {
  		boolean x=false;
  		this.connDB();
  		try {
  			rs=stmt.executeQuery("select * from 成绩表");
  			while(rs.next()) {
  				if(rs.getString("学号").trim().equals(str)) {  
  					x=true;
  				}
  			}
  		
  		}catch(Exception e) {
  			e.printStackTrace();
  		}
  		return x;
  	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btnsure) {
			if(cno.getText().equals("") || cname.getText().equals("") || ctno.getText().equals("") ) {
				JOptionPane.showMessageDialog(null,"信息不能为空！");
			}else if(searchtest(cno.getText())){
				JOptionPane.showMessageDialog(null,"该课程已经存在！请重新修改");
				cno.setText("");
				cname.setText("");
				ctno.setText("");
			}else {
				this.insert();
			}
		}
		if(e.getSource()==btnagain) {
			cno.setText("");
			cname.setText("");
			ctno.setText("");
		}
		if(e.getSource()==btncancel) {
			this.dispose();
			new CM().display();;
	
		}
	}

	public static void main(String[] args) {
		new CMadd();
    }

}
