package sx;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.sql.*;

public class TMupdate extends JFrame implements ActionListener,ItemListener{
	//定义面板
	Panel p=new Panel();
	Panel p1=new Panel();
	Panel p2=new Panel();
	//定义属性标签及文本框
	JLabel no=new JLabel("教师号:");
	JTextField tno=new JTextField(10);
	JLabel name=new JLabel("姓名:");
	JTextField tname=new JTextField(20);
	JLabel sex=new JLabel("性别:");
	JComboBox tsex=new JComboBox();
	JLabel pwd=new JLabel("密码:");
	JTextField tpwd=new JTextField(10);
	JLabel pwd1=new JLabel("确认密码:");
	JTextField tpwd1=new JTextField(10);
	//定义字体
	Font f1=new Font("宋体",Font.BOLD,20);
 
    //定义按钮
    JButton btnsure=new JButton("确定");
    JButton btnagain=new JButton("重置");
    JButton btncancel=new JButton("取消");
    
    int sex1=0; 
    String[] sexlist= {"男","女"};
  	String dbURL = "jdbc:mysql://localhost:3306/教务系统?useSSL=false";
  	String userName = "root";
  	String userPwd = "Ss3255155.";
  	Connection conn;
  	Statement stmt;
  	ResultSet rs;
  	
  	public void connDB() {   //连接数据库方法
		try {
			//连接数据库
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(dbURL,userName,userPwd);
			stmt=conn.createStatement();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeDB() {  //关闭数据库方法
		try {
			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
	
		}
	}
	
	public TMupdate(String str) {  
		super("教师信息修改页面");
		setLayout(new FlowLayout());
		
		tno.setText(str);//教师编号直接获取，不用修改。
		tno.setEditable(false);
		
		//设置字体
		no.setFont(f1);
		name.setFont(f1);
		sex.setFont(f1);
		pwd.setFont(f1);
		pwd1.setFont(f1);
		//按钮字体、颜色
		btnsure.setFont(f1);
		btnsure.setBackground(new Color(131,175,155)); 
		btncancel.setFont(f1);
		btncancel.setBackground(new Color(131,175,155)); 
		btnagain.setFont(f1);
		btnagain.setBackground(new Color(131,175,155));
		//设置面板的网格布局管理
		p1.setLayout(new GridLayout(8,2));
		tsex.addItem("男");
		tsex.addItem("女");
		//将标签和文本框添加进面板
		p1.add(sex);
		p1.add(tsex);
		p1.add(no);
		p1.add(tno);
		p1.add(name);
		p1.add(tname);
		p1.add(pwd);
		p1.add(tpwd);
		p1.add(pwd1);
		p1.add(tpwd1);
		
		//将按钮添加进面板
		p.add(btnsure);
		p.add(btnagain);
		p.add(btncancel);
		
		//添加面板
		this.add(p1);
		this.add(p);
		
		//设置顶层容器的大小、位置、可见性及close功能
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setSize(500,350);
	    setLocationRelativeTo(null);
	    setVisible(true);
	    
	    //注册监听器
	    tsex.addItemListener(this);
	    btnsure.addActionListener(this);
	    btnagain.addActionListener(this);
	    btncancel.addActionListener(this);
	}
	public void itemStateChanged(ItemEvent e) {
		if(e.getStateChange()==ItemEvent.SELECTED) {
			JComboBox j=(JComboBox)e.getSource();
			sex1=j.getSelectedIndex();
		}
	}
	
	public void delete(String s) {   //删除信息方法
		try {
			stmt.executeUpdate("delete  from 教师表  where 教师号="+s);  
			stmt.executeUpdate("delete  from 课程表  where 教师号="+s);
		}catch(Exception e) {
			e.printStackTrace();
		}
}
	
	public void insert() {  //插入方法
		try {
			this.connDB();
			//插入数据
			PreparedStatement ps = null;
			String sql = "update 教师表 set 姓名=?,性别=?,登录密码=? where 教师号= ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, tname.getText().trim());
			ps.setString(2, sexlist[sex1]);
			ps.setString(3, tpwd1.getText().trim());
			ps.setString(4, tno.getText().trim());
			ps.executeUpdate();
//			stmt.executeUpdate("insert into 教师表  values("+tno.getText().trim()+","+"'"+tname.getText().trim()+"'"+","+"'"+sexlist[sex1]+"'"+","+tpwd1.getText().trim()+")");
			JOptionPane.showMessageDialog(null,"信息修改成功！");
		    this.dispose();
		    new TM().display();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btnsure) {
			if(tno.getText().equals("") || tname.getText().equals("") ||  tpwd.getText().equals("") || tpwd1.getText().equals("") ) {
				JOptionPane.showMessageDialog(null,"信息不能为空！");
			}else if(!tpwd.getText().trim().equals(tpwd1.getText().trim())) { 
				JOptionPane.showMessageDialog(null,"请重新确认密码！");
			}else {
	
				this.delete(tno.getText());
				this.insert();
			}

		}
		if(e.getSource()==btnagain) {
	
			tname.setText("");
			tpwd.setText("");
			tpwd1.setText("");
		}
		if(e.getSource()==btncancel) {
			this.dispose();
			new TM().display();;

		}
	}


	public static void main(String[] args) {
		new TMupdate("  ");
    }
}