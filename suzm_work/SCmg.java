package sx;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.sql.*;
import java.util.*;

public class SCmg extends JFrame implements ActionListener{
	
	Font f1=new Font("宋体",Font.BOLD,30);

	JButton btnrt=new JButton("返回");
	JButton btnchange=new JButton("修改");
	JMenuBar mb=new JMenuBar();
	//定义滚轮面板
	JScrollPane jsp;
	JScrollPane jsp1;
	//定义表格
	JTable tb;


  	String dbURL = "jdbc:mysql://localhost:3306/教务系统?useSSL=false";
  	String userName = "root";
  	String userPwd = "Ss3255155.";
	
	Connection conn;
	Statement stmt;
	ResultSet rs;
	

	JScrollPane scroll;
	

	Object[][] arr;
	String csname;
	String csno;
	String tno;
	String sno;
	String cno;
	
	public void connDB(){  //连接数据库方法
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(dbURL,userName,userPwd);
			stmt=conn.createStatement();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeDB() {  //关闭数据库方法
		try {
			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public SCmg(String s1,String s2) {

		tno = s1;//教师号
		csno = s2;//班级
		this.connDB();
		try {
			rs = stmt.executeQuery("select 班级 from 学生表 where 班级=" + s2);
			while (rs.next()) {
				csname = rs.getString("班级").trim();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.setTitle(csname + "班学生成绩信息");
		setLayout(new FlowLayout());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(500, 300);
		setVisible(true);
		setLocationRelativeTo(null);


		//设置字体和颜色
		btnchange.setFont(f1);
		btnchange.setBackground(new Color(131, 175, 155));
		btnrt.setFont(f1);
		btnrt.setBackground(new Color(131, 175, 155));


		//将按钮添加进菜单栏
		mb.add(btnchange);
		mb.add(btnrt);

		//连接数据库
		this.connDB();

		//注册事件监听器
		btnrt.addActionListener(this);
		btnchange.addActionListener(this);
		btnchange.setLayout(new FlowLayout());
		btnrt.setLayout(new FlowLayout());

		setSize(600, 300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		setLayout(new FlowLayout());
		//将菜单栏添加进容器中
		this.setJMenuBar(mb);


		int i=0,j=0;
		ArrayList al=new ArrayList();
		try {
			rs=stmt.executeQuery("select a.学号,a.性别,a.姓名,a.年龄,b.课程名,c.成绩 from 学生表 a,成绩表 c ,课程表 b where b.课程号=c.课程号 and a.学号=c.学号 and 教师号="+tno+" and 班级="+csno);
			while(rs.next()) {
				al.add(rs.getString("学号"));
				al.add(rs.getString("姓名"));
				al.add(rs.getString("性别"));
				al.add(rs.getString("年龄"));
//				al.add(rs.getString("人数"));
//				al.add(rs.getString("课程号"));
				al.add(rs.getString("课程名"));
				al.add(rs.getString("成绩"));
				i++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		arr=new Object[i][8];
		try {
			rs=stmt.executeQuery("select a.学号,a.性别,a.姓名,a.年龄,b.课程名,c.成绩 from 学生表 a,成绩表 c ,课程表 b where b.课程号=c.课程号 and a.学号=c.学号 and 教师号="+tno+" and 班级="+csno);

			while(rs.next()) {
				arr[j][0]=rs.getString("学号");
				arr[j][1]=rs.getString("姓名");
				arr[j][2]=rs.getString("性别");
				arr[j][3]=rs.getString("年龄");
//				arr[j][4]=rs.getString("人数");
//				arr[j][4]=rs.getString("课程号");
				arr[j][4]=rs.getString("课程名");
				arr[j][5]=rs.getString("成绩");
				j++;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String[] list= {"学号","姓名","性别","年龄","课程名","成绩"};
		tb=new JTable(arr,list); //创建表格
		scroll=new JScrollPane(tb);
		this.add(scroll);

//		btnchange.addActionListener(this);
//		btnrt.addActionListener(this);

	}
	
	public void scchange(String sno,String cno) {  
		String sname=null;
		String cname=null;
		this.cno=cno;
		this.sno=sno;
		this.connDB();
		try {
			rs=stmt.executeQuery("select * from 成绩表  where 学号="+sno+" and 课程号="+cno);
			while(rs.next()) {
				sname=rs.getString("姓名").trim();
				cname=rs.getString("课程名").trim();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		JFrame f=new JFrame(sname+"同学 --成绩页面");
		f.setLayout(new FlowLayout());
		f.setLocation(470,280);
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setSize(500,200);
		f.setVisible(true);
		JPanel p=new JPanel();
		p.setLayout(new GridLayout(4,2));
		JLabel l0=new JLabel("学号");
		JTextField tf0=new JTextField(10);
		tf0.setText(sno);
		tf0.setEditable(false);
		JLabel l1=new JLabel("姓名");
		JTextField tf1=new JTextField(10);
		tf1.setText(sname);
		tf1.setEditable(false);
		JLabel l2=new JLabel("课程名");
		JTextField tf2=new JTextField(10);
		tf2.setText(cname);
		tf2.setEditable(false);
		JLabel l3=new JLabel("成绩");
		JTextField tf3=new JTextField(10);
		JButton btn1=new JButton("确定");
		btn1.setFont(f1);
		JButton btn2=new JButton("取消");
		btn2.setFont(f1);
		p.setOpaque(false);
		p.add(l0);
		p.add(tf0);
		p.add(l1);
		p.add(tf1);
		p.add(l2);
		p.add(tf2);
		p.add(l3);
		p.add(tf3);
		f.add(p);
		f.add(btn1);
		f.add(btn2);

		btn1.addMouseListener(new MouseAdapter() {  //确认数据更新
			public void mouseClicked(MouseEvent e) {
			    f.dispose();
				try {
					stmt.executeUpdate("update 成绩表 set 成绩="+"'"+tf3.getText()+"'"+" where 学号="+sno+" and 课程号="+cno);
					JOptionPane.showMessageDialog(null,"成绩修改成功！");
					new SCmg(tno,csno);
				}catch(Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btn2.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				f.dispose();
				new SCmg(tno,csno);
			}
		});
	}
	
	public void se() {
		this.connDB();
		int row=tb.getSelectedRow();
		if(row==-1) { 
			JOptionPane.showMessageDialog(null,"请选择要修改的信息！");
//			new SCmg(tno,csno);
		}else {
			int x=0;
			try {
				rs=stmt.executeQuery("select a.学号,课程号 from 成绩表 a,学生表 b where a.学号=b.学号 and 班级="+csno);
				while(rs.next() && x<=row) {
					sno=rs.getString("学号");
					cno=rs.getString("课程号");
					x++;
				}
				this.dispose();
				scchange(sno,cno);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btnchange) {
//			this.dispose();
			se();
		}
		if(e.getSource()==btnrt) {
			this.dispose();
			TeacherFrame t = new TeacherFrame(tno);
			t.scmg(tno);
			t.dispose();
//			new TeacherFrame(tno);


		}
	}

	public static void main(String[] args) {
		new SCmg("1","1");//教师号，班级
	}

}