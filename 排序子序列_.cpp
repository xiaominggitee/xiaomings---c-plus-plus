#define _CRT_SECURE_NO_WARNINGS 1
链接：https://www.nowcoder.com/questionTerminal/2d3f6ddd82da445d804c95db22dcc471?answerType=1&f=discussion
来源：牛客网

//没读懂题目，看答案得知是非递增或者非递减的最少可能子序列，即递增或递减都可的子序列至少有多少个，相等的话即跳过这个，因为是至少
#include <iostream>
#include <vector>
using namespace std;

int main() {

    int n;
    cin >> n;
    vector<int> v;
    v.resize(n);
    int tmp = 0;
    while (n--) {
        cin >> v[tmp++];
    }
    int ret = 0, i = 0;
    while (i < v.size()) {
        if (v[i] < v[i + 1]) {
            while (i < v.size() && v[i] <= v[i + 1]) // 遍历非递减
                ++i;
            ++i, ++ret;
        }
        else if (v[i] == v[i + 1]) // 相等时不能判断
            ++i;
        else {
            while (i < v.size() && v[i] >= v[i + 1]) // 遍历非递增
                ++i;
            ++i, ++ret;
        }
    }
    cout << ret << endl;
    return 0;
}