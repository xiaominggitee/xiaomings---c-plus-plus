#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

class mystack
{
public:
	mystack(int capacity = 10)
	{
		_st = new int[capacity];
		_capacity = capacity;
		_top = 0;
	}
	mystack(const mystack& st)
	{
		_st = new int[st._capacity];
		memcpy(_st, st._st, st._top * 4);
		_top = st._top;
		_capacity = st._capacity;
	}
	mystack& operator=(mystack tmp)
	{
		swap(_st, tmp._st);
		swap(_top, tmp._top);
		swap(_capacity, tmp._capacity);
		return *this;
	}
	~mystack()
	{
		delete[] _st;
		_top = _capacity = 0;
		_st = nullptr;
	}
	
private:
	int* _st;
	int _top;
	int _capacity;
};

class A
{
public:
	int a;
};
class test :public mystack,protected A
{
public:
	test(int capacity,int a)
		:mystack(capacity)
		,_a(a)
	{}
	test(const test& t1)
		/*:mystack(t1)*/		//切割，拷贝构造只能显示去写，否则会调用到默认构造
		:_a(t1._a)
	{
		//这个也是构造,拷贝构造
	}
	test& operator=(test tmp)
	{
		//一般是要调用基类的赋值运算符重载的，复用原则，而且这样显式直接交换首先私有就不行了，于是就统一直接调用这个接口
		// 
		mystack::operator=(tmp);//切割
		//swap(_st, tmp._st);
		//swap(_top, tmp._top);
		//swap(_capacity, tmp._capacity);
		_a = tmp._a;
		return *this;
	}
	~test()
	{
		_a = 0;
		//会自己调用析构，因为要保证析构的顺序，析构会构成隐藏，统一都是desturctor
	}
private:
	int _a;
};

void test_3()
{
	test t1(10,20);
	test t2(1, 2);
	t1 = t2;
	test t3 = t1;
}

#include <stdlib.h>
#include <time.h>
#include <windows.h>
int main()
{
	//srand((unsigned int)time(0));
	//while (1)
	//{
	//	//cout << time(0) << endl;
	//	cout << rand()%99 << endl;//不使用srand函数每次运行都是从一样的数开始的，往下循环
	//	Sleep(1000);
	//}
	test_3();
}