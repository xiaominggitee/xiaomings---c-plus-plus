#pragma once
//#pragma pack(1)
#include <iostream>
#include <stdlib.h>
//#include <assert.h>
#include <cassert>
using std::cout;
using std::cin;
using std::endl;

//声明和定义分离，做项目隐藏实现方式,但是内联函数不能分开文件放，因为无地址无法链接，内联是直接代替的
//写在类里面的函数默认就是内联，要写在外面就要同一个文件
class Date
{

	//流插入和流提取运算符，要定义成全局函数，而不是类里面的函数，因为cout<< d1,由于this默认是第一个成员，无法更改，调用就变成d <<cout，可以但是很怪
	// 于是就要利用全局函数，两个参数都写进去，d写后面，in和out为第一个参数，但是外面的函数要访问私有成员就需要利用友元函数。变成朋友
	// 
	// 友元函数不是类里面的函数，需要链接的，故不能卸载头文件中，写在类里面调用的方式会有问题。故写在cpp文件才可
	//友元函数
	friend std::istream& operator>>(std::istream& in, Date& d); //流插入运算符
	friend std::ostream& operator<<(std::ostream& out, const Date& d); //流提取运算符
public:

	// 获取某年某月的天数
	int GetMonthDay(int year, int month);

	// 全缺省的构造函数
	inline Date(int year = 1900, int month = 1, int day = 1)//类中默认内联
	{
		//判断是否无效
		assert(year >= 1 && month >= 1 && month <= 12 && day >= 1 && day <= GetMonthDay(year, month));
		_year = year;
		_month = month;
		_day = day;
	}

	//Print的this是Date* const this, 这就是调用函数是const Date* 去调用Date* this，就是权限放大了。而this是隐含的故语法支持在外面添加
	//void Print(const Date* const this);，于是，只要是不会修改*this的值的函数都可以用const修饰this
	void Print() const;//打印函数可以放内联，简短，无递归


	// 拷贝构造函数
    // d2(d1)
	//Date( Date& d);

	// 赋值运算符重载
	// d2 = d3 -> d2.operator=(&d2, d3)
	Date& operator=(const Date& d);

	// 析构函数
	~Date();

	// 日期+=天数
	Date& operator+=(int day);

	// 日期+天数
	Date operator+(int day) const;//给this指针添加const Date *权限，平衡外面的const Date& d引用调用

	// 日期-天数
	Date operator-(int day) const;

	// 日期-=天数
	Date& operator-=(int day);

	// 前置++
	Date& operator++();

	 //后置++
	Date operator++(int);

	//// 后置--
	Date operator--(int);

	// 前置--
	Date& operator--();

	// >运算符重载

	bool operator>(const Date& d) const
	{
		if (_year > d._year)
			return true;
		else if (_year == d._year && _month > d._month)
			return true;
		else if (_year == d._year && _month == d._month && _day > d._day)
			return true;
		else
			return false;
	}


	// ==运算符重载

	bool operator==(const Date& d) const
	{
		return _year == d._year && _month == d._month && _day == d._day;
	}

	// >=运算符重载
	inline bool operator >= (const Date& d) const
	{
		//if (_year > d._year)
		//	return true;
		//else if (_year == d._year && _month > d._month)
		//	return true;
		//else if (_year == d._year && _month == d._month && _day >= d._day)
		//	return true;
		//else
		//	return false;

		//一般写了大于和等于之后都可以复用了
		return *this > d || *this == d;
	}


	bool operator < (const Date& d) const
	{
		//if (_year < d._year)
		//	return true;
		//else if (_year == d._year && _month < d._month)
		//	return true;
		//else if (_year == d._year && _month == d._month && _day < d._day)
		//	return true;
		//else
		//	return false;

		//复用
		/*d.opterator<*///没有这个成员
		return !(*this >= d);
		//return d > *this;//这是错误的，因为该函数是this内的，这也写就成了调用d的函数了
	}

	// <=运算符重载

	bool operator <= (const Date& d) const
	{
		//if (_year < d._year)
		//	return true;
		//else if (_year == d._year && _month < d._month)
		//	return true;
		//else if (_year == d._year && _month == d._month && _day <= d._day)
		//	return true;
		//else
		//	return false;
		// 

		//复用
		//return *this < d || *this == d;
		return !(*this > d);

	}

	// !=运算符重载

	bool operator != (const Date& d) const
	{
		return _day != d._day || _month != d._month || _year != d._year;
	}

	 //日期-日期 返回天数
	int operator-(const Date& d)  const;//用循环计数法

	////最后两个默认成员函数，取地址重载和const 取地址重载，两个基本上不用自己写，编译器自己生成已经够了,两者会对照的调用
	//Date* operator&()
	//{
	//	return this;
	//}

	//const Date* operator&() const//this的类型是const Date* const this
	//{
	//	return this;
	//}


	//const和非const之间的问题
	//只有权限的缩小，不能权限的放大
		//1.const对象可以调用非const成员函数吗？                    不可以，权限放大了，自己不能修改，给别人就能修改了吗？
		//2. 非const对象可以调用const成员函数吗？					可以，权限缩小了，
		//3. const成员函数内可以调用其它的非const成员函数吗？		不可以，权限放大了，例如operator+() const 的this想要调用非const的this是不行的
		//4. 非const成员函数内可以调用其它的const成员函数吗？		可以，权限缩小了。



	std::istream& operator>>(std::istream& in) //流插入运算符
	{
		in >> _year >> _month >> _day;
		return in;
	}
	 std::ostream& operator<<(std::ostream& out)//流提取运算符
	 {
		 out << _year << '-' << _month << '-' << _day << endl;
		 return out;
	 }
//private:
	int _year;
	int _month;
	int _day;
};