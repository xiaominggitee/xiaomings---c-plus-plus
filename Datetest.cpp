#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include "Date_class.h"


void test1()
{
	//测试比较
	Date d1(2022, 5, 18);
	Date d2(2023, 3, 20);
	Date d3(2021, 6, 12);
	Date d4(2021, 6, 12);
	cout << (d1 >= d2) << endl;
	cout << (d1 <= d2) << endl;
	cout << (d1 == d2) << endl;
	cout << (d2 > d3) << endl;
	cout << (d2 <= d3) << endl;
	cout << (d2 == d3) << endl;
	cout << (d3 >= d4) << endl;
	cout << (d3 <= d4) << endl;
	cout << (d3 == d4) << endl;
}


void test2()
{
	//测试拷贝构造和赋值，+=和+

	Date d1(2022, 5, 18);
	Date d2 = d1 + 15;//这里调用了拷贝构造，而不是赋值重载，因为用一个对象去初始化一个新对象才是拷贝构造，而赋值重载是两个对象都存在
	//这是防止拷贝构造的参数是一个 const值

	//Date d3 = d1;//等价于拷贝构造Date d3(d1)，因为d3是新创建的对象
	Date d4;
	d4 = d1 + 15;//这样写就是赋值了
	d4.Print();
	d2.Print();
	d1.Print();
}


void test3()
{
	Date d1(2022, 5, 18);
	Date d2 = d1;//拷贝构造是引用做参数，但是这个d1+15的返回值是传参返回，临时对象是个const，导致权限放大，报错
	d2 += 10000;

	//d2 = d1 + 15;
	d2.Print();
	//d2 = d2-10000;
	d2 -= 10000;
	d2.Print();
}

void test4()
{
	Date d1(2022, 5, 18);
	d1 -= -100;//还要处理减法的负数和加法的负数，因为他们的判断条件是不一样的
	d1.Print();
}

void test5()
{
	Date d1(2022, 5, 18);

	Date ret1 = d1++;//后置++编译器会自动识别并添加一个int型参数，这样规定用来区分前置和后置++，故定义的时候只能用int类型作为参数
	ret1.Print();
	Date ret2 = ++d1;
	ret2.Print();

	Date d2(2000, 5, 20);
	ret1 = d2--;
	ret1.Print();

	ret2 = --d2;
	ret2.Print();
}

void Date::Print() const//这个在外面添加是添加到this指针中的
{
	cout << _year << "-" << _month << "-" << _day << endl;
}
void func(const Date& d)//为了防止参数是return临时对象连续调用返回的const对象
{
	d.Print();//这就会发现没有这个，因为这里涉及权限的放大，因为d为了防止错误，它的类型是const Date，Print的this为const Date*指针
			  //但是Print的this是Date* const this ,这就是想用const Date* 去调用Date*  this，就是权限放大了。而this是隐含的故语法支持在外面添加

}
void test6()
{
	Date d1(2000, 05, 20);
	d1.Print();
	func(d1);//测试const函数和非const函数，const对象和非const对象
}


void test7()
{
	Date d1(2022, 5, 31);
	//cout << d1;
	//d1.operator>>(cin);//这样写在类里，面由于this的位置导致只能这样写，非常不规范
	//d1 >> cin;
	//d1.operator<<(cout);
	//d1 << cout;
	//cout << d1;

	Date d2;
	operator<<(cout, d1);
	cin >> d1;
	cout << d1 << endl;

	//本质上流提取流插入的运算符重载，库里面把内置类型的函数都重载了
	//cout和cin是iostream中的类
	int a = 10;
	double b = 2.2;
	cout << a<<endl;
	cout.operator<<(a)<<endl;//本质上只是这样子调用的


	cout << b;

}

int main()
{

	test7();



}
