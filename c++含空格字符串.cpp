#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
using namespace std;
void test2()
{
	char c;
	c = getchar();
	char s[100];
	int i = 0;
	while (c != '\n')
	{
		s[i++] = c;
		c = getchar();
	}
	s[i] = '\0';
	printf("%s", s);
}

void test3()
{
	char c;
	cin >> c;//cin识别缓冲区第一个字符是空格或者换行，会跳过并识别下一个
	char s[100];
	int i = 0;
	while (c != '\n')
	{
		s[i++] = c;
		cin >> c;

	}
	s[i] = '\0';
	printf("%s", s);
}

void test5()
{
	char c;
	//c = cin.get();
	char s[100];
	int i = 0;
	//while (c != '\n')
	//{
	//	s[i++] = c;
	//	c = cin.get();

	//}
	//s[i] = '\0';
	//printf("%s", s);

	//cin.get(s, 10);//get获取字符串，\n结束，但是不会处理\n,字符串自动补\0
	//printf("%s", s);
	//cout <<endl<< cin.get() << endl;//若结束位置\n比需要个数少，直接结束，不处理\n

	cin.getline(s, 10);
	//cin.getline(s, 10, '\n');//可指定结束符号。默认换行，getline会处理后面的数


	string s1;
	//getline(cin, s1);//string这个是专门用的，只用于string
	printf("%s", s);
	cout << cin.get() << endl;
}


