#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include <vector>
#include <string>
#include<algorithm>
using namespace std;


//vector的构造
void test1()
{
	vector<int> v1;//空，空的话也是向默认内存池申请的空间，空用的最多
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);

	vector<double> v2(10, 2.2);//指定个数以及变量，初始化

	vector<int> v3(v1);//拷贝构造

	string s1;//string是basic_string<char>这个模板的typedef，所以不用指定具体模板类型
	s1 += "hello world";
	vector<char> v4(s1.begin(), --s1.end());//利用迭代器进行拷贝值的初始化


	vector<int> v5 = v3;//拷贝构造

	/*v1 = 1;*///vector的赋值只能是相同类型的赋值
	//v2 = v1;这样也错，不同类型


	v1.clear();
	v1 = v5;//这是赋值


}

//vector和string的个别不同函数
void test2()
{
	//缩容，慎用，shrink_to_fit，缩容到合适大小
	vector<int> v1;//空，空的话也是向默认内存池申请的空间，空用的最多
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.reserve(100);
	std::cout << v1.capacity() << std::endl;//变成100多

	v1.shrink_to_fit();
	std::cout << v1.capacity() << std::endl;//缩小成为放得下所插入的四个元素



	//vector和string的个别不同，各种函数接口没有下标访问了，如insert和erase等，只有迭代器
	//v1.insert(0, 1);//错，没有下标
	string s1;
	s1.insert(0,1, 'c');
	s1.insert(1, "sdas");

	v1.insert(v1.begin(), 10);
	v1.erase(v1.end()--,v1.end());//erase两种，一个是指定迭代器区间，一个是删除迭代器指定位置，没有参数个数的接口。
	v1.push_back(11);
	//v1.erase(v1.end(),1);//错
	v1.erase(v1.end());


}


//vector的遍历，依旧是三个，下标，迭代器，以及范围for
void test3()
{

	//迭代器也是底层指针，也有四个，const_reverse_iterator这些

	vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	
	vector<int>::const_iterator it = v1.begin();//迭代器也要指定模板的参数，迭代器是连接容器的对象，需要知道其参数啥
	auto rit = v1.rbegin();//自动识别反迭代器

	while (it != v1.end())
	{
		//*it += 1;
		cout << (*it) << endl;
		it++;
	}

	//while (rit != v1.begin())//类型不同不可转换
	while (rit != v1.rend())
	{
		cout << (*rit) << endl;
		rit++;
	}

	for (auto e : v1)
	{
		cout << e << endl;
	}
}


//算法模块
void test4()
{
	//包含算法algorithm 头文件，sort和find，vector里面没有find，统一采用find算法找，find是用迭代器区间找的

	vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	vector<int>::iterator pos = find(v1.begin(), v1.end(), 3);//返回的是相应迭代器的位置，如这个是vector<int>::iterator，auto
	if (pos != v1.end())
	{
		cout << "找到" << endl;//找到返回相应迭代器位置，没有找到返回的是区间末尾，last，这个区间是左闭右开的区间，故有效值不在那
	}
	else
	{
		cout << "没有找到" << endl;
	}


	//sort与仿函数

	sort(v1.begin(), v1.end());//sort也是左闭右开区间。默认升序
	for (auto e : v1)
	{
		cout << e << endl;
	}
	sort(v1.begin(), v1.end(), greater<int>());//greater,大于，也就是左大右小，>。降序
	for (auto e : v1)
	{
		cout << e << endl;//这些排序都是直接排顺序表里面的值
	}
}
int main()
{
	test4();
	return 0;
}