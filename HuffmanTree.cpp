#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

//huffmanTree结构体
typedef struct
{
	int weight;
	int parent, lchild, rchild;
}HTNode, * HuffmanTree;
typedef char** HuffmanCode;//二级指针，或者用字符指针数组


void Select(HuffmanTree HT, int len, int& s1, int& s2)
{
	int i, min1 = INT_MAX, min2 = INT_MAX;//先赋予最大值
	for (i = 1; i <= len; i++)
	{
		if (HT[i].weight < min1 && HT[i].parent == 0)//找到没有双亲的最小两个
		{
			min1 = HT[i].weight;
			s1 = i;
		}
	}

	for (i = 1; i <= len; i++)
	{
		if (HT[i].weight < min2 && HT[i].parent == 0&&i!=s1)
		{
			min2 = HT[i].weight;
			s2 = i;
		}
	}
	
}

//赫夫曼树
void CreatHuffmanTree(HuffmanTree& HT, int n)
{
	//构造赫夫曼树HT
	int m, s1, s2, i;
	if (n <= 1) return;
	m = 2 * n - 1;
	HT = new HTNode[m + 1];  		//从1开始 ，1到m
	for (i = 1; i <= m; ++i)        	
	{
		HT[i].parent = 0;  HT[i].lchild = 0;  HT[i].rchild = 0;//孩子双亲置为0，下标由一开始，0无数值
	}
	cout << "请输入叶子结点的权值：\n";
	for (i = 1; i <= n; ++i)        	  
		cin >> HT[i].weight;


	for (i = n + 1; i <= m; ++i)
	{  	//通过n-1次的选择、删除、合并来创建赫夫曼树
		Select(HT, i - 1, s1, s2);//

		HT[s1].parent = i;
		HT[s2].parent = i;
		//得到新结点i，从森林中删除s1，s2，将s1和s2的双亲域由0改为i
		HT[i].lchild = s1;
		HT[i].rchild = s2;							//s1,s2分别作为i的左右孩子，默认第一小的是左孩子
		HT[i].weight = HT[s1].weight + HT[s2].weight; 	//孩子节点权值相加归为新节点的权值
	}									
}

void CreatHuffmanCode(HuffmanTree HT, HuffmanCode& HC, int n)
{
	//从叶子到根逆向求每个字符的赫夫曼编码，存储在编码表HC中
	int i, start, c, f;
	HC = new char* [n + 1];         						//分配n个字符编码的头指针矢量
	char* cd = new char[n];							
	cd[n - 1] = '\0';                            		//编码结束符
	for (i = 1; i <= n; ++i)
	{                      							
		start = n - 1;                          		//start指向\0
		c = i;
		f = HT[i].parent;                 			//f指向结点c的双亲结点
		while (f != 0)
		{                          					
			--start;                          		
			if (HT[f].lchild == c)
				cd[start] = '0';						//结点c是f的左孩子，则生成代码0，因为默认第一小的是左孩子。
			else
				cd[start] = '1';                 		//结点c是f的右孩子，则生成代码1
			c = f;
			f = HT[f].parent;             			//向上回溯
		}                                  			//求出第i个字符的编码      
		HC[i] = new char[n - start];         			
		strcpy(HC[i], &cd[start]);        			
	}
	delete []cd;                            			//释放临时空间
}		



void show(HuffmanTree HT, HuffmanCode HC)			//打印函数
{
	for (int i = 1; i <= sizeof(HC); i++)//这里sizeof（指针）数组名求指针数组的大小，每个成员是指针
		cout << HT[i].weight << "编码为" << HC[i] << endl;
}
int main()
{
	HuffmanTree HT;			//树
	HuffmanCode HC;			//编码
	int n;
	cout << "请输入叶子结点的个数：\n";
	cin >> n;											
	CreatHuffmanTree(HT, n);
	CreatHuffmanCode(HT, HC, n);
	show(HT, HC);
	return 0;
}