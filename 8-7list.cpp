#define _CRT_SECURE_NO_WARNINGS 1
#include <list>
#include <iostream>
#include <algorithm>
using namespace std;

//splice,拼接。两个链表结合为一个，可以指定结合到哪一个位置。注意这不是insert。splice是底层相连，会删一个，合并到一块
namespace std
{

	void test1()
	{
		list<int> lt1;
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);
		list<int> lt2;
		lt2.push_front(10);
		lt2.push_front(20);
		lt2.push_front(30);


		lt1.splice(++lt1.begin(), lt2);//begin()返回临时对象，加就加了
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;
		for (auto e : lt2)//加完2就没有了噢
		{
			cout << e << " ";
		}
		cout << endl;
	}
	//template<class T>
	//T add(T& a, T& b)
	//{
	//	cout << typeid(a).name() << endl;
	//	cout << typeid(b).name() << endl;
	//	a++;
	//	b++;
	//	return a + b;
	//
	//
	//}
	//
	//int main()
	//{
	//	//add<const int>(10, 20);//模板自动识别类型不能主动识别const类型的，只能手动添加
	//	const int a = 10;
	//	const int b = 20;
	//	cout << typeid(a).name() << endl;
	//	cout << typeid(b).name() << endl;
	//	add(a, b);
	//	//add(10, 20);
	//	return 0;
	//}


	//int add(int a)
	//{
	//	return a++;
	//}
	//#include"lish实现.h"

	template <class T>//这便是类的声明，类的定义是实例化
	class AA
	{
	public:
		void print()
		{
			cout << a;
		}

	private:
		int a;
	};

	void test2()
	{
		test1();
		//std::list();
		//cout << add(10) << endl;

		//const const int a = 10;//多个const使用是不会报错的，只是有警告，故一些模板用const修饰是可以的，const修饰变量是变量保存到符号表了，编译期间就有，就不需分配空间了
		//cout << a << endl;
		AA<int> z;
		z.print();

		//add(10, 20);//实验模板
		//return 0;
	}


	//merge归并，两个按照次序归为一个
	bool cmp(int a, int b)
	{
		return a > b;
	}
	void test3()
	{
		list<int> lt1;
		lt1.push_back(10);
		lt1.push_back(20);
		lt1.push_back(30);
		lt1.push_back(40);
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;
		lt1.resize(10, 20);//为了初始化而用的，一般很少用，因为不需要提前扩容，list的每个节点都是独立的空间
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;

		//去重函数，较少使用，而且还得先排好序才可
		lt1.unique();
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;
		//排序，由于算法中的sort是快排，故是随机排序，list无法使用，内含一个sort，较慢少节点可以用
		lt1.sort();
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;


		//去重函数，较少使用，而且还得先排好序才可
		lt1.unique();
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;

		list<int> lt2;
		lt2.push_back(1);
		lt2.push_back(1);
		lt2.push_back(1);

		lt1.sort(cmp);//要归并成什么顺序，就要先排好是什么顺序先，两个list都是
		//bool cmp(int a, int b);//按照自己给的比较方式
		lt1.merge(lt2,cmp);//要排成升序那么原来链表就要升序，不能降序或者乱序
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;

		for (auto e : lt2)
		{
			cout << e << " ";
		}
		cout << endl;//这时候lt2是空的


		//remove删除一个特定值，和remove_if删除满足条件的值，参数是一个bool的函数
		lt1.remove(100);
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;//不存在的不会完成删除

	}



	void test4()
	{
		double a = 2.1;//精度大的强转是截断，是编译器的运算
		double b = 2.9;
		cout << (int)a << " " << (int)b << endl;
		//int c = a;
		//int d = b;
		//cout << c << " " << d << endl;

		int* e = ((int*)&a);
		cout << *e << endl;

		float f = 10.5;
		printf("%d", *((int*)&f));

		//大端是高字节数据放低位
		int g = 1;//0 0 0 1一共四个字节
		if (*(char*)&g == 1)
		{
			cout << "小端" << endl;
		}
		else
			cout << "大端" << endl;



	}
}
//
//
//int main()
//{
//	test3();
//
//
//}



class aa//new的类型若是自定义类型，可以（）多个值进行初始化
{
public:
	aa(int a, int b)
	{
		_a = a;
		_b = b;
	}
	void print()
	{
		cout << _a << _b << endl;
	}
private:
	int _a;
	int _b;

};


int main()
{
	aa* a = new aa(10, 20);
	a->print();

}