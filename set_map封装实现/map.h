#pragma once
#include "RB_Tree.h"




namespace su {
	template<class K,class V>
	class map
	{
		//写一个内置仿函数，可以拿出里面的内置类型
		struct mapofT
		{
			const K& operator()(const pair<const K,V>& kv)
			{
				return kv.first;//KV类型返回K，用k比较
			}
		};
		typedef RB_Tree<K, pair<const K, V>, mapofT> Tree;
	public:
		typedef typename RB_Tree<K, pair<const K,V>,mapofT>::iterator iterator;
		typedef typename RB_Tree<K, pair<const K,V>, mapofT>::const_iterator const_iterator;


		//构造函数，析构函数，调用自定义类型的默认
		//拷贝构造和赋值运算符重载得手动写
		map() = default;//析构会自己析构
		~map() {};


		map(const map<K,V>& m)
			:_t(m._t)//初始化列表初始化也会调用其拷贝构造
		{
			/*Tree tmp(m._t);
			swap(_t._root, tmp._root);*/
		}


		map<K, V>& operator=(const map<K,V>& m)
		{
			_t = m._t;//自己会调用tree的赋值运算符重载
			return *this;
		}



		pair<iterator, bool> insert(const pair<const K,V>& kv)
		{
			return _t.insert(kv);
		}

		iterator begin()
		{
			return _t.Begin();
		}
		iterator end()
		{
			return _t.End();
		}
		const_iterator begin() const
		{
			return _t.Begin();
		}
		const_iterator end() const
		{
			return _t.End();
		}

		const V& operator[](const K& key) const 
		{
			pair<iterator, bool> ret = _t.insert(make_pair(key, V()));
			return ret.first->second;//pair里面的元素是自定义的话，会隐藏弹出框
		}

		V& operator[](const K& key) 
		{
			//这里要改insert的返回值，用于实现其insert的查找功能，节省了find
			//插入一个pair(key，V())，由于不是multi版本，就会返回其失败或成功的迭代器，失败返回本来就有的那个

			pair<iterator,bool> ret = _t.insert(make_pair(key, V()));
			return ret.first->second;//pair里面的元素是自定义的话，会隐藏弹出框
		}
	private:
		Tree _t;
	};
}
