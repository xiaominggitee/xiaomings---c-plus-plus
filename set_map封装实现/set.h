#pragma once
#include "RB_Tree.h"

namespace su {

	template <class K>
	class set
	{
		//写一个内置仿函数，可以拿出里面的内置类型
		
		struct setofT
		{
			const K& operator()(const K& value)
			{
				return value;//K类型的直接返回K
			}
		};
		typedef RB_Tree<K, K,setofT> Tree;



	public:
		typedef typename RB_Tree<K, K, setofT>::const_iterator iterator;
		typedef typename RB_Tree<K, K, setofT>::const_iterator const_iterator;

		set() = default;//析构会自己析构
		~set() {};


		set(const set<K>& s)
			:_t(s._t)//初始化列表初始化也会调用其拷贝构造
		{}


		set<K>& operator=(const set<K>& s)
		{
			_t = s._t;//自己会调用tree的赋值运算符重载
			return *this;
		}

		pair<iterator,bool> insert(const K& value)
		{
			pair<typename RB_Tree<K, K, setofT>::iterator,bool> ret =  _t.insert(value);//set的insert的返回值是不同类型的迭代器，要接受过来转一下
			return pair<iterator, bool>(ret.first._node, ret.second);
		}
		//迭代器用的是红黑树里面的迭代器，而不能直接调用迭代器，node是无法直接访问的，外面只有tree
		//iterator begin()
		//{
		//	//返回最左节点的迭代器
		//	return _t.Begin();
		//}
		//iterator end()
		//{
		//	return _t.End();
		//}
		const_iterator begin() const //set的迭代器都是const，统一用const的，不然会类型不匹配
		{
			//返回最左节点的迭代器
			return _t.Begin();
		}
		const_iterator end() const 
		{
			return _t.End();
		}

	private:
		Tree _t;
	};
}