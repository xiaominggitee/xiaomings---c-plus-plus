#define _CRT_SECURE_NO_WARNINGS 1
#include "map.h"
#include "set.h"
#include <map>

void set_map_test1()
{
	su::map<int, int> m;
	su::set<int> s;
	m.insert(make_pair(1, 1));
	m.insert(make_pair(2, 2));
	m.insert(make_pair(3, 3));
	m.insert(make_pair(4, 4));
	m.insert(make_pair(5, 5));
	s.insert(1);
	s.insert(2);

	su::map<int, int>::iterator it = m.begin();
	int i = 1;
	while (it != m.end())
	{
		cout << it->first << ":" << it->second << endl;
		cout << m[i++] << endl;
		++it;
	}

	su::set<int>::iterator it1 = s.begin();
	while (it1 != s.end())
	{
		cout << it1._node->_data << endl;
		++it1;
	}
}



void print(const su::set<int>& s)
{
	su::set<int>::const_iterator it = s.begin();
	int i = 1;
	while (it != s.end())
	{
		cout << *it << endl;
		++it;
	}
}

void print(const su::map<int, int>& m)
{
	su::map<int, int>::const_iterator it = m.begin();
	int i = 1;
	while (it != m.end())
	{
		cout << it->first << ":" << it->second << endl;
		//cout << m[i++] << endl;//因为operator【】是通过insert代理查找的。const对象无法调用insert
		++it;
	}
}

void set_map_test2()
{
	//su::map<int, int> m;
	su::set<int> s;
	//m.insert(make_pair(1, 1));
	//m.insert(make_pair(2, 2));
	//m.insert(make_pair(3, 3));
	//m.insert(make_pair(4, 4));
	//m.insert(make_pair(5, 5));
	////print(m);
	//su::map<int, int> m1(m);
	////print(m1);//测试默认成员函数

	s.insert(1);
	s.insert(2);
	su::set<int> s1(s);
	print(s);
	print(s1);
}
int main()
{
	set_map_test2();
	return 0;
}


