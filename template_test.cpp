#define _CRT_SECURE_NO_WARNINGS 1
#include "7-24.hpp"

void test10()
{
	int a = 0, b = 1;
	double c = 2.2, d = 3.3;

	swap(a, b);//iostream里面有，函数可以根据参数自动推导模板的实例化函数
	swap<double>(c, d);//函数也可以指定

	//但是类模板没有参数，故需要指定
}



void test11()
{
	//测试声明定义分为不同文件，有链接错误，需要显式实例化指定，
	int a = 0, b = 1;
	double c = 2.2, d = 3.3;

	swap(a, b);
	swap<double>(c, d);

	stack<int> st1;
	st1.push(10);

}
//int main()
//{
//
//}