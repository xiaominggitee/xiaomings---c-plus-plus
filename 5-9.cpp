#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <math.h>
using namespace std;

////引用
//int main()
//{
//	//int a = 10;
//	//1.引用必须再定义的时候初识化
//	//int& b;//别名从创建的时候就要知道是谁
//	//b = a;
//
//	////2.引用一旦指定实体，就不能再次指定实体了
//	//int& b = a;
//	//int e = 20;
//	//b = e;//这个是把e赋值给a/b，而不是更换b别名的指向
//
//
//	////const引用权限只能不变或者缩小，不能放大；
//	////只读不能转为可读写
//	//const int a = 10;
//	////int& b = a;//权限放大了，自己都只能读，引用也不能读
//
//	//int c = 20;
//	//const int& d = c;//权限缩小了
//	////d = a;只读
//	//c = 30;//自己改没事
//
//
//	//隐式转换的原理和他的引用
//	//int a = pow(2, 24) + 1;
//	//float b;
//	//b = a;
//	//printf("a = %d\n", a);				//16777217
//	//printf("b = %lf\n", b);			//b = 16777216.000000 多了就截断了，双方的隐式转换存在一个临时变量，临时变量具有常属性
//	//int a = 1093140480;
//	//int* pa = &a;
//	//float* pb;
//	////b = *((float*)pa);
//	//pb = (float*)pa;
//	////printf("b = %lf\n", b);
//	//printf("*pf = %lf\n", *pb);
//
//}


//#define add(a,b) ((a)+(b));
//int main()
//{
//	int a = 10, b = 20;
//	int z = add(10, 20);
//	cout << z;
//}

