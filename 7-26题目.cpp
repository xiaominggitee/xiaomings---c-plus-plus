#define _CRT_SECURE_NO_WARNINGS 1
#include <string>
#include <iostream>
using namespace std;



//迭代器版本，迭代器一般用不等于判断，只有明确是连续空间才可以用大于小于判断，否则如链表这这些不连续的位置就会有错
class Solution {
public:
	bool isletter(const char ch)
	{
		if (ch >= 'a' && ch <= 'z')
			return true;
		else if (ch >= 'A' && ch <= 'Z')
			return true;
		else
			return false;
	}
	string reverseOnlyLetters(string s) {

		string::iterator it_left = s.begin();
		string::iterator it_right = s.end() - 1;

		while (it_left != it_right)//不等于判断，对于偶数个会报错，因为相同之后交换又会错开了
		{
			while (it_left != it_right && !isletter(*it_left))
				it_left++;
			while (it_left != it_right && !isletter(*it_right))
				it_right--;


			swap(*it_left, *it_right);
			if (it_left != it_right)//小于判断迭代器就不需要这么麻烦，不等于判断就会有相邻字母交换后错过，导致栈溢出
			{
				it_left++;
			}
			if (it_left != it_right)
			{
				it_right--;
			}
		}
		return s;
	}
};



namespace su
{
	//下标版本
#include <string>
#include <iostream>

	class Solution {
	public:
		bool isletter(const char ch)
		{
			if (ch >= 'a' && ch <= 'z')
				return true;
			else if (ch >= 'A' && ch <= 'Z')
				return true;
			else
				return false;
		}
		string reverseOnlyLetters(string s) {

			int left = 0;
			int right = s.size() - 1;
			while (left < right)
			{
				while (left < right && !isletter(s[left]))
					left++;
				while (left < right && !isletter(s[right]))
					right--;
				swap(s[left], s[right]);
				left++;
				right--;
			}
			return s;
		}
	};
}

//int main()
//{
//	//test1();
//	string s("a-bC-dEf-ghIj");
//	Solution a;
//
//	a.reverseOnlyLetters(s);
//}
