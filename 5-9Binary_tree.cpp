#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>
using namespace std;
//#include "stack.h"
#include "queue.h"


typedef char BTNodetype;
typedef struct BinaryTreeNode
{
	BTNodetype data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
}BTNode;


//计算叶子节点的个数
int CountLeaf(BTNode* s)
{
	//递归结束条件
	if (s == NULL)
		return 0;
	if (s->left == NULL && s->right == NULL)
		return 1;
	return CountLeaf(s->left) + CountLeaf(s->right);
}


//双序遍历，先遍历节点，再左子树，再节点，再右子树
void doubleOrder(BTNode* s)
{
	if (s == NULL)
	{
		cout << "NULL ";
		return;
	}
	cout << s->data << ' ';
	doubleOrder(s->left);
	cout << s->data << ' ';
	doubleOrder(s->right);
}


//判断两个树是否相等
bool isEqualTree(BTNode* s1, BTNode* s2)
{
	if (s1 == NULL && s2 == NULL)
		return true;
	if (s1 == NULL || s2 == NULL)
		return false;

	//到这说明两节点都存在,思考的是继续的条件，判断的是结束的条件。
	if (s1->data != s2->data)
		return false;

	return isEqualTree(s1->left, s2->left) && isEqualTree(s1->right, s2->right);
}

void swap(BTNodetype* a, BTNodetype* b)
{
	assert(a && b);
	BTNodetype temp = *a;
	*a = *b;
	*b = temp;
}

//交换节点的左右孩子节点
void ExchangeTreeNode(BTNode* s)
{
	if (s == NULL)
		return;

	//后序交换自己
	if(s->left)
		ExchangeTreeNode(s->left);
	if(s->right)
		ExchangeTreeNode(s->right);

	//交换自己
	BTNode* temp = s->left;
	s->left = s->right;
	s->right = temp;
}

//c语言实现树宽度
#define MAX_NUM 20000

/* 二叉树最大深度计算函数 */
int getDepth(struct TreeNode* root)
{
	if (root == NULL) {
		return 0;
	}

	int left = getDepth(root->left);
	int right = getDepth(root->right);

	return 1 + (left > right ? left : right);
}

/* 中序遍历，更新每层最左子节点的下标合最右子节点下标 */
void inOrder(struct TreeNode* root, unsigned long long* left, unsigned long long* right, unsigned long long level, unsigned long long index)
{
	if (root == NULL) {
		return;
	}

	inOrder(root->left, left, right, level + 1, 2 * index + 1);
	if (left[level] == -1) {   // 先更新同一层 左子节点下标后固定不改了
		left[level] = index;
	}
	else {
		right[level] = index;
	}
	inOrder(root->right, left, right, level + 1, 2 * index + 2);
}

int widthOfBinaryTree(struct TreeNode* root)
{
	if (root == NULL) {
		return 0;
	}

	int depth = getDepth(root);
	unsigned long long left[MAX_NUM];    // 记录每层最左子节点下标
	unsigned long long right[MAX_NUM];   // 记录每层最右子节点下标
	unsigned long long cur_level = 0;    // 当前所处的层级
	unsigned long long cur_index = 0;    // 当前节点的下标
	int i;
	for (i = 0; i < depth; i++) {  // 下标初始化为-1
		left[i] = -1;
		right[i] = -1;
	}

	inOrder(root, left, right, cur_level, cur_index);    // 中序遍历

	long maxWidth = 1;    // 每层只有一个节点时宽度为1
	for (i = 0; i < depth; i++) {
		if (left[i] != -1 && right[i] != -1) {
			int cur_width = right[i] - left[i] + 1;
			maxWidth = (maxWidth < cur_width) ? cur_width : maxWidth;
		}
	}

	return maxWidth;
}

//typedef struct TreeNode {//c语言实现树宽度，难实现。有问题
//	int val;
//	struct TreeNode* left;
//	struct TreeNode* right;
//	int pos;
//	
//}TNode;
//
////计算二叉树的宽度，最大宽度，就是某一层节点，左右节点中间若有空节点也算宽度中
//int widthOfBinaryTree(struct TreeNode* root) {
//	ST s;
//	StackInit(&s);
//	StackPush(&s,root);//第一个元素先进栈
//	int max = 1;//第一行的元素
//	root->pos = 0;//下标0开始
//	while (!StackEmpty(&s))
//	{
//		//数组存储下标
//		int* arr = (int*)calloc(1,sizeof(int));
//		int len = 1;
//		int curpos = Stacktop(&s)->pos;
//		while(Stacktop(&s)->pos < curpos*2+1)//循环结束该行
//		{
//			TNode* temp = StackPop(&s);//temp是pop出来的肯定不为空
//
//			if (temp->left)
//			{
//				StackPush(&s, temp->left);
//				temp->left->pos = temp->pos * 2 + 1;//保存左节点下标
//
//				arr = (int*)realloc(arr, (len) * sizeof(int));//一个一个增加
//				arr[len-1] = temp->left->pos;//把节点的下标保存到动态数组
//				len++;
//			}
//			if (temp->right)
//			{
//				StackPush(&s, temp->right);
//				temp->right->pos = temp->pos * 2 + 2;//右节点下标
//
//				arr = (int*)realloc(arr, (len) * sizeof(int));//子节点存在保存子节点的下标
//				arr[len-1] = temp->right->pos;
//				len++;
//			}
//
//			//计算该层节点数
//			int m = arr[len - 1] - arr[0];//有元素的都是有节点的
//			max = m > max ? m : max;
//			free(arr);
//		}
//	}
//	
//	StackDestroy(&s);
//	return max;
//}

//int main()
//{
//	return 0;
//}







int Count1(BTNode* root)
{
	if (root->left == NULL && root->right == NULL)
		return 0;
	if (root->left == NULL || root->right == NULL)
		return 1;//只有度为一返回真
	else
		return 0;
}
//层序遍历二叉树并统计度为1的节点数目
int LevelOrder(BTNode* root)
{
	int count = 0;
	Queue S;
	QueueInit(&S);
	QueuePush(&S, root);
	while (!QueueEmpty(&S))
	{
		BTNode* temp = QueuePop(&S);
		//cout << temp->data << ' ';
		if (Count1(temp))
		{
			count++;
		}
		if (temp->left)
			QueuePush(&S, temp->left);
		if (temp->right)
			QueuePush(&S, temp->right);
	}

	QueueDestroy(&S);
	printf("\n");
	printf("%d ", count);
	return count;

}


//遍历二叉树的大小
int Count(BTNode* root)
{
	return root != NULL ? Count(root->left) + Count(root->right) + 1 : 0;
}
//
////树的高度
//int Depth(BTNode* root)
//{
//	if (root == NULL)
//		return 0;
//	int left = Depth(root->left) + 1;
//	int right = Depth(root->right) + 1;
//	if (left > right)
//		return left;
//	return right;
//}


//输出所有叶子节点到根节点的路径
#define MaxSize 101

int Getindex(BTNode**& arr, BTNode* root)
{
	for (int i = 0; i < MaxSize; i++)
	{
		if (arr[i] == root)
			return i;
	}
	return 0;
}
void Putarr(BTNode* root, BTNode**& arr)
{
	Queue S;
	QueueInit(&S);
	QueuePush(&S, root);
	int index = 1;
	arr[index] = root;
	while (!QueueEmpty(&S))
	{
		BTNode* temp = QueuePop(&S);
		index = Getindex(arr, temp);//获取现在的下标
		if (temp->left)
		{
			QueuePush(&S, temp->left);
			arr[index * 2] = temp->left;//按下标存放到数组，其他为空
		}
		if (temp->right)
		{
			QueuePush(&S, temp->right);
			arr[index * 2 + 1] = temp->right;
		}
	}
}
void _leaves(BTNode** arr)
{
	//叶子节点后面两个为空
	int i = 1;
	while(i*2+1<MaxSize)//等于也没有数
	{
		if (arr[i] != NULL && arr[i*2] == NULL && arr[i*2+1] == NULL)
		{
			//输出
			int child = i;
			int parent = child / 2;
			while (child > 0)
			{
				cout << arr[child]->data << ' ';
				child = parent;
				parent = child / 2;
			}
			cout << endl;
		}
		i++;
	}
	
}
void SelectLeaves(BTNode* root)
{

	//递归找叶子节点，并把所有值放数组
	int n = Count(root);
	BTNode** arr = new BTNode * [MaxSize];//下标从1开始,或做动态增长
	for (int i = 0; i < MaxSize; i++)
	{
		arr[i] = NULL;//全部置空
	}
	Putarr(root, arr);
	//输出叶子节点路径
	_leaves(arr);

}


void _leaves1(BTNode** arr)
{
	//叶子节点后面两个为空
	int i = 1;
	while (i * 2 + 1 < MaxSize)//等于也没有数
	{
		int count = 0;
		if (arr[i] != NULL && arr[i * 2] == NULL && arr[i * 2 + 1] == NULL)
		{
			//输出
			int child = i;
			int parent = child / 2;
			while (child > 0)
			{
				count++;
				child = parent;
				parent = child / 2;
			}
			cout << endl;
		}
		i++;
	}

}
//
//int _depth(BTNode** arr)
//{
//
//}
////输出最长路径各节点的值
//void HighTree(BTNode* root)
//{
//	//转数组
//	int n = Count(root);
//	BTNode** arr = new BTNode * [MaxSize];//下标从1开始
//	for (int i = 0; i < MaxSize; i++)
//	{
//		arr[i] = NULL;//全部置空
//	}
//	Putarr(root, arr);
//	_depth(arr);
//}



//利用数组创建二叉树
BTNode* CreateBinaryTree(char* s, int* i)
{
	if (s[*i] == '#')
	{
		(*i)++;
		return NULL;
	}
	BTNode* temp = (BTNode*)malloc(sizeof(BTNode));
	if (temp == NULL)
		return NULL;
	temp->data = s[(*i)++];				//*i自增，而不是地址向后移动一位
	//cout << temp->data;
	//printf("%c ", temp->data);
	temp->left = CreateBinaryTree(s, i);
	temp->right = CreateBinaryTree(s, i);
	return temp;
}
int main()
{
	//char* s = "ABD###C##";
	char* s = (char*)malloc(sizeof(char) * 20);
	scanf("%s", s);
	int i = 0;
	BTNode* root = CreateBinaryTree(s, &i);
	//doubleOrder(root);
	//LevelOrder(root);
	SelectLeaves(root);
	return 0;
}

//最大路径
void longest_path(BTNode* T, int* path, int& len, int* longestpath, int& longest_len)
{
	if (T != NULL)
	{
		if (T->left == NULL && T->right == NULL)//当遇到叶子结点时，该条路径完毕 
		{
			path[len] = T->data;
			if (len > longest_len)//如果长于longest_len就替换 
			{
				for (int j = 0; j <= len; j++)
				{
					longestpath[j] = path[j];
				}
				longest_len = len;//longest_len更新 
			}
		}
		else//当遇到的不是叶子结点时，该条路径继续 
		{
			path[len++] = T->data;
			longest_path(T->left, path, len, longestpath, longest_len);
			longest_path(T->right, path, len, longestpath, longest_len);   
		}
	}
}
