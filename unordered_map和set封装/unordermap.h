#pragma once
#include "Hash_Bucket.h"

struct Date
{
	Date(int year = 1, int month = 1, int day = 1)
		:_year(year)
		, _month(month)
		, _day(day)
	{}

	bool operator==(const Date& d) const
	{
		return _year == d._year
			&& _month == d._month
			&& _day == d._day;
	}

	int _year;
	int _month;
	int _day;
};

struct DateHash
{
	size_t operator()(const Date& d)
	{
		//return d._year + d._month + d._day;
		size_t hash = 0;
		hash += d._year;
		hash *= 131;
		hash += d._month;
		hash *= 1313;
		hash += d._day;

		cout << hash << endl;

		return hash;
	}
};

template<class K,class V,class HashFunc = Hashdefault<K>>
class unordered_map
{
public:
	struct MapKeyofT
	{
		const K& operator()(const pair<K, V>& kv)
		{
			return kv.first;
		}
	};
	typedef typename Bucket<K, pair<K, V>, MapKeyofT, HashFunc>::iterator iterator;//typename 用于获取非实例化类中的数据，区分非静态成员用的
public:
	//构造，拷贝构造，析构，赋值都不用写


	pair<iterator,bool> insert(const pair<K,V>& kv)
	{
		return _ht.insert(kv);
	}

	iterator begin() 
	{
		return _ht.begin();
	}
	iterator end()
	{
		return _ht.end();
	}
	iterator find(const K& key)
	{
		return _ht.find(key);
	}

	bool erase(const K& key)
	{
		return _ht.erase(key);
	}

	V& operator[](const K& key)
	{
		pair<iterator, bool> ret = insert(make_pair(key, V()));
		return ret.first->second;
	}
private:
	Bucket<K, pair<K, V>,MapKeyofT, HashFunc> _ht;			//对于不常用的类型转hash码，需要自己传相应的仿函数化为哈希值
};