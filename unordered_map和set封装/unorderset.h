#pragma once
#include "Hash_Bucket.h"

template<class K, class HashFunc = Hashdefault<K>>
class unordered_set
{
	struct SetKeyofT
	{
		const K& operator()(const K& key)
		{
			return key;
		}
	};
public:
	//构造，拷贝构造，析构，赋值都不用写

	typedef typename Bucket<K, K, SetKeyofT, HashFunc>::iterator iterator;


	pair <iterator,bool> insert(const K& key)
	{
		return _ht.insert(key);
	}
	iterator begin()
	{
		return _ht.begin();
	}
	iterator end()
	{
		return _ht.end();
	}
	iterator find(const K& key)
	{
		return _ht.find(key);
	}

	iterator erase(const K& key)
	{
		return _ht.erase(key);
	}
private:
	Bucket<K, K,SetKeyofT, HashFunc> _ht;
};