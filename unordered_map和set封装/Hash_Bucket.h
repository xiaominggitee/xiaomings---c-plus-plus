#pragma once
#include <iostream>
#include <vector>
using namespace std;
//hash桶实现，开散列（拉链法）解决哈希冲突，但依然是除留余数法算hash值


template <class T>
struct BucketNode
{
	T _data;
	BucketNode<T>* _next = nullptr;
	BucketNode(const T& data)
		:_data(data)
		,_next(nullptr)
	{}

};

template <class K>
struct Hashdefault
{
	size_t operator()(const K& key)
	{
		return key;
	}
};

template <>//string的特化，把string转换为一个数
struct Hashdefault<string>
{
	size_t operator()(const string& s)
	{
		//return s[0];//首字母ascii可以
		//return (size_t)&s;地址不行，因为一样的string地址也不一样
		size_t sum = 0;
		for (auto& e : s)
		{
			sum = sum * 131 + e;
		}
		return sum;
	}
};


template <class K, class T, class KeyofT, class HashFunc>
class Bucket;

template <class K, class T, class KeyofT, class HashFunc>			//之前的T ，T& ，T*是为了支持const迭代器的
class Hash_iterator
{
	//哈希表的迭代器主要考虑就是加到该桶的最后一个，需要跳到下一个有值的桶
	typedef Hash_iterator<K, T, KeyofT, HashFunc> self;
public:
//private:
	BucketNode<T>* _Node;
	Bucket<K, T, KeyofT, HashFunc>* _HashBucket;
public:
	Hash_iterator()
	{}
	Hash_iterator(BucketNode<T>* Node, Bucket<K, T, KeyofT, HashFunc>* HashBucket)
		:_Node(Node)
		,_HashBucket(HashBucket)
	{}
	self& operator++()
	{
		if (_Node->_next != nullptr)//下一个不为空
		{
			_Node = _Node->_next;
		}
		else
		{
			//寻找下一个哈希桶，如果找不到，置空
			_Node = _HashBucket->GetNextBucket(_Node);
		}
		return *this;
	}

	bool operator!=(const self& it) const 
	{
		return _Node != it._Node;
	}

	bool operator ==(const self& it) const
	{
		return _Node == it._Node;
	}
	T& operator*()	
	{
		return _Node->_data;
	}
	T* operator->()
	{
		return &_Node->_data;
	}


};


template <class K,class T,class KeyofT,class HashFunc>
class Bucket 
{
	typedef BucketNode<T> Node;

	template<class K, class T, class KeyOfT, class HashFunc>
	friend class Hash_iterator;

public :
	typedef Hash_iterator<K, T, KeyofT, HashFunc> iterator;	//库里面实现的const iterator是单独写的一个类



	pair<iterator,bool> insert(const T& data)
	{
		HashFunc hf;
		KeyofT kot;

		iterator pos = find(kot(data));
		if (pos != end())
		{
			return make_pair(pos, false);
		}

		//哈希桶
		if (_tables.size() == _n)
		{
			//扩容，刚好为0的时候也一起判断了，哈希桶的话一般是负载因子为1扩容的，因为其哈希冲突的影响不大
			size_t newsize = _n == 0 ? 10 : _tables.size() * 2;

			//这里也可以创建一个新的哈希表复用insert依次插入，但是由于这里是节点，移交节点会更好。不用浪费
			vector<Node*> newtables;
			newtables.resize(newsize);
			for (size_t i = 0; i < _tables.size(); i++)
			{
				if (_tables[i] != nullptr)
				{
					Node* cur = _tables[i];
					while (cur)
					{
						Node* next = cur->_next;
						size_t hashi = hf(kot(cur->_data)) % newsize;
						cur->_next = newtables[hashi];
						newtables[hashi] = cur;
						cur = next;
					}
				}
			}
			_tables.swap(newtables);//老的vector会自己析构
		}

		size_t hashi = hf(kot(data)) % _tables.size();//扩容后不会有除零错误了
		Node* newNode = new Node(data);
		newNode->_next = _tables[hashi];
		_tables[hashi] = newNode;
		_n++;

		return make_pair(iterator(newNode, this), false);;
	}

	iterator find(const K& key)
	{
		HashFunc hf;
		KeyofT kot;
		if (_tables.size() == 0)
		{
			return iterator(nullptr,this);
		}
		//find是通过key来找该节点
		size_t hashi = hf(key) % _tables.size();
		Node* cur = _tables[hashi];
		while (cur)
		{
			if (kot(cur->_data) == key)
				return iterator(cur,nullptr);
			else
				cur = cur->_next;
		}
		return iterator(nullptr, this);

	}

	iterator erase(const K& key)
	{
		HashFunc hf;
		KeyofT kot;

		//前后指针删除，或者替换法删除都可以，前者优先，后者学思维
		if (_tables.size() == 0)
		{
			return iterator(nullptr,this);
		}
		size_t hashi = hf(kot(key)) % _tables.size();
		Node* prev = nullptr;
		Node* cur = _tables[hashi];
		while (cur)
		{
			if (kot(cur->_data) == key)
			{
				iterator* next = new iterator(cur, this);
				++(*next);
				if (prev == nullptr)
					_tables[hashi] = cur->_next;
				else
					prev->_next = cur->_next;
	
				delete cur;
				_n--;
				return *next;
			}
			prev = cur;
			cur = cur->_next;
		}
		return iterator(nullptr, this);


	}

	iterator begin()
	{
		for (size_t i = 0; i < _tables.size(); i++)
		{
			if (_tables[i] != nullptr)
			{
				return iterator(_tables[i], this);
			}
		}
		return iterator(nullptr, this);
	}
	iterator end()
	{
		return iterator(nullptr, this);
	}
	~Bucket()
	{
		for (size_t i = 0; i < _tables.size(); ++i)
		{
			Node* cur = _tables[i];
			while (cur)
			{
				Node* next = cur->_next;
				delete cur;
				cur = next;
			}

			_tables[i] = nullptr;
		}
	}
public:

	Node* GetNextBucket( Node* node)
	{
		HashFunc hf;
		KeyofT kot;

		//判断当前在哪一个桶
		size_t n = hf(kot(node->_data)) % _tables.size();
		n++;											//n先++一下，跳过自己，不然后面一直遍历都是自己
		for (int i = n; i < _tables.size(); i++)
		{
			if (_tables[i] != nullptr)
			{
				return _tables[i];
			}
		}
		return nullptr;

	}



private:
	//vector<Node> _tables;
	vector<Node*> _tables;//指针不会调用自己的构造函数，指针在reszie的模板中初始化为空
	size_t _n = 0;	//插入值的个数

};
