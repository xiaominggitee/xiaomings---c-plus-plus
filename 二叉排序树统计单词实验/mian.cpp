#define _CRT_SECURE_NO_WARNINGS 1
#include "filtration.h"

void menu()
{
	cout << "1.线性表" << endl;
	cout << "2.平衡二叉树" << endl;
	cout << "3.退出" << endl;
}

void show()
{
	cout << "1.连续执行至完毕" << endl;
	cout << "2.显示执行时间" << endl;
	cout << "3.单步执行：识别并统计单词" << endl;
	cout << "4.单步执行：删除并显示出现的频率低的单词" << endl;
	cout << "5.单步执行：输出其余单词及其频率" << endl;
	cout << "6.单步执行：计算并输出ASL值" << endl;
	cout << "7.返回上一级" << endl;
}
int main()
{
	Line l;
	SortTree t;
	int a = 0, b = 0,time = 0;
	char c;
	int startTime, endTime;
	float asl =0;
	while (1)
	{
		menu();
		cin >> a;
		switch (a)
		{
		case 1:
			system("cls");
			show();
			cin >> b;
			switch (b)
			{
			case 1:
				system("cls");
				cout << "1.连续执行至完毕" << endl;
				startTime = clock();//计时开始
				l.statstring();
				l.Print();
				asl = l.ASL();
				cout << "删除低频输出低频元素，剩余输出到文件:" << endl;
				l.delstring();
				l.out_Sort();
				l.destory();
				endTime = clock();//计时结束
				time = endTime - startTime;
				cout << "The run time is: " << (double)time / CLOCKS_PER_SEC << "s" << endl;
				cout << "顺序表ASL：" <<asl << endl;
				system("pause");
				break;
			case 2:
				system("cls");
				cout << "2.显示执行时间" << endl;
				cout << "The run time is: " << (double)time/ CLOCKS_PER_SEC << "s" << endl; 
				system("pause");
				break;			
			case 3:
				system("cls");
				cout << "3.单步执行：识别并统计单词" << endl;
				l.statstring();
				l.Print();
				l.destory();
				system("pause");
				break;
			case 4:
				system("cls");
				cout << "4.单步执行：删除并显示出现的频率低的单词" << endl;
				l.statstring();
				l.delstring();
				system("pause");
				break;
			case 5:
				cout << "5.单步执行：输出其余单词及其频率" << endl;
				l.Print();
				l.out_Sort();
				system("pause");
				break;
			case 6:
				system("cls");
				cout << "6.单步执行：计算并输出ASL值" << endl;
				cout << "顺序表ASL：" << asl << endl;
				system("pause");
				break;
			case 7:
				continue;
			}
			break;
		case 2:
			system("cls");
			show();
			cin >> b;
			switch (b)
			{
			case 1:
				system("cls");
				cout << "1.连续执行至完毕" << endl;
				startTime = clock();//计时开始
				t.statstring();
				t.Print();
				cout << "删除低频输出低频元素，剩余输出到文件:" << endl;
				asl = t.ASL();
				t.delstring();
				t.out_Sort();
				t.destory();
				cout << "二叉排序树ASL：" << asl << endl;
				endTime = clock();//计时结束
				cout << "The run time is: " << (double) time/ CLOCKS_PER_SEC << "s" << endl;
				time = endTime - startTime;
				system("pause");
				break;
			case 2:
				system("cls");
				cout << "2.显示执行时间" << endl;
				cout << "The run time is: " << (double)time/ CLOCKS_PER_SEC << "s" << endl;
				system("pause");
				break;
			case 3:
				system("cls");
				cout << "3.单步执行：识别并统计单词" << endl;
				t.statstring();
				t.Print();
				t.destory();
				system("pause");
				break;
			case 4:
				system("cls");
				cout << "4.单步执行：删除并显示出现的频率低的单词" << endl;
				t.statstring();
				t.delstring();
				system("pause");
				break;
			case 5:
				cout << "5.单步执行：输出其余单词及其频率" << endl;
				t.Print();
				t.out_Sort();
				system("pause");
				break;
			case 6:
				system("cls");
				cout << "6.单步执行：计算并输出ASL值" << endl;
				cout << "二叉排序树ASL：" << asl << endl;
				system("pause");
				break;
			case 7:
				continue;
			}
			break;
		case 3:
			cout << "是否确认退出【y/n】:";
			cin >> c;
			if(c=='y')
				exit(0);
		}
		system("cls");
	}

	return 0;
}





