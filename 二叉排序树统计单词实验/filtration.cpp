#define _CRT_SECURE_NO_WARNINGS 1
#include "filtration.h"


//读取单词
void  getstring(FILE** fp,char*& string)
{
	char ch = fgetc(*fp);
	int i = 0;
	while (isalnum(ch)||ch =='\'')
	{
		string[i++] = ch;
		ch = fgetc(*fp);
	}
	string[i] = '\0';
	if (!strlen(string) && *string != '10')//换行问题
		string = nullptr;
}


LNode* BuyNode()
{
	LNode* temp = (LNode*)malloc(sizeof(LNode));
	if (temp) 
	{
		temp->count = 0;
		temp->next = nullptr;
		return temp;
	}
	return nullptr;
}


//遍历链表
LNode* Search_Line(LNode* head,LNode* tail ,char* s)
{
	assert(head && tail);//传进来是空
	while (head != tail)
	{
		if (strcmp(head->s, s) == 0)
			return head;
		head = head->next;
	}
	//出来还剩一个没有比较
	if (strcmp(head->s, s) == 0)
		return head;
	else
		return nullptr;
}

//统计
int Line::statstring()
{

	int startTime = clock();//计时开始
	//打开文件
	FILE* fp = fopen("InFile.txt", "r");
	if (fp == nullptr)
	{
		cout << "openfile(line) is false" << endl;
		return -1;
	}
	char* string = (char*)malloc(sizeof(char) * 25);
	getstring(&fp,string);
	while (string != nullptr)
	{
		if (*string == '10')
			continue;
		LNode* temp = Search_Line(_head,_tail, string);		//遍历搜寻链表
		if (temp)
		{
			//单词存在
			temp->count++;
		}
		else
		{
			//单词不存在，接到后面
			temp = BuyNode();//不存在temp返回的是空，故创建
			temp->count++;
			strcpy(temp->s, string);

			_tail->next = temp;
			_tail = temp;
		}
		getstring(&fp,string);
	}
	fclose(fp);
	fp = nullptr;
	int endTime = clock();//计时结束
	return endTime - startTime;
}

void Line::destory()
{
	LNode* cur = _head->next;
	while (cur!=nullptr&&cur  != _tail)
	{
		LNode* temp = cur;
		cur = cur->next;
		free(temp);
	}
	free(_tail);
	_tail = _head;
}
void Line::Print()
{
	
	LNode* head = _head->next;
	while (head!=nullptr)
	{
		cout << head->s << ' ' << head->count << endl;
		head = head->next;
	}

}

//删除低频词并显示已经删了的单词
void Line::delstring()
{
	LNode* prev = _head;//前一个指针删除节点
	LNode* head = _head->next;

	while(head != nullptr)
	{
		if (head->count < 5)
		{
			cout << head->s << " " << head->count << endl;
			prev->next = head->next;
			free(head);
			head = prev->next;
		}
		else
		{
			prev = head;
			head = head->next;
		}
	}

	//尾节点可能被删了
	_tail = prev;//更新尾节点
	

}

void Line::out_Sort()
{
	FILE* fp = fopen("outfile.txt", "w");
	if (fp == nullptr)
	{
		cout << "outfile is open fail" << endl;
		return;
	}
	//拿出来就删除了
	int max = 0;
	LNode temp;
	temp.next = nullptr;
	while (_head != _tail)
	{
		//找出剩下的最大的
		LNode* head = _head->next;
		LNode* prev = _head;//跟随到最后，防止尾节点指向空
		LNode* prevMax = prev;//记录最大值的前一个节点
		max = head->count;
		temp.count = max;
		strcpy(temp.s, head->s);
		while (head != _tail->next)
		{
			if (head->count > max)
			{
				max = head->count;
				//记录这个节点
				temp.count = max;
				strcpy(temp.s, head->s);
				prevMax = prev;
				
			}
			prev = head;
			head = head->next;
		}
		//删除该节点
		if (strcmp(temp.s,_tail->s)==0)//尾节点
		{
			_tail = prevMax;
			_tail->next = nullptr;
			free(prev);
		}
		else 
		{
			//删的不是尾节点,不用改变_tail,临时利用head
			head = prevMax->next;
			prevMax->next = head->next;
			free(head);
		}

		//把temp的数值存入文件中
		fprintf(fp, "%s %d\n", temp.s, temp.count);
	}
	fclose(fp);
}


//****************************************************实现方式分割线**************************************************
void SortTree::PostDelete(TNode*& root)
{
	if (!root)
		return;
	if (root->lchild)
		PostDelete(root->lchild);
	if (root->rchild)
		PostDelete(root->rchild);
	free(root);
}


bool Search_Tree(TNode* root, char* string,TNode*& temp)
{
	if (root == nullptr)
		return false;
	else if (strcmp(root->s, string) < 0)
	{
		//找右子树
		temp = root;
		return Search_Tree(root->rchild, string, temp);
	}
	else if (strcmp(root->s, string) > 0)
	{
		//找左子树
		temp = root;
		return Search_Tree(root->lchild, string, temp);
	}
	else
	{
		//找到了
		temp = root;
		return true;
	}
}

TNode* BuyTNode()
{
	TNode* temp = (TNode*)malloc(sizeof(TNode));
	if (temp)
	{
		temp->count = 0;
		temp->lchild = nullptr;
		temp->rchild = nullptr;
		return temp;
	}
	return nullptr;
}

//二叉排序树的统计构建字符串
void SortTree::statstring()
{
//打开文件
	FILE* fp = fopen("InFile.txt", "r");
	if (fp == nullptr)
	{
		cout << "openfile(tree) is false" << endl;
		return ;
	}
	char* string = (char*)malloc(sizeof(char) * 25);
	getstring(&fp, string);
	while (string != nullptr)
	{
		if (*string == '10')//换行就下一次，没有就结束
			continue;
		TNode* temp1 = nullptr;
		bool status = Search_Tree(_root, string, temp1);		//遍历搜寻树中元素
		if (status)
		{
			//单词存在
			temp1->count++;
		}
		else
		{
			TNode* temp2 = BuyTNode();//创造新节点
			temp2->count++;
			strcpy(temp2->s, string);

			//单词不存在，插入单词节点
			if (!_root)//空树直接放
			{
				_root = temp2;
			}
			else
			{
				//不是空树，temp1就是父节点
				if (strcmp(temp1->s, string) > 0)
				{
					temp1->lchild = temp2;
				}
				else
				{
					temp1->rchild = temp2;
				}
			}

		}
		getstring(&fp, string);
	}
	fclose(fp);
	fp = nullptr;

}

void InOrder(TNode* root)
{
	if (!root)
		return;
	if (root->lchild)
		InOrder(root->lchild);

	cout << root->s << ' ' << root->count << endl;

	if(root->rchild)
		InOrder(root->rchild);
}
void SortTree::Print()
{
	//中序从小到大打印
	InOrder(_root);
}


void InOrderDel(TNode*& root,TNode* parent)
{
	if (!root)
		return;

	InOrderDel(root->lchild,root);
	InOrderDel(root->rchild, root);

	//处理本个节点
	if (root->count < 5)
	{
		cout << root->s << ' ' << root->count << endl;
		//小于5删除
		//叶子节点和单子树的节点处理方法一样，把子节点接上就行
		if (!root->rchild)
		{
			////右节点为空
			//TNode* temp = root;//这样并没有接上父节点，这种写法有问题
			//root = root->lchild;
			//free(temp);


			TNode* temp = root;
			if (parent != nullptr)//不是根节点，需要父节点去接
			{
				if (parent->lchild == root)
				{
					parent->lchild = root;
				}
				else
				{
					parent->rchild = root;
				}
			}
			root = root->lchild;
			free(temp);

		}
		else if (!root->lchild)
		{
			TNode* temp = root;
			if (parent != nullptr)//不是根节点，需要父节点去接
			{
				if (parent->lchild == root)
				{
					parent->lchild = root;
				}
				else
				{
					parent->rchild = root;
				}
			}
			root = root->rchild;
			free(temp);

		}
	
		else
		{
			//左右子树都在，找到该节点的前驱或者后驱节点都行，用其中一个替换
			//这里用后驱，右子树中最小的一个,就是一直找左节点即可
			TNode* prev = root;
			TNode* cur = root->rchild;
			while (cur->lchild)//找到的cur是没有左节点的。因为是最小的一个了
			{
				prev = cur;
				cur = cur->lchild;
			}

			if (prev == root)
			{
				//prev == root就说明root的后驱节点就是root->rchild，
				//要把rchild（cur）的右节点接到root（prev）的右节点中，替换数值就行
				
				root->count = cur->count;
				strcpy(root->s, cur->s);
				prev->rchild = cur->rchild;
				free(cur);
				
			}
			else
			{
				//这里root的后驱不是直接子孩子。把cur的右节点接到prev的左节点中
				root->count = cur->count;
				strcpy(root->s, cur->s);
				prev->lchild = cur->rchild;
				free(cur);
			}


		}
	}
		

}

void SortTree::delstring()
{
	//二叉排序树有三种删除情况，1.该节点是叶子节点，2.该节点只有一边子树，3.该节点两个子节点都有
	//这个是删除所有出现次数低于5的单词

	//遍历寻找 ，这里用后序

	TNode* parent = nullptr;
	InOrderDel(_root,parent);
	
}
void prevASL(TNode* root, int& sum,float& total, int level)
{
	if (root == nullptr)
		return;
	sum += level;
	total++;//个数
	prevASL(root->lchild, sum,total, level + 1);//回到哪一次的level都不会随之改变
	prevASL(root->rchild, sum, total,level + 1);

}

float SortTree::ASL()
{
	int sum = 0;
	float total = 0;
	prevASL(_root, sum,total,1);
	return sum / total;
}

void prevOrder(TNode* root,TNode*& temp,TNode* prev,TNode*& parent,int& max)
{
	if (root)
	{
		if (root->count > max)
		{
			temp = root;
			max = root->count;
			parent = prev;
		}
		if (root->lchild)
			prevOrder(root->lchild, temp, root, parent,max);
		if (root->rchild)
			prevOrder(root->rchild, temp, root, parent,max);
	}
	
}
void SortTree::out_Sort()
{
	FILE* fp = fopen("outfileTree.txt", "w");
	if (fp==nullptr)
	{
		cout << "outfileTree is opened fail" << endl;
		return;
	}
	//cout中找最大的，注意二叉排序树用到的是字符串的大小排序而不是cout
	while (_root)//树为空结束
	{
		//前序遍历
		TNode* cur=nullptr;			//最大值的节点
		TNode* parent = nullptr;	//保留最大值的父节点
		TNode* prev = nullptr;		//跟随记录父节点
		int max = 0;
		prevOrder(_root,cur,prev,parent,max);
		fprintf(fp, "%s %d\n", cur->s, cur->count);

		//删除cur,二叉排序树删除有三个可能，1.叶子节点 2.有一个子树，3.有左右两个子树
		if (!cur->rchild)
		{
			//右节点没有
			if (parent == nullptr)
			{
				//cur为根节点
				_root = cur->lchild;
				free(cur);
				cur = nullptr;
			}
			else
			{
				//cur不为根节点
				if (parent->lchild == cur)
				{
					parent->lchild = cur->lchild;
					free(cur);
					cur = nullptr;
				}
				else
				{
					parent->rchild = cur->lchild;
					free(cur);
					cur = nullptr;
				}
			}
		}
		else if (!cur->lchild)
		{
			if (parent == nullptr)
			{
				//cur为根节点
				_root = cur->rchild;
				free(cur);
				cur = nullptr;
			}
			else
			{
				//cur不为根节点
				if (parent->lchild == cur)
				{
					parent->lchild = cur->rchild;
					free(cur);
					cur = nullptr;
				}
				else
				{
					parent->rchild = cur->rchild;
					free(cur);
					cur = nullptr;
				}
			}
		}
		else//左右孩子都有,前驱后驱都可以
		{
			//找左孩子最右边一个，是cur的前驱节点
			TNode* max = cur->lchild;
			TNode* prev = cur;
			while (max->rchild)
			{
				prev = max;
				max = max->rchild;
			}
			//交换值
			cur->count = max->count;
			strcpy(cur->s, max->s);
			//删除
			if (cur->lchild == max)
				cur->lchild = max->lchild;
			else
				prev->rchild = max->lchild;
			free(max);
			max = nullptr;
		}
	}
	fclose(fp);

}