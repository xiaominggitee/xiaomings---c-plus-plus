#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <cstdio>
#include <cassert>
#include <ctime>

using std::cout;
using std::endl;
using std::cin;



typedef struct Line_Node
{
	char s[25];//单词大小
	int count;
	struct Line_Node* next;
}LNode;

class Line
{
public:
	Line()
	{
		_head = (LNode*)malloc(sizeof(LNode));//带头的链表
		_tail = _head;
		_tail->next = nullptr;
	}
	~Line()
	{
		while (_head!=nullptr && _tail!=nullptr && _head!= _tail)
		{
			LNode* temp = _head;
			_head = _head->next;
			free(temp);
		}
		free(_tail);
		_tail = _head = nullptr;
	}

	void destory();//临时删除
	//计算ASL
	float ASL()
	{
		//计算长度
		LNode* head = _head->next;
		int count = 0;
		while (head != nullptr)
		{
			count++;
			head = head->next;
		}
		return (count + 1) / (float)2.0;
	}

	//统计
	int statstring();

	//打印单词和次数
	void Print();

	//删除低频词
	void delstring();

	//排序输出到文件
	void out_Sort();
	
//链表
private:
	LNode* _head;
	LNode* _tail;
};

typedef struct TreeNode
{
	char s[25];
	int count;
	TreeNode* lchild;
	TreeNode* rchild;
}TNode;


class SortTree 
{
public:
	SortTree()
	{
		this->_root = nullptr;//一开始是空树
	}
	~SortTree()
	{
		//后序遍历释放节点
		PostDelete(this->_root);
	}
	float ASL();
	void destory()//临时删除
	{
		PostDelete(this->_root);
		_root = nullptr;
	}

	//释放节点
	void PostDelete(TNode*& root);

	//统计
	void statstring();

	//打印单词和次数
	void Print();

	//删除低频词
	void delstring();

	//排序输出到文件
	void out_Sort();

private:
	TNode* _root;
};




