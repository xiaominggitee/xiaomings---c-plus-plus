#define _CRT_SECURE_NO_WARNINGS 1
#include "Date_class.h"




//模板的特化，对于特殊需求的模板可以单独写一个特殊的指定板块，模板特化的前提首先得有原先模板才可以
//特化也是模板，其也是在编译期间确定实例化对象，是半生不熟的一种，介于两者之间（汇编期间会生成符号表，故是在其之前弄的实例化）



//回顾仿函数，一般不写成类模板，因为仿函数是用作指定的类型进行比较的
//template <class T> //或者template 也可以作为模板的类型参数，其中template还可以提前访问虚拟类型的内嵌类型。如reverse_iterator 需要的T的引用和指针
//struct lessDate
//{
//	bool operator()(const T& a, const T& b)
//	{
//		return a < b;
//	}
//};
//
//void test1()
//{
//	lessDate<Date> l1;
//
//	Date d1(2022, 1, 13);
//	Date d2(2022, 1, 14);
//	cout << l1(d1, d2) << endl;
//}



template <class T>
bool less(T a, T b)
{
	return a < b;
}

template <>//这里是全特化，函数没有偏特化，因为其能通过传进来的类型决定实例化的类型，防止偏特化和参数冲突，且函数可以重载即可支持不同类型的了
bool less<Date*>(Date* pa, Date* pb)//作为上面类模板的特化，这个类型的符号就要和上面匹配，上面是const T& 的参数，下面也得是下面是普通的Date* 就不匹配了
{																												//而且也没有const Date*& 的匹配用法
	return *pa < *pb;
}

//template <>
//bool less<T*>(T* a, T* b)//像这种的限制性的偏特化也是不能实例化的，因为参数T是不确定的，也不是模板参数，还不如直接写个T*的模板，错的
//{
//	return 
//}

//template <T>
//bool less(T* a, T* b)//这个新模板也算特化的一种吧，会和上面的Date*相冲突，也能认为是新模板
//{
//	return *a < *b;
//}

void test2()
{
	Date d1(2022, 2, 25);
	Date d2(2022, 2, 22);
	cout << less(d1, d2) << endl;

	cout << less(&d1, &d2) << endl;//对于上面一种模板less可以比较，但是用指针就不同了，若还是比较的话就会变成比较地址。就会有错误
}

template <class T1,class T2>
void print(T1 a,T2 b)
{
	cout << "T1  T2" << endl;
	
}
//这算函数重载，函数没有偏特化
//template <class T1>
//void print<T1, int>(T1 a, int b)函数没有显示指定的偏特化，因为其偏特化是通过函数参数带入的，这是错的
//{
//	cout << "T1  int" << endl;
//}

template <class T1>
void print(T1 a, int b)//函数的偏特化不能显示指定，类的偏特化只能显示指定
{
	cout << "T1  int" << endl;
}



////利用日期类来排序
//int main()
//{
//	int a = 0;
//	int b = 0;
//	char c = 0;
//	print(a, c);//这是T1，T2
//	print(c, b);
//	
//	return 0;
//}



template<class T1, class T2>
class Data
{
public:
	Data(T1 a,T2 b) 
		:_a(a)
		,_b(b)
	{
		cout << "Data<T1, T2>" << endl; 
	}
private:
	T1 _a;
	T2 _b;
};

//全特化,类不是通过参数确认的实例，只能显示特化，也只有类能偏特化
template<>
class Data<int, int>
{
public:
	Data(int a,int b)
		:_a(a)
		,_b(b)
	{
		cout << "Data<int ,int>" << endl;
	}
private:
	int _a;
	int _b;
};

template<class T1>
class Data<T1, double>//类的
{
public:
	Data(T1 a, double b)
		:_a(a)
		, _b(b)
	{
		cout << "Data<T1 ,double>" << endl;
		cout << _a << " " << b << endl;
	}
private:
	T1 _a;
	double _b;
};

template <class T1,class T2>
class Data<T1*, T2*>
{
public:
	Data(T1* a ,T2* b)
		:_a(a)
		, _b(b)
	{
		cout << "Data<T1*,T2*>" << endl;
		cout << _a << " " << _b << endl;
	}
private:
	T1* _a;
	T2* _b;
};
int main()
{
	int a = 10;
	int b = 20;
	double c = 1.01;
	Data<int ,int> aa(10,20);
	Data<int, double> bb(b, c);
	Data<int*, double*> cc(&a, &c);
}

//非类型模板参数，例如array定长数组，这个容器是指定大小的，且非类型模板参数是常数，不能被修改
//非类型模板参数只能为整形（char也是），浮点型自定义没有意义
template <class T,size_t N>
class Da
{
	Da()
	{

	}
private:
	T _a[N];//这里指定了定长的大小。array是这样实现的
	size_t size;
};