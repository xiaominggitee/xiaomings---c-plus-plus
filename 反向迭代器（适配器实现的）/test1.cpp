#define _CRT_SECURE_NO_WARNINGS 1
#include "8-6vector实现.h"
#include "lish实现.h"
void test_vector()
{
	su::vector<int> v;
	v.push_back(1);
	v.push_back(22);
	v.push_back(3);

	auto it = v.rbegin();
	while (it != v.rend())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}
void test_list()
{
	std::list<int> lt;
	lt.push_back(1);
	lt.push_back(5);
	lt.push_back(2);
	lt.push_back(4);
	lt.push_back(3);
	std::list<int>::const_reverse_iterator it = lt.rbegin();
	while (it != lt.rend())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}

void test_list1()
{
	su::list<int> lt;
	lt.push_back(1);
	lt.push_back(5);
	lt.push_back(2);
	lt.push_back(4);
	lt.push_back(3);
	//su::list<int>::reverse_iterator it = lt.rbegin();//没有实现普通对象转const的问题，函数rbegin无法被const迭代器访问，只能对应的crbegin
	//while (it != lt.rend())
	//{
	//	cout << *it << " ";
	//	it++;
	//}
	//cout << endl;
	su::list<int>::const_reverse_iterator it = lt.crbegin();
	while (it != lt.crend())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}
int main()
{
	test_vector();

	return 0;
}

