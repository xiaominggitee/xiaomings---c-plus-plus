#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;


class weight
{
public:
	weight(int a = 10)
		:_a(a)
	{}
		
	weight(const weight& w)
	{
		_a = w._a;
		cout << "weight" << endl;
	}

	~weight()
	{
		cout << "~weight" << endl;
	}
private:
	int _a;
};

weight f(weight u)
{
	weight v(u);
	weight w = v;

	return w;
}

void test_1()
{
	weight x;//构造
	f(x);//四次构造四次析构

	weight();//匿名对象，生命周期只是这一行，构造完就析构，引用可以延长生命周期
	weight().~weight();//通常是为了调用函数而使用的。
}


void test_2()
{
	f(weight());//先构造在传参拷贝构造，但是一般编译器对于连续的构造都会优化，成为直接构造,所以这个传参没有拷贝构造

	weight a1(1);
	weight a2 = 2;//这也是连续的构造，优化成直接构造

	weight x;
	weight  ret = f(x);//连续的拷贝构造也会优化为一次拷贝构造
}

void test_3()
{
	int* a = (int*)calloc(4, sizeof(int));//4个整型的空间，初始化为0
	int* b = (int*)realloc(a, 4 * sizeof(int));

	int* p1 = new int;//new和molloc一样都是为指针开辟空间，而不是直接为实体初始化，如不能直接int a = new int;

	int* p2 = new int[10];//开辟十个整型空间，十个整型对象.没有初始化，为随机值
	int* p3 = new int(10);//开辟一个空间并初始化为10；
	//int* p4 = new int[10](10);//数组不可一起初始化

	int* p4 = new int[10]{ 10 };//大括号是c++11的列表初始化，如同数组只是初始化给出的下标元素，然后其他值会初始化为0.要是一个都没有那都是随机值
}


void test_4()
{
	int a = 10;
	int& ra = a;
	ra = 20;

	int* pa = &a;//引用和指针底层
	*pa = 20;
	
}


void test_5()
{

	int a = 10;

	//权限问题

	int& b = a;				//正常引用
	const int& c = a;		//权限缩小，可以引用
	//int& d = c;			//报错，int对象引用const int对象，权限放大，不可

	//引用的隐式类型转换

	//double& e = a;//报错，a会隐式转换为double类型，为临时对象，临时对象具有常属性
	const double& e = a;
}



int add(int a, int b);
inline int add(int a, int b)
{
return a + b;
}

//int main()
//{
//add(1, 2);
//}
