#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
//using std::cout;
//using std::cin;
//using std::endl;
//int main()
//{
//	int a, b, c, d;
//	cin >> a >> b >> c>>d;
//	cout << a << b << c << endl << d;
//	cout << "\nhello world" << '\n';//endl就是换行\n,也可打在字符串里面
//}

using namespace std;

//缺省参数或者默认参数
//缺省只能连着缺省，而且要从右往左，因为实参传进来是按照左到右的，否则会有歧义
//void Test(int a = 10, int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//
//}

//void Test(int a, int b = 20, int c = 30)//缺省参数只能从右边开始，不然只是中间和前面有缺省参数，那么Test（1）就会有歧义，给abc都不知道
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//
//}
//int main()
//{
//	Test();
//	Test(1);
//	Test(1,2);
//	Test(1,2,3);//有实参传进来用实参，没就用默认缺省值
//	Test(,1)这样空着是不行的，要连续
//
//}


//
//#include "4-30.h"
//
////缺省参数若是声明和定义同时存在的话，不能同时给缺省参数，因为怕不同值混淆了
////声明和定义不同文件，只能给声明，因为声明是在编译期间展开的，展开头文件之后再链接找定义，链接期间按照先出现的合并符号表
//// 如果是按照主函数是按照声明去确认定义的那么缺省只能给声明，若主函数文件中就包含了该缺省的定义，就可直接用
//// 因为声明是不是缺省的那主函数调用时就要传参了，不然编译过不了
////相同文件
////void Test(int a = 10, int b = 20, int c = 30);
////void Test(int a, int b, int c);
////void Test(int a = 10, int b = 20, int c = 30)
//////{
////	cout << "a = " << a << endl;
////	cout << "b = " << b << endl;
////	cout << "c = " << c << endl;
////
////}
//int main()
//{
//	Test();//靠声明去编译函数的时候要是声明没有使用缺省的话，这就会报错，因为实际的缺省在定义时，编译都过不了
//	Test(1);
//	Test(1, 2);
//	Test(1, 2, 3);//有实参传进来用实参，没就用默认缺省值
//	//Test(, 1)这样空着是不行的，要连续
//
//}



//
//函数重载，可以支持同名函数
//可在参数（个数，类型，类型顺序）有不同即可
//方便使用，swap函数就可以同时写几个，不用分开swap1，2，3
void swap(int* a, int* b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}
void swap(double* a, double* b)
{
	double temp = *a;
	*a = *b;
	*b = temp;
}

//int add(int a, int b)//这样是不构成重载的，因为会有歧义，名字调换并不是重载，要类型调换才可
//{
//	return a + b;
//}
//int add(int b, int a)
//{
//	return a + b;
//}
//short add(int b, int a)//只是返回值不一样也是不可以的，因为在main中调用时无法确定是哪一个返回值，只看参数，如调用add（a，b）
//{																									//不知道是用哪一个，会有歧义
//	return a + b;
//}
//int main()
//{
//	int a = 10, b = 20;
//	double c = 25.2, d = 32.1;
//
//	swap(&a, &b);
//	swap(&c, &d);//只是为了方便使用，使用但是还是得创建不同的类型函数，只是名字可以相同，后期模板可解决这个
//	cout << a << " " << b << endl;
//	cout << c << " " << d << endl;
//
//}

//
////命名空间回顾
//int a = 10;
//int main()
//{
//	int a = 20;
//	printf("%d ", a);
//	printf("%d ", ::a);//空格表示使用全局的命名空间
//}
//
////在大项目中一般是使用哪一个元素才放开他的命名空间，因为大项目的命名可能会混淆
////或者指定命名空间（这个写起来比较麻烦），或者引用放开部分
//
//void Test(int a, int b, int c);
////#include "4-30.h"
//#define M 10
//int main()
//{
////#if M==10
////	printf("yes");
////#elif
////	printf("NO");
////#endif
//	
//
//	Test(1, 2, 3);
//
//
//}