#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <stack>
using namespace std;

//原地改线索二叉树
//根据前中序序列，确认一棵树
//根据中后序序列，确认一棵树
//前中后序非递归实现


struct TreeNode {
	int val;
	struct TreeNode* left;
	struct TreeNode* right;
	TreeNode(int x) :
		val(x), left(NULL), right(NULL) {
	}
};

//改线索二叉树
//直接中序递归，要求是不能创建新节点，故在原地改链接
class Solution {
public:
	void InOrder(TreeNode* cur, TreeNode*& prev)
	{
		if (cur == nullptr)
			return;
		InOrder(cur->left, prev);
		cur->left = prev;
		if (prev != nullptr)
			prev->right = cur;
		prev = cur;
		InOrder(cur->right, prev);
	}
	TreeNode* Convert(TreeNode* pRootOfTree) {
		//中序遍历，改节点的链接，线索二叉树
		TreeNode* prev = nullptr;
		InOrder(pRootOfTree, prev);
		while (pRootOfTree != nullptr && pRootOfTree->left)//防止一开始就是空
		{
			pRootOfTree = pRootOfTree->left;
		}
		return pRootOfTree;
	}
};


//前序中序，确认一棵树
class Solution {
public:
	TreeNode* _buildTree(vector<int>& preorder, vector<int>& inorder, int& prei, int inbegin, int inend)
	{
		if (inbegin > inend)
			return nullptr;
		TreeNode* root = new TreeNode(preorder[prei]);
		prei++;
		int ri = inbegin;
		while (inorder[ri] != root->val)
		{
			ri++;//找中序的根节点
		}
		root->left = _buildTree(preorder, inorder, prei, inbegin, ri - 1);
		root->right = _buildTree(preorder, inorder, prei, ri + 1, inend);
		return root;
	}
	TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
		//前序是确定根的，中序是根据根来确认左右子树范围的。[begin,root-1],root,[root+1,end]
		//类似与快排的范围，但是这里需要去寻找

		int inbegin = 0;
		int inend = inorder.size() - 1;//闭区间
		int prei = 0;
		return  _buildTree(preorder, inorder, prei, inbegin, inend);
	}
};

//中序后序确认一棵树
class Solution {
public:
	TreeNode* _buildTree(vector<int>& inorder, vector<int>& postorder, int& posti, int inbegin, int inend)
	{
		if (inbegin > inend)
			return nullptr;
		TreeNode* root = new TreeNode(postorder[posti]);
		posti--;
		int ri = inbegin;
		while (inorder[ri] != root->val)
		{
			ri++;//找中序的根节点
		}
		root->right = _buildTree(inorder, postorder, posti, ri + 1, inend);//根之后到右子树,这个posti是先右子树的
		root->left = _buildTree(inorder, postorder, posti, inbegin, ri - 1);
		return root;
	}
	TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
		int inbegin = 0;
		int inend = inorder.size() - 1;//闭区间
		int posti = postorder.size() - 1;
		return  _buildTree(inorder, postorder, posti, inbegin, inend);
	}
};

//前序非递归
class Solution {
public:
	vector<int> preorderTraversal(TreeNode* root) {
		//遍历树改非递归，把树分为左节点和右子树，无论什么序，左节点都是先进去的
		stack<TreeNode*> st;
		vector<int> v;
		TreeNode* cur = root;
		while (cur != nullptr || !st.empty())
		{
			//放入左节点
			while (cur)
			{
				v.push_back(cur->val);
				st.push(cur);
				cur = cur->left;
			}
			TreeNode* top = st.top();
			st.pop();//访问其右子树前得出该节点，否则之后无法辨别到哪个节点了

			if (top->right)
				cur = top->right;
		}
		return v;
	}
};

//中序非递归
class Solution {
public:
	vector<int> inorderTraversal(TreeNode* root) {
		//遍历树改非递归，把树分为左节点和右子树，无论什么序，左节点都是先进去的
		stack<TreeNode*> st;
		vector<int> v;
		TreeNode* cur = root;
		while (cur != nullptr || !st.empty())
		{
			//放入左节点
			while (cur)
			{
				st.push(cur);
				cur = cur->left;
			}
			TreeNode* top = st.top();
			v.push_back(top->val);//出栈的时候再访问
			st.pop();//访问其右子树前得出该节点，否则之后无法辨别到哪个节点了

			if (top->right)
				cur = top->right;
		}
		return v;
	}
};

//后序非递归
class Solution {
public:
	vector<int> postorderTraversal(TreeNode* root) {
		//后序遍历和前序中序的判断方法不一样，先访问完右节点再去访问自己
		TreeNode* cur = root;
		stack<TreeNode*> st;
		vector<int> v;
		TreeNode* prev = nullptr;
		while (cur || !st.empty())
		{
			while (cur)
			{
				st.push(cur);
				cur = cur->left;
			}
			TreeNode* top = st.top();

			if (top->right == nullptr || prev == top->right)
			{
				st.pop();
				v.push_back(top->val);
				prev = top;
			}
			else
				cur = top->right;
		}
		return v;
	}
};