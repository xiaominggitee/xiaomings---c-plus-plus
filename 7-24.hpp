#pragma once
#include <iostream>
using std::cout;
using std::endl;

//头文件hpp是声明和定义的结合，也就是头文件和cpp文件合并。当然可以写成h，但hpp能更好表述

//template<class T = char>
template<class T>//模板可以缺省，缺省的时类型，在声明处添加char等
void swap(T& a, T& b);//声明


template<class T>
class stack
{
public:
	stack(int capacity = 10)
	{
		_a = new int[capacity];
		_top = 0;
		_capacity = capacity;//对于const，引用，自定义非默认类型（这个因为必须手动传参）这三必须在定义时候初始化，初始化列表
	}
	void push(const T& d);


private:
	T* _a;
	int _top;
	int _capacity;

};