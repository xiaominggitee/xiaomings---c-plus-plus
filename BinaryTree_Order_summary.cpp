#define _CRT_SECURE_NO_WARNINGS 1
#include <vector>
#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

//前中后序的非递归总结，都能用栈实现，只需改一下访问的时机，后序的访问时机需要单独判断。其中中序的三叉链可以判断左右节点实现


//前序，中序，后序用同一个方法，不同条件访问，后序需要判断该节点是从左边来的还是右边来的。用个prev来判断
//前序
vector<int> preorderTraversal(TreeNode* root) {
    //遍历树改非递归，把树分为左节点和右子树，无论什么序，左节点都是先进去的
    stack<TreeNode*> st;
    vector<int> v;
    TreeNode* cur = root;
    while (cur != nullptr || !st.empty())
    {
        //放入左节点
        while (cur)
        {
            v.push_back(cur->val);
            st.push(cur);
            cur = cur->left;
        }
        TreeNode* top = st.top();
        st.pop();//访问其右子树前得出该节点，否则之后无法辨别到哪个节点了

        if (top->right)
            cur = top->right;
    }
    return v;
}

//中序
vector<int> inorderTraversal(TreeNode* root) {
    //遍历树改非递归，把树分为左节点和右子树，无论什么序，左节点都是先进去的
    stack<TreeNode*> st;
    vector<int> v;
    TreeNode* cur = root;
    while (cur != nullptr || !st.empty())
    {
        //放入左节点
        while (cur)
        {
            st.push(cur);
            cur = cur->left;
        }
        TreeNode* top = st.top();
        v.push_back(top->val);
        st.pop();//访问其右子树前得出该节点，否则之后无法辨别到哪个节点了

        if (top->right)
            cur = top->right;
    }
    return v;
}

//后序非递归，
vector<int> postorderTraversal(TreeNode* root) {
    //遍历树改非递归，把树分为左节点和右子树，无论什么序，左节点都是先进去的
    stack<TreeNode*> st;
    vector<int> v;
    TreeNode* cur = root;
    TreeNode* prev = nullptr;
    while (cur != nullptr || !st.empty())
    {
        //放入左节点
        while (cur)
        {
            st.push(cur);
            cur = cur->left;
        }
        TreeNode* top = st.top();
        //后序访问top节点有两情况，因为是左右中，自己的前一个就是prev，是自己的右节点的时候就该访问自己了
        if (top->right == nullptr||prev==top->right)
        {
            v.push_back(top->val);
            prev = top;
            st.pop();
        }
        else
        {
            cur = top->right;
        }
    }
    return v;
}

//中序的非递归，用于红黑树封装的map和set的迭代器++，--，适用于三叉链
// 

//找下一个：看该节点的右节点是否为空来判断，1.右节点不为空，下一个是右子树的最左节点，2.右子树为空，下一个是祖先节点，为孩子是父亲的左的父亲
//找上一个：看该节点的左节点是否为空来判断，1.左节点不为空，上一个是左子树的最右节点，2.左子树为空，上一个是祖先节点，为孩子是父亲的右的父亲
//上下的情况刚好相反。
self& operator++()
{
    //++,是中序遍历的后一个，即若是用stack实现遍历的话也可以，但是比较麻烦了，对于三叉树来说可以直接判断的
    if (this->_node->_right)
    {
        //该节点右子树不为空，下一个就是右子树的最左节点
        Node* cur = this->_node->_right;
        while (cur->_left)
        {
            cur = cur->_left;
        }
        this->_node = cur;
    }
    else
    {
        //该节点的右子树为空，说明已经走到这颗子树的最后了，得回去找祖先，找到孩子是父亲的左节点，这个父亲就是下一个
        //因为中序是左中右，若是这个祖先是某个节点的左，那么下一个就是这某个节点，因为左中右的缘故
        Node* child = this->_node;
        Node* parent = child->_parent;
        while (parent && child == parent->_right)//parent不存在说明就是根了。根就到end了返回空就行，因为这实现的是无哨兵的
        {
            child = parent;
            parent = child->_parent;
        }

        this->_node = parent;//空也是赋值
    }
    return *this;
}

self& operator--()
{
    //--是++的反过来，看其左节点就行
    if (this->_node->_left)
    {
        //左节点不为空，说明其左树已经访问完了，左树的最右边的节点就是其上一个节点
        Node* cur = this->_node->_left;
        while (cur->_right)
        {
            cur = cur->_right;
        }
        this->_node = cur;
    }
    else
    {
        //左节点为空，说明前一个节点是你的祖先，找到孩子是父亲的右节点，该父亲就是上一个节点。
        //因为中序是左中右，某个祖先节点右子树的最左节点就是现在的这个节点，找回他祖先就行
        Node* child = this->_node;
        Node* parent = child->_parent;
        while (parent && child == parent->_left)
        {
            child = parent;
            parent = child->_parent;
        }
        this->_node = parent;
    }
    return *this;
}