#define _CRT_SECURE_NO_WARNINGS 1
#include <string>
#include <iostream>
using namespace std;

namespace su {
	class string
	{
	private:
		char buff[16];//size=15，一个\0
		char* ptr;
		size_t size;
		size_t capacity;
	};
}

int main()
{

	string s1("111111");
	string s2("22222222222222222222222222222");

	cout << sizeof(s1) << endl;
	cout << sizeof(s2) << endl;
	return 0;
}

//扩容（正确的版本）
void reserve(size_t n)
{
	size_t size = _size();
	if (n > _capacity())
	{
		//vector<T> tmp;
		//tmp._start = new T[n];
		//tmp._finish = tmp._start+size;//不可创建临时的一个类对象，因为出了作用域便会析构。故造成内存泄露
		//tmp._end_of_storage = tmp._start+size;
		T* tmp = new T[n];//只能通过直接创建一个堆的空间，才可以延长其生命周期
		if (_start)
		{
			//memcpy(tmp, _start, size * sizeof(T));//这个memcpy是一个浅拷贝，对于深一层次的深拷贝会有问题，如vector的类型是vector或string这些管理资源的类型

			//故需要利用到循环赋值来解决这个深层次的深拷贝问题，因为其所实现的赋值也是深拷贝
			for (size_t i = 0; i < size; i++)
			{
				//tmp._start[i] = _start[i];
				tmp[i] = _start[i];
			}
			delete[] _start;
		}
		//_start = tmp._start;
		_start = tmp;
		/*_finish = _size();*/	//这个就会由问题，因为_size()用的就是两个指针相减，而这个时候_start已经发生变化了,故需要提前保存你的szie
	}
	_finish = _start + size;
	_end_of_storage = _start + n;
}
