#include <iostream>
#include <stack>;
using namespace std;


//O（1）访问到栈的最小数
class MinStack {
public:
    MinStack() {

    }

    void push(int val) {
        st.push(val);
        //创建一个保存最小数的栈，小于等于val都要入，top等于pop的数就出
        if (minst.empty() || val <= minst.top())
        {
            minst.push(val);
        }
    }

    void pop() {
        if (st.top() == minst.top())
        {
            minst.pop();
        }
        st.pop();//里面会控制是否为空

    }

    int top() {
        return st.top();
    }

    int getMin() {
        return minst.top();
    }
private:
    stack<int> st;
    stack<int> minst;
};

#include <vector>
using namespace std;

class Solution {
public:
    bool IsPopOrder(vector<int> pushV, vector<int> popV) {
        stack<int> st;//用栈模拟实现出栈入栈顺序

        vector<int>::iterator it1 = pushV.begin();
        vector<int>::iterator it2 = popV.begin();
        while (it2 != popV.end())
        {
            if (st.empty() || *it2 != st.top())
            {
                //考虑有成员没入栈
                if (it1 == pushV.end())
                    return false;//这里不匹配且没进栈元素了
                st.push(*it1++);

            }
            else
            {
                //栈顶和所要出的元素相同
                it2++;
                st.pop();
            }
        
        }

        //   //或者先进来再循环判断出去
        //while (it1 != pushV.end())
        //{
        //    st.push(*it1++);
        //    while (!st.empty() && *it2 == st.top())
        //    {
        //        it2++;
        //        st.pop();
        //    }

        //}

        //return st.empty();;
    }

private:
};

int main()
{
    vector<int> pushV;
    pushV.push_back(1);
    //pushV.push_back(2);
    //pushV.push_back(3);
    //pushV.push_back(4);
    //pushV.push_back(5);

    vector<int> popV;
    popV.push_back(2);
    //popV.push_back(5);
    //popV.push_back(3);
    //popV.push_back(2);
    //popV.push_back(1);
    Solution s1;
    s1.IsPopOrder(pushV, popV);
    return 0;
}