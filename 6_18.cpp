#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

struct info
{
	int num;
	char a;
};
//输入字符串统计字符并输出到文件
void in_out()
{
	FILE* fp = fopen("outfile.txt", "w");
	if (!fp)
		return;
	char str[100];
	info s[36];
	for (int i = 65; i <= 90; i++)
	{
		s[i-65].a = i;
		s[i - 65].num = 0;
	}
	for (int i = 48; i <= 57; i++)
	{
		s[i-48+26].a = i;
		s[i - 48 + 26].num = 0;
	}
	cout << "输入字符串：";
	cin >> str;
	char* s1 = str;
	while (*s1 != '\0')
	{
		if ((*s1 >= 65 && *s1 <= 90) || (*s1 >= 48 && *s1 <= 57))
		{
			int i = 0;
			//找对应位置
			for ( i = 0; i < 36; i++)
			{
				if (s[i].a == *s1)
					break;
			}
			s[i].num++;
		}
		s1++;
	}
	//输出
	for (int i = 0; i < 36; i++)
	{
		if (s[i].num > 0)
		{
			fprintf(fp, "%c %d\n", s[i].a, s[i].num);
		}
	}
}

struct line
{
	int num;
	line* next;
};
//单链表的选择排序
void select_sort(line* head)
{
	//带头双向
	line* mcur = head;
	while (mcur)
	{
		line* cur = mcur;
		line* temp=cur;
		int min = cur->num;
		while (cur)
		{
			//找单趟最小的和head换数值
			if (cur->num < min)
			{
				min = cur->num;
				temp = cur;
			}
			cur = cur->next;
		}
		//交换
		if (cur != temp)
		{
			int a = temp->num;
			temp->num = mcur->num;
			mcur->num = a;
		}
		mcur = mcur->next;
	}
	mcur = head;
	while (mcur)
	{
		cout << mcur->num<<" ";
		mcur = mcur->next;
	}
}
void test1()
{	/*in_out();*/
	line* n1 = (line*)malloc(sizeof(line));
	line* n2 = (line*)malloc(sizeof(line));
	line* n3 = (line*)malloc(sizeof(line));
	line* n4 = (line*)malloc(sizeof(line));
	line* n5 = (line*)malloc(sizeof(line));
	n1->num = 545;
	n2->num = 214;
	n3->num = 665;
	n4->num = 9;
	n5->num = 8;
	n1->next = n5;
	n5->next = n2;
	n2->next = n4;
	n4->next = n3;
	n3->next = nullptr;
	select_sort(n1);
}


//折半查找的递归
int binarySearch(int* a,int left,int right,int key)
{
	//左闭右开,要一直保持是左闭右开才可以
	int mid = left + (right - left) / 2;
	if (left >= right)
		return -1;
	if (a[mid] == key)
		return mid;
	else if (a[mid] > key)
		return binarySearch(a, left, mid, key);
	else
		return binarySearch(a, mid+1, right, key);
}
//int main()
//{
//	int a[] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (int i = 0; i < 12; i++)
//	{
//		cout << binarySearch(a, 0, 10, i) << " ";
//	}
//	//cout << binarySearch(a, 0, 9, 10);
//	return 0;
//}
//


#include <stdio.h>
#include <stdlib.h>
int check_sys()
{
	union
	{
		int i;
		char c;
	}un;
	un.i = 1;
	return un.c;
}
int main()
{
	int ret = check_sys();
	int a = 0x11223344;
	if (ret == 1)
	{
		printf("小端\n");
	}
	else
	{
		printf("大端\n");
	}
	return 0;
}
