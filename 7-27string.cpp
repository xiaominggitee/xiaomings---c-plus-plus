#define _CRT_SECURE_NO_WARNINGS 1
#include <string>
#include <iostream>
//#include <stdd>
using namespace std;
void test8()
{
	//insert
	string s1("hello");
	string s2("world");

	//迭代器插入
	s1.insert(s1.begin(), 'x');
	s1.insert(s1.begin(), 6, 'x');
	//s1.insert(s1.begin(), "xxxx");不可迭代器插入字符串，迭代器只能字符插入
	cout << s1 << endl;


	//下标位置插入
	s1.insert(0, s2);
	s1.insert(0, "ee");
	s1.insert(0, 1, 'x');//字符插入只能携带个数插入
	cout << s1 << endl;
	
	//也可插入子串
	s1.insert(s1.size() - 1, "asfdhiuashd", 5);//插入前几个
	cout << s1 << endl;

	s1.insert(s1.size() - 1, s2,1,5);//插入子串
	cout << s1 << endl;

	s1.insert(s1.size() - 1, "asfdhiuashd", 1,5);//插入前几个
	cout << s1 << endl;

}

void test9()
{
	//erase,删除
	string s1("hello");
	string s2("world");

	//删列表（下标）
	s1.erase(3);//默认删除4到npos缺省的个数，删完
	cout << s1 << endl;

	s1.erase(1, 2);//一位置后面两个字符
	cout << s1 << endl;

	s2.erase(s2.begin() + 2, s2.end());
	cout << s2 << endl;


	//删单字符
	s2.erase(s2.begin());//单删迭代器指向的字符
	cout << s2 << endl;


}

void test10()
{
	//swap
	string s1("hello");
	string s2("world");

	s1.swap(s2);//库里面的，直接交换指针，效率高
	cout << s1 << endl;
	cout << s2 << endl;

	swap(s1, s2);//创建空间拷贝，深拷贝，效率低
	cout << s1 << endl;
	cout << s2 << endl;
}

void test11()
{
	//find以及rfind，还有substr

	//当我们需要得到一个文件的后缀（找点 .），或者一个url的协议名时，我们就需要find以及substr得到，要是有多个就可以用rfind从后面找或find_first_of等等

	//举例，https://cplusplus.com/reference/string/string/  得到它的协议，http

	string url1("https://cplusplus.com/reference/string/string");
	size_t pos1 = url1.find(':');
	pos1 = url1.find("://");//找字符或者找串都是一样，返回首个字符的下标位置，失败返回npos
	if (pos1 != string::npos)
	{
		//由于是下标位置，而不是返回地址，故需要用substr取子串
		cout << url1.substr(0, pos1);//pos1是下标位置，刚好是之后的数量

	}

}

void test12()
{
	//c_str
	//按照c的要求返回c的字符串格式，即使不再是string类，而是首元素的地址，const的地址
	string s1("hello");
	const char* str = s1.c_str();

	cout << (void*)str << endl;
	cout << str << endl;

}
//
//int main()
//{
//	test12();
//	return 0;
//}