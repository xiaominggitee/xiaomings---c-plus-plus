//#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
//
//namespace A
//{
//	int a;			
//	int b = 10;		
//
//	int Add(int a, int b);
//	int Add(int a, int b)
//	{
//		return a + b;
//	}
//
//	namespace B
//	{
//		int c;
//		int d;
//		int Sub(int left, int right)
//		{
//			return left - right;
//		}
//	}
//}
////printf("%d",A::a)
////2. 命名空间可以嵌套
//namespace A
//{
//	int a;
//	int b;
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//	namespace N3
//	{
//		int c;
//		int d;
//		int Sub(int left, int right)
//		{
//			return left - right;
//		}
//	}
//}


//using namespace std;
//
////重载
//
//int func(int a, int b)		{	}		//原函数
//
//int func(int a)				{	}		//个数不同
//
//int func(int a, double b)	{	}		//类型不同
//
//int func(double a, int b)	{	}		//顺序不同，注意是类型顺序而不是变量顺序
//	
//
//extern "C";
//
//int main()
//{
//	func(11);
//}

//extern "C" {
#include "../stack_C/stack.h"
    bool isValid(const char* s) {

        assert(s);
        ST ps = { 0 };
        StackInit(&ps);

        while (*s)
        {
            if ((*s == '(') || (*s == '{') || (*s == '['))
            {
                StackPush(&ps, *s);
                s++;
            }
            else
            {
                if (StackEmpty(&ps))                  //如果再这为空说明该开始就是错的
                    return false;
                char top = Stacktop(&ps);//*s只包含括号，所以只需要考虑括号的比就行
                StackPop(&ps);
                if ((*s == ']' && top != '[') || (*s == '}' && top != '{') || (*s == ')' && top != '('))
                    return false;
                else
                {
                    s++;
                }
            }

        }

        if (StackEmpty(&ps))
            return  true;
        return false;



    }


    int main()
    {
        printf("%d\n", isValid("{[]}"));
        printf("%d\n", isValid("{[}]"));

        return 0;
    }
//}
