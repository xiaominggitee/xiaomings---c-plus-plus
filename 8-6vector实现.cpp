#define _CRT_SECURE_NO_WARNINGS 1
#include"8-6vector实现.h"


void test1()
{
	su::vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(5);

	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
	//v1.pop_back();
	//v1.pop_back();
	//v1.pop_back();
	//cout << v1.empty() << endl;
	//for (auto e : v1)
	//{
	//	cout << e << " ";
	//}
	//cout << endl;
	//v1.pop_back();
	//v1.pop_back();
	//cout << v1.empty() << endl;
	//for (auto e : v1)
	//{
	//	cout << e << " ";
	//}
	v1.clear();
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test2()
{
	su::vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(5);
	su::vector<int> v2(v1);
	std::string s1("hello");
	su::vector<int> v3(s1.begin(), s1.end());

	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
	for (auto e : v2)
	{
		cout << e << " ";
	}
	cout << endl;
	for (auto e : v3)
	{
		cout << e << " ";
	}
	cout << endl;

	v1 = v3;
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

}

void test3()
{
	//vector的两大迭代器失效问题，1.野指针（空间变换导致），2.迭代器失去意义（linux比较佛系，vs相对比较严格，但也不一定检测出来）
	//迭代器失效不一定检查出来，但是可能会倒是意想不到的后果，故利用迭代器时候要小心
	su::vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(5);

	//偶数位置插入二十
	auto it = v1.begin();
	while (it != v1.end())
	{
		if (*it % 2 == 0)
		{
			it = v1.insert(it,20);//这里不赋值的话，迭代器要是扩容就会失去意义，故需要接收新的pos迭代器返回值
			it++;
			//插入之后迭代器位置还是不变，不++的话，迭代器便失去意义失效，故都要++

		}
		it++;
	}
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;



	//删除
	auto it1 = v1.begin();
	while (it1 != v1.end())
	{
		if (*it1 % 2 == 0)
		{
			it1 = v1.erase(it1);
		}
		else
		{
			it1++;//删除是返回的刚才删位置的下一位，即原本那个相对位置
		}
	}
	v1.erase(v1.begin());
	v1.erase(v1.end()-1);
	//v1.erase(v1.begin());
	//v1.erase(v1.begin());

	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test4()
{
	int b = int();
	int a = int(10);//这里说明内置类型也有构造
	cout << a <<" "<<b << endl;


	//间接寻址问题，编译器找最匹配函数
	su::vector<int> v1(10, 2);//有一个迭代器区间构造，19检查较为严格，没有直接进去，但是他是比较匹配的
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

}

int main()
{
	test4();
	return 0;
}
