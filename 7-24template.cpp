#define _CRT_SECURE_NO_WARNINGS 1

#include "7-24.hpp"
//模板,(函数模板和类模板)

//模板可以声明和定义分离，但不提倡声明和定义分在不同文件，会导致链接错误，因为模板的声明和定义都是空壳子，没有实例化，不知道具体类型就无法生成符号表对应实例函数或类
//若硬要声明和定义分离，只能在定义处给模板生成指定实例。



//template<typename T>//现阶段typename和class的模板类型是相同的
template<class T>//模板可以缺省，缺省的时类型，在声明处添加char等
void swap(T& a, T& b)
{
	T temp = a;
	a = b;
	b = temp;//两个文件，这是定义
}


template<class T>//这是定义
void stack<T>::push(const T& d)
{
	//数组
	if (_top + 1 == _capacity)
	{
		//扩充
	}
	_a[_top++] = d;
}
//.........................................................................这里没有显式实例化指定，报链接错误



template
void swap<int>(int& a, int& b);//加一个template表示这是模板的实例化

template
void swap<double>(double& a, double& b);

template
class stack<int>;//方案一：显式指定实例化，方案二：声明定义放同一个头文件，目的是使模板能够生成指定的实例
				//同一文件展开被调用是已知具体类型的，编译完成后就会有实例,语法语义分析之后等到汇编之前已经生成实例才有符号表映射

//.........................................................................这个方案比较挫，最好声明定义放同一个文件



//模板参数类比函数参数
template <class k =char ,class v = char>
void func()
{
	cout << sizeof(k) << endl;
	cout << sizeof(v) << endl;

}
int main()
{
	func<int, int>();
	func<int>();
	func();
	func<>();//这样也可以
}