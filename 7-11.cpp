﻿#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
//
////struct a
////{
////	double d;
////	int s[10];
////	char* s1;
////};
////
////struct b
////{
////	struct a* a1;
////};
////int main()
////{
////	struct b b1 = { 0 };
////	struct b* b2 = NULL;
////	if(b1.a1->s)
////		printf("%x", b1.a1->s);//数组名是逻辑上存在的一个东西，空指针（0）里面有其偏移量，创建时的结构体也是0，故空指针能访问到数组名。
////	//if(b2->a1)				//其他情况，空指针访问元素都会报错
////	//	printf("%x", b2->a1->s);
////
////	return 0;
////}
//
//
//class A
//{
//
//public:
//	void print()
//	{
//		cout << _a << endl;
//	}
//	
//	~A()
//	{
//		_a = 0;
//	}
//	//~A(int a =0)//析构函数无参数无返回值，故无法重载，构造函数有重载，拷贝构造是特殊的构造，有且只有一个参数是引用传参
//	{
//		_a = 0;
//	}
//
//private:
//	int _a;
//};
//
//int mian()
//{
//	A* p = nullptr;
//	p->print();//p是空，但print不是在p里面的。故在这不会报错，而_a是p里面的，即用空指针访问元素就是错
//}

int count = 0;
class A
{
public:
	A(int a=10)
	{
		_a = a;
	}
	A(const A& w) 
	{
		this->_a = w._a;
		::count++;
	}


	//operator=赋值的默认生成是值拷贝
	A operator=(const A& a) 
	{
		
		this->_a = a._a;
		return *this;
	}
	A* operator&()
	{
		return this;
	}

	const A* operator&() const
	{
		return this;
	}

	A f()
	{
		return *this;
	}
	
private:
	int _a;
};

A func(A u)
{
	A v(u);
	A w = v;
	return w;
}


void test1()
{
	//A x;
	//A y = func(func(x));
	//cout << ::count << endl;

	//A x;
	//A y = func(x);//这里用返回的临时对象初始化y。拷贝构造为const则不会报错
	//A& j = func(x);//这里就是权限的放大了，const变为普通变量
}


void test()
{
	A a(20);
	//A& b =a;
	A b = a.f();

	const int& c = 10;
	printf("sdhuaidhasiof");
	printf("%s", "sdsadfaf");
}
int main() 
{
	int a = 10;
	float b = 12.12;
	a = b;
	//int& c = b;报错
	const int& c = b;
	const double d = a;
	



}



//日期类
class Date
{
public:
	//int Getmonthsday(int year,int month)
	//{
	//	static days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	//	if()

	//}


			//if (year > 0 && month > 0 && month <= 12 && day <= Getmonthsday() && day > 0)
		//{

		//}
	Date(int year = 1, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

private:
	int _year;
	int _month;
	int _day;
};
int main()
{
	Date d1;
	Date d2(2022, 5, 20);

	Date d3(d1);
	Date d4 = 2022;//隐式类型转换，再拷贝构造

	Date d5 = (2021, 4, 3, 5);//逗号表达式
}