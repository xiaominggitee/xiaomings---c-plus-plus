#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

template <class T>
class stack
{
public:
	stack(int capacity = 10)
	{
		_str = new T[capacity];
		_top = 0;
		_capacity = 10;//有效数据容量
	}
	~stack()
	{
		delete[] _str;
		_top = 0;
		_capacity = 0;
	}
	void print()
	{
		cout << "输出" << endl;
	}
private:
	T* _str;
	size_t _top;
	size_t _capacity;
};

//extern int c;
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	const int* ppa = &a;
//	*pa = 20;
//	a = c;
//}


void reverseString(string& s) {
	size_t begin = 0;
	size_t end = s.size() - 1;//下标
	while (begin < (int)end)
	{
		swap(s[begin], s[end]);
		begin++;
		end--;
	}
	for (auto ch : s)
	{
		cout << ch << ' ';
	}

}
int main()
{
	string s("hello");
	reverseString(s);
	return 0;
}