#define _CRT_SECURE_NO_WARNINGS 1
#include <algorithm>
#include <string>
using namespace std;
//自定义类型的仿函数排序，以及优先级队列
//拿排序来说明
struct Goods
{
	string _name;
	double _price;
	size_t _saleNum;

	//对于自定义类型这种多个变量选择的排序，在类里面实现运算符重载就显得不够用
	bool operator<(const Goods& gds)
	{
		return _name.compare(gds._name)<0;
	}
};

struct LessPrice
{
	bool operator()(const Goods& gds1, const Goods& gds2)
	{
		return gds1._price<gds2._price;
	}
};
struct GreaterPrice
{
	bool operator()(const Goods& gds1, const Goods& gds2)
	{
		return gds1._price > gds2._price;
	}
};
struct LessSaleNum
{
	bool operator()(const Goods& gds1, const Goods& gds2)
	{
		return gds1._saleNum < gds2._saleNum;
	}
};
struct GreaterSaleNum
{
	bool operator()(const Goods& gds1, const Goods& gds2)
	{
		return gds1._saleNum > gds2._saleNum;

	}
};

int main()
{
	Goods gds[4] = { { "苹果", 2.1, 1000}, { "香蕉", 3.0, 200}, { "橙子", 2.2,300}, { "菠萝", 1.5,50} };

	sort(gds, gds + 4);//按照自己默认的比较方式，因为函数<能重载，但是参数每个都相同，无法重载
	//若要实现每个参数的<重载，得在sort中写到比的是里面的参数类型。写死if(price1>price2)才交换，而不是模板带入的goods类型
	//且重载运算符比较的是那个类型，是不能实现多个重载的。这时候运算符重载就显得乏力
	

	//于是仿函数就能很好解决自定义类型的比较方式了
	
	sort(gds, gds + 4, LessPrice());
	sort(gds, gds + 4, GreaterPrice());
	sort(gds, gds + 4, LessSaleNum());
	sort(gds, gds + 4, GreaterSaleNum());
}