#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

//对于继承下来的构造函数，析构函数，拷贝构造，赋值运算符重载等，都是基类成员调用基类的成员函数，派生类的自己像普通对象一般处理

class Person
{
public:
	Person(const char* name)
		: _name(name)
	{
		cout << "Person()" << endl;
	}

	Person(const Person& p)//拷贝构造
		: _name(p._name)
	{
		cout << "Person(const Person& p)" << endl;
	}

	Person& operator=(const Person& p)//这里没有对空间的管理，直接是浅拷贝即可，手动写拷贝构造要注意判断地址问题，否则会被提前析构报错
	{
		cout << "Person operator=(const Person& p)" << endl;
		if (this != &p)
			_name = p._name;

		return *this;
	}

	~Person()
	{
		cout << "~Person()" << endl;
	}
protected:
	string _name; // 姓名
};

class Student : public Person
{
public:
	Student(const char* name = "", int num = 0)
		:_num(num)
		, Person(name)//如同直接调用其构造一般直接显示写，构造和析构函数能显示调用a.A::A()，但不意味着创建或销毁了空间，因为此时以及创捷了a1,而a1也只能在生命周期结束时自动销毁
	{
		cout << "Student(const char* name = "", int num = 0)" << endl;
	}

	Student(const Student& s)
		:Person(s)//这里是切片，手动写的话（带入值需要手动写，且person没有默认构造）
		, _num(s._num)
	{
		cout << "Student(const Student& s)" << endl;
	}

	// s1 = s3
	Student& operator=(const Student& s)
	{
		if (this != &s)
		{
			Person::operator=(s);//这里构成隐藏，切片
			_num = s._num;
		}

		cout << "Student& operator=(const Student& s)" << endl;

		return *this;
	}



	//析构函数由于多态的原因会被统一命名为destructor(),重要原因也是其无法重载，构造能重载。这便导致隐藏问题
	//所以不能直接用~person()去调用基类的析构，需要指定作用域
	//最重要一个东西是，析构函数无法像构造一样显示写，为保证析构顺序，按照堆栈（先创建的后析构），故析构函数会在结束前自动调用父类以及内涵自定义类型的析构
	~Student()
	{
		//Person::~Person();

		cout << "~Student()" << endl;
	}// -》自动调用父类析构函数
protected:
	int _num; //学号
	//string _addrss = "西安";
};

 //子类构造函数原则：
 //a、调用父类构造函数初始化继承自父类成员
 //b、自己再初始化自己的成员 -- 规则参考普通类
 //析构、拷贝构造、复制重载也类似
int main()
{
	Student s1("李四", 1);
	Student s2(s1);
	Student s3("王五", 2);
	s1 = s3;

	return 0;
}
