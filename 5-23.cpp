#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;


//第四个类的默认成员函数，赋值重载函数，与构造函数的特点基本一样，不写编译器生成，有返回值，参数是一个，自动生成的是浅拷贝
class Date
{
public:

	int Getyearmonth(int year, int month)
	{
		int montharray[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))//二月且为闰年
			montharray[month]++;
		return montharray[month];
	}
	Date(int year = 2022, int month = 5, int day = 23)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	Date(const Date& d,int a = 10)//拷贝构造，且防止d被修改,拷贝构造只有一个参数，就是被拷贝函数的别名，当然有其他函数也可。但是没必要
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	bool operator==( const Date d2/*，int c = 0*/)//只能两个操作数，因为==是双目运算符,参数是有规定的，按照其运算符的作用决定
	{
		return _day == d2._day && _month == d2._month && _year == d2._year;
	}

	bool operator<(const Date d2)//编译器补齐执行为(Date* const this, Date d)
	{
		if (_year < d2._year)
			return true;
		if (_year==d2._year&&_month <d2._month)
			return true;
		if (_year == d2._year && _month == d2._month && _day < d2._day)
			return true;
		return false;
	}

	//赋值运算符首先要知道的是，赋值运算符是有返回值的，而且不是const，因为赋值是支持连等的，而且也支持被修改(a=b)=c
	Date& operator=(const Date& d)//这个引用是*this的别名，而this是它的指针
	{
		if (this != &d)//若是两个对象是一样的就不需要赋值了
		{
			_day = d._day;
			_month = d._month;
			_year = d._year;
		}
		return *this;//注意返回值的话是通过临时对象或者寄存器返回的，而自定义类型无论大小都是临时对象返回，就要调用拷贝构造函数初始化拷贝的对象
		//而这个赋值运算符出了作用域对象依然存在，可以用引用
	}

	//Date operator++();//没有操作数
	//Date operator+(int day);//加天数

///*private:*///用全局类型要变公有
	int _year;
	int _month;
	int _day;
};

//运算符重载，运算符除了以下五个，其他都可以重载，(1).* 点星 (2). (3)sizeof  (4)::  (5)? : 三目
// 运算符重载函数一般有意义的才会重载，如日期类的日期-日期= 天数，而日期+日期是无意义的
//
////运算符重载,operator 关键字加运算符,要变公有才可写全局
//bool operator==(const Date d1, const Date d2)
//{
//	return d1._day == d2._day && d1._month == d2._month && d1._year == d2._year;
//}


int  main()
{
	Date d1(2002, 5, 13);
	Date d2(2001, 10, 13);
	Date d3(d2);//这里调用拷贝构造，是一个存在的对象去初始化创建另一个对象
	d3 = d2;//注意拷贝和赋值的区别，这里是两个已经存在的对象的赋值,这个叫做赋值重载或者复制拷贝
	if (d1 == d2)
	{
		cout << "==" << endl;
	}
	if (d1.operator==(d2/*,10*/))//编译器转化的调用方法，这是全局operator,运算符重载的函数参数是按要求固定的
	{
		cout << "==" << endl;
	}

	//把运算符重载定义到类中,两个都写会优先找局部类中的运算符重载函数
	//if (d1.operator==(d2))
	//{
	//	cout << "==" << endl;
	//}
	//if (d1 == d2)
	//{
	//	cout << "==" << endl;
	//}

	if (d1 < d2)
	{
		cout << "<" << endl;
	}
	if (d1.operator<(d2))				
										//带入补齐为if(d1.operator<(&d1,d2)),隐含的this
	{
		cout << "<" << endl;
	}
	
	return 0;
}


//赋值运算符函数，不写也会自己生成，注意只会生成赋值的这一个
//struct a
//{
//	int b;
//	int c;
//	double d;
//};
//
//int main()
//{
//	a p1 = { 10,20,2.3 };
//	a p2;
//	p2 = p1;
//	printf("%d %d %lf", p2.b, p2.c, p2.d);
//	return 0;
//}