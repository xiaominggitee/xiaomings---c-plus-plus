#pragma once

#include<iostream>
using std::cout;
using std::endl;
using std::cin;
#include <cassert>


//日期类
class Date
{
	friend std::ostream& operator<<(std::ostream& out, const Date& d);
	friend std::istream& operator>>(std::istream& in, Date& d);
public:

	// 获取某年某月的天数
	int Getmonthsday(int year, int month)
	{
		static int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
		{
			return 29;
		}
		else
		{
			return days[month];
		}
	}




	//explicit Date(int year = 1, int month = 1, int day = 1)                             explicit关键字可以取消编译器的隐式类型转换
	//{
	//		if (year > 0 && month > 0 && month <= 12 && day <= Getmonthsday() && day > 0)
	//	{

	//	}
	//	_year = year;
	//	_month = month;
	//	_day = day;
	//}

	// 全缺省的构造函数
	Date(int year = 1, int month = 1, int day = 1)
	{
		if (year > 0 && month > 0 && month <= 12 && day <= Getmonthsday(year, month) && day > 0)
		{
			_year = year;
			_month = month;
			_day = day;
		}
		else
		{
			cout << "Date::error" << endl;
			exit(-1);
		}
	}


	// 拷贝构造函数  d2(d1)
	Date(const Date& d)//拷贝构造的参数只有一个，并且是引用传参，否则会无穷递归，因为传值传参需要调用拷贝构造
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}



	// 赋值运算符重载
	// d2 = d3   变成    d2.operator=(&d2, d3)
	Date& operator=(const Date& d) //返回值是Date&，可以支持连续赋值，而且函数调用完成后对象没有销毁可以用引用
	{
		//d3赋值给d2，d3不被修改

		if (this != &d)//用地址判断，而不是用值判断，值判断是调用函数，且this可能为空（传进来就是空）
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}
		return *this;
	}



	// 析构函数
	~Date()
	{
		//析构函数是用来销毁空间的，如malloc的或者new等。普通变量无需销毁
		_year = 0;
		_month = 0;
		_day = 0;

		//可写可不写
	}

	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}

	// 日期+=天数
	Date& operator+=(int day) 
	{
		if (day < 0)
		{
			return *this -= (-day);
		}
		this->_day += day;
		while (_day > Getmonthsday(_year, _month))
		{
			_day -= Getmonthsday(_year, _month);
			_month++;
			if (_month > 12)
			{
				_month = 1;
				_year++;
			}
		}
		return *this;
	}



	// 日期+天数
	Date operator+(int day) const//加一个const是为了使const对象可以调用这些不会被修改内容的函数，const Date* const this。中间const是this自己的
	{
		if (day < 0)
		{
			return *this - (-day);
		}
		////拷贝构造一个ret
		//Date ret(*this);
		//ret._day += day;
		//while (ret._day > Getmonthsday(ret._year, ret._month))
		//{
		//	ret._day -= Getmonthsday(ret._year, ret._month);
		//	_month++;
		//	if (ret._month > 12)
		//	{
		//		ret._month = 1;
		//		ret._year++;
		//	}
		//}
		//return ret;//这个是函数体内的变量，故传值返回


		//复用+=，operator+复用operator+=比operator+=复用operator+好，因为+会调用多次拷贝构造，+=复用+电话，+=也变复杂了
		Date ret(*this);
		ret += day;		//operator+=
		return ret;

	}



	// 日期-天数
	Date operator-(int day) const
	{
		//if (day < 0)
		//	return *this += (-day);
		//Date ret(*this);
		//ret._day -= day;
		//while (ret._day < 0)
		//{
		//	ret._month--;
		//	if (ret._month < 1)
		//	{
		//		ret._month = 12;
		//		ret._year--;
		//	}
		//	ret._day += Getmonthsday(ret._year, ret._month);
		//}
		//return ret;

		Date ret(*this);
		ret -= day;
		return ret;
	}



	// 日期-=天数
	Date& operator-=(int day)
	{
		if (day < 0)
			return *this += (-day);
		_day -= day;
		while (_day < 0)
		{
			_month--;
			if (_month < 1)
			{
				_month = 12;
				_year--;
			}
			_day += Getmonthsday(_year, _month);
		}
		return *this;
	}



	// 前置++
	Date& operator++()
	{
		*this += 1;
		return *this;
	}



	// 后置++
	Date operator++(int)//编译器为了区分前置和后置++，把后置++添加一个int参数作为函数重载用来区分，而参数不使用的话可以只写类型
	{
		Date ret(*this);
		*this += 1;
		return ret;
	}

	// 前置--
	Date& operator--()
	{
		*this -= 1;
		return *this;
	}

	// 后置--
	Date operator--(int)
	{
		Date ret(*this);
		*this -= 1;
		return ret;
	}


	 //>运算符重载
	bool operator>(const Date& d) const
	{
		if (_year > d._year)
			return true;
		else if (_year == d._year && _month > d._month)
			return true;
		else if (_year == d._year && _month == d._month && _day > d._day)
			return true;
		else 
			return false;
	}



	// ==运算符重载
	bool operator==(const Date& d) const
	{
		return _day == d._day && _month == d._month && _year == d._year;
	}


	// >=运算符重载
	bool operator >= (const Date& d)  const
	{
		//写了两个之后直接复用即可
		return *this > d || *this == d;
	}



	// <运算符重载
	bool operator < (const Date& d) const
	{
		return !(*this >= d);
	}



	// <=运算符重载
	bool operator <= (const Date& d) const
	{
		return !(*this > d);
	}


	// !=运算符重载
	bool operator != (const Date& d) const
	{
		return !(*this == d);
	}

	// 日期-日期 返回天数
	int operator-(const Date& d) const
	{
		//直接相见考虑因素太多，进位且闰年，还需得到该月的天数
		//直接循环判断即可
		Date min(*this);
		Date max(d);
		if (min > max)
		{
			//交换
			min = d;
			max = *this;
		}
		int count = 0;
		while (max!=min)
		{
			min++;
			count++;
		}
		return count;
	}

	static int Getcount1()//静态成员函数一般是用来访问静态成员变量的，没有this指针，是属于整个类的
							//静态成员函数不能够访问其他成员变量，因为那些变量是属于那个对象的
	{
		return count1;
	}
private:
	int _year;
	int _month;
	int _day;
	static int count1;//静态的成员变量不会到初始化列表和构造函数
	static int count2;

};

//静态成员变量不属于任何一个对象，属于整个类，故定义需要单独定义，静态变量声明用即可。
//定义，这不是直接访问，只是定义

//static int Date::count2 = 0;对于类中的变量不能指定存储类型了，因为指定不明确
int Date::count1 = 0;



