#define _CRT_SECURE_NO_WARNINGS 1
#include "BSTree_KV.h"


//之前的复杂链表赋值，有random的那个链表，用KV模型也能实现了

void TestBSTree()
{
	key_value::BSTree<int,int> t;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	for (auto e : a)
	{
		t.InsertR(e,1);
	}
	t.InOrder();

}


//用于关联两个没有关系的对象，如中英文字典
void TestBSTree1()
{
	key_value::BSTree<string, string> ECDict;
	ECDict.InsertR("root", "根");
	ECDict.InsertR("string", "字符串");
	ECDict.InsertR("left", "左边");
	ECDict.InsertR("insert", "插入");
	//...
	string str;
	while (cin >> str)  //while (scanf() != EOF)
	{
		key_value::BSTreeNode<string, string>* ret = ECDict.FindR(str);
		//auto ret = ECDict.FindR(str);
		if (ret != nullptr)
		{
			cout << "对应的中文:" << ret->_value << endl;
			//ret->_key = "";
			ret->_value = "";
		}
		else
		{
			cout << "无此单词，请重新输入" << endl;
		}
	}
}

//或者用作计数
void TestBSTree2()
{
	string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
	// 水果出现的次数
	key_value::BSTree<string, int> countTree;
	for (const auto& str : arr)
	{
		auto ret = countTree.FindR(str);
		if (ret == nullptr)
		{
			countTree.InsertR(str, 1);
		}
		else
		{
			ret->_value++;  // 修改value
		}
	}

	countTree.InOrder();
}



//复杂链表的复制。没有map直接实现，时间较慢
class Node {
public:
	int val;
	Node* next;
	Node* random;

	Node(int _val) {
		val = _val;
		next = NULL;
		random = NULL;
	}
};

class Solution {
public:
	Node* copyRandomList(Node* head) {
		Node* cur = head;
		key_value::BSTree<Node*, Node*> bs;

		//入搜索二叉树
		while (cur != nullptr)
		{
			Node* copyNode = new Node(cur->val);
			bs.InsertR(cur, copyNode);
			cur = cur->next;
		}

		//遍历树的节点
		cur = head;
		while (cur)
		{
			key_value::BSTreeNode<Node*, Node*>* Bcur_next = bs.FindR(cur->next);
			key_value::BSTreeNode<Node*, Node*>* Bcur_random = bs.FindR(cur->random);
			key_value::BSTreeNode<Node*, Node*>* Bcopy = bs.FindR(cur);
			if(Bcur_next)
				Bcopy->_value->next = Bcur_next->_value;
			if(Bcur_random)
				Bcopy->_value->random = Bcur_random->_value;

			cur = cur->next;
		}
		return bs.FindR(head)->_value;


	}
};
int main()
{
	Node* n1 = new Node(7);
	Node* n2 = new Node(13);
	Node* n3 = new Node(11);
	Node* n4 = new Node(10);
	Node* n5 = new Node(1);

	n1->next = n2;
	n2->next = n3;
	n3->next = n4;
	n4->next = n5;

	n1->random = nullptr;
	n2->random = n1;
	n3->random = n5;
	n4->random = n3;
	n5->random = n1;
	
	Solution s;
	s.copyRandomList(n1);


	return 0;
}
