#define _CRT_SECURE_NO_WARNINGS 1

#include"test_list.h"
using namespace su;

void print(const list<int> lt)
{
	su::list<int>::const_iterator it = lt.begin();
	while (it != ++lt.begin())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}
int main()
{
	list<int> lt1;
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);
	su::list<int>::iterator it = lt1.begin();
	while (it != lt1.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;


	return 0;
}