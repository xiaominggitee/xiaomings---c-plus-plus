#pragma once
#include <iostream>
using namespace std;

//list的难点。迭代器的实现。包括其模板
namespace su {

	template <class T>
	struct list_Node
	{
		//定义节点
		list_Node(const T& val = T())
		{
			_data = val;
			_next = _prev = nullptr;
		}

		T _data;
		list_Node<T>* _next;
		list_Node<T>* _prev;
	};


	template<class T ,class Ref,class Ptr>//第一个模板参数T是传给Node用的，两边要一致的类型，不然两边的Node都不匹配了
	//template<class T ,class V>
	//template<class T>
	struct __list_iterator
	{
		typedef list_Node<T> Node;
		typedef __list_iterator<T> self;
		Node* _node;
		//注意这个传入的类型必须要相同，因为iterator是间接管理数据的，是封装的一个类模板，就算只是差const也是不同类型,就会有不同模板类

		__list_iterator(Node* node)//外面传进来一个该类型的节点，就直接创造空间即可，浅拷贝，直接赋值
			:_node(node)
		{}


		Ref operator*()
		{
			return _node->_data;
		}
		Ptr operator->()
		{
			return &(_node->_data);
		}

		self& operator++()
		{
			_node = _node->_next;
			return *this;
		}

		bool operator!=(const self& it) const 
		{
			return _node != it._node;
		}
	};

	template<class T>
	class list
	{
		typedef list_Node<T> Node;
	public:
		typedef __list_iterator<T,T&,T*> iterator;//第一个参数必须是T，因为得有个T来作为Node的参数，而list的Node要和iterator的Node要一致
		typedef __list_iterator<T,const T&,const T*> const_iterator;

		list()//空list
		{
			//带头双向循环链表，先定义哨兵
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;
		}

		iterator begin()
		{
			return iterator(_head->_next);
		}
		iterator end()
		{
			return iterator(_head);
		}
		const_iterator begin() const
		{
			return const_iterator(_head->_next);
		}
		const_iterator end() const
		{
			return const_iterator(_head);
		}

		void push_back(const T& val)
		{
			Node* newNode = new Node(val);
			Node* tail = _head->_prev;

			newNode->_next = _head;
			tail->_next = newNode;
			_head->_prev = newNode;
			newNode->_prev = tail;
		}

		void pop_back()
		{
			Node* pos = _head->_prev;
			Node* prev = pos->_prev;
			Node* next = pos->_next;

			prev->_next = next;
			next->_prev = prev;
			delete pos;
		}

		//然后直接实现迭代器，进行封装成为指针的样子
	private:
		Node* _head;

		//个别可以根据需要构建size参数
	};

}