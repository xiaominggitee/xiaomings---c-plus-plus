#pragma once
#include <iostream>
#include<list>
#include<cassert>
using namespace std;

//list的难点。迭代器的实现。包括其模板

namespace su {

	template <class T>
	struct list_Node
	{
		//定义节点
		list_Node(const T& val = T())
		{
			_data = val;
			_next = _prev = nullptr;
		}

		T _data;
		list_Node<T>* _next;
		list_Node<T>* _prev;
	};
	

	//template<class T ,class Ref,class Ptr>
	////template<class T ,class V>
	////template<class T>
	//struct __list_iterator 
	//{
	///*	typedef V* Ptr;
	//	typedef V& Ref;*/
	//	//typedef __list_iterator<T,V> self;//要实现const至少为两个
	//	typedef __list_iterator<T, T&, T*>             iterator;
	//
	//	typedef list_Node<T> Node;
	//	typedef __list_iterator<T,Ref,Ptr> self;//要实现const至少为两个

	//	Node* _node;
	//	//注意这个传入的类型必须要相同，因为iterator是间接管理数据的，是封装的一个类模板，就算只是差const也是不同类型,就会有不同模板类

	//	__list_iterator(Node* node)//外面传进来一个该类型的节点，就直接创造空间即可，浅拷贝，直接赋值
	//		:_node(node)
	//	{}


	//	//这里要写拷贝构造，但是浅拷贝就够了，因为对于类来说，const和非const模板传进来是不同类，可能用到隐式类型转换的拷贝构造
	//	//外面传进来一个该类型的节点，就直接创造空间即可，浅拷贝，直接赋值
	//	
	//	//__list_iterator(const self& it)//写成self依然无法支持隐式类型转换，因为传过来啥，生成的就是啥
	//	//	:_node(it._node)				//而写成普通iterator的目的就是为了要是传过来的是const，那就能隐式类型转换
	//	//{}

	//	__list_iterator(const iterator& it)
	//		:_node(it._node)
	//	{}

	//	Ref operator*()
	//	{
	//		return _node->_data;
	//	}

	//	Ptr operator->()
	//	{
	//		return &(_node->_data);
	//	}

	//	self& operator++()
	//	{
	//		_node =  _node->_next;
	//		return *this;
	//	}

	//	bool operator!=(const self& it)
	//	{
	//		return _node != it._node;
	//	}
	//};


	
	//库里面的版本
		//template<class T, class Ref, class Ptr>
		//struct __list_iterator {
		//	typedef __list_iterator<T, T&, T*>             iterator;
		//	typedef __list_iterator<T, const T&, const T*> const_iterator;
		//	typedef __list_iterator<T, Ref, Ptr>           self;
		//	typedef list_Node<T> Node;
		//	

		//	Node* node;

		//	__list_iterator(Node* x) : node(x) {}
		//	__list_iterator() {}
		//	__list_iterator(const iterator& x) : node(x.node) {}

		//	bool operator==(const self& x) const { return node == x.node; }
		//	bool operator!=(const self& x) const { return node != x.node; }
		//	Ref operator*() const { return (*node)._data; }


		//	Ptr operator->() const { return &(operator*()); }


		//	self& operator++() {
		//		node = (Node*)((*node)._next);
		//		return *this;
		//	}
		//	self operator++(int) {
		//		self tmp = *this;
		//		++* this;
		//		return tmp;
		//	}
		//	self& operator--() {
		//		node = (Node*)((*node)._prev);
		//		return *this;
		//	}
		//	self operator--(int) {
		//		self tmp = *this;
		//		--* this;
		//		return tmp;
		//	}
		//};
	
	//实现迭代器
	template<class T ,class Ref, class Ptr>
	struct __list_iterator
	{
		typedef __list_iterator<T, T&, T*> iterator;
		typedef list_Node<T> Node;
		typedef __list_iterator<T, Ref, Ptr> self;
		Node* _node;	//定义数据，迭代器是为了访问节点

		//构造
		__list_iterator(Node* node)
			:_node(node)
		{}

		//self的拷贝构造就够用，但是要支持非const隐式类型转换const，得有个针对普通迭代器拷贝构造
		__list_iterator(const iterator& it)
			:_node(it._node)
		{}

		Ref operator* ()
		{
			return _node->_data;
		}

		Ptr operator->() //返回的是节点的地址，隐藏了一个箭头，增加可读性
		{
			return &_node->_data;
		}

		self& operator++() //返回的是自己的引用
		{
			 _node = _node->_next;
			 return *this;
		}

		self operator++(int) //后置
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}

		//不等于
		bool operator!=(const self& it)
		{
			return _node != it._node;
		}
		bool operator==(const self& it)
		{
			return _node == it._node;
		}

		//减减
		self& operator--() //返回的是自己的引用
		{
			_node = _node->_prev;
			return *this;
		}

		self operator--(int) //后置
		{
			self tmp(*this);
			_node = _node->_prev;
			return tmp;
		}
	};

	template<class T>
	class list
	{
		typedef list_Node<T> Node;
	public:
		typedef __list_iterator<T, T&, T*> iterator;
		typedef __list_iterator<T, const T&, const T*> const_iterator;

		list()//空list 
		{
			//带头双向循环链表，先定义哨兵
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;
		}
		//拷贝构造
		list(const list<T>& lt)
		{
			empty_init();//调的这个构造，也得先创建空间
			//深拷贝
			list<T> tmp(lt.begin(), lt.end());
			swap(tmp);
		}
		
		void empty_init()
		{
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;
		}

		template <class InputIterator>
		list(InputIterator first, InputIterator last)
		{
			empty_init();//要是调的是这个构造，就_head就需要空的初始化

			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		list<T>& operator=(const list<T>& lt)
		{
			list<T> tmp(lt.begin(), lt.end());
			swap(tmp);
			return *this;
		}

		void swap(list<T>& lt)
		{
			std::swap(_head, lt._head);
		}

		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}
		void clear()
		{
			while (_head->_next != _head)
			{
				erase(begin());
			}
		}
		
		//然后直接实现迭代器，进行封装成为指针的样子
		iterator begin()
		{
			return iterator(_head->_next);
		}
		iterator end()
		{
			return iterator(_head);
		}

		const_iterator begin() const 
		{
			return const_iterator(_head->_next);
		}

		const_iterator end() const
		{
			return const_iterator(_head);
		}

		void push_back(const T& val)
		{
		/*	Node* newNode = new Node(val);
			Node* tail = _head->_prev;

			newNode->_next = _head;
			tail->_next = newNode;
			_head->_prev = newNode;
			newNode->_prev = tail;*/

			insert(end(), val);
		}
		void push_front(const T& val)
		{
			insert(begin(), val);
		}

		void pop_back()
		{
			//if (_head->_next != _head)
			//{
			//	Node* pos = _head->_prev;
			//	Node* prev = pos->_prev;
			//	Node* next = pos->_next;

			//	prev->_next = next;
			//	next->_prev = prev;
			//	delete pos;
			//}
			erase(--end());
		}
		void pop_front()
		{
			erase(begin());
		}

		bool empty() const 
		{
			return _head->_next == _head;
		}

		int size() const
		{
			//最好是多一个size参数，但是这里为了快速添加
			int size =0 ;
			Node* cur = _head->_next;
			while (cur!= _head)
			{
				size++;
				cur = cur->_next;
			}
			return size;
		}
		T& front()
		{
			assert(!empty());
			return _head->_next->_data;
		}

		iterator insert(iterator it , const T& val)//在it之前插入
		{

			//list不需要考虑扩容问题
			Node* cur = it._node;
			Node* prev = it._node->_prev;
			Node* newNode = new Node(val);

			newNode->_next = cur;
			cur->_prev = newNode;
			prev->_next = newNode;
			newNode->_prev = prev;

			return it;//it不会变化。故不会迭代器失效

		}

		iterator erase(iterator it)//删除it位置
		{
			assert(it._node);//不能为空，也不能删掉头
			if (it._node != _head)
			{
				Node* cur = it._node;
				Node* next = cur->_next;
				Node* prev = cur->_prev;

				prev->_next = next;
				next->_prev = prev;

				it._node = next;
			}

			return it;//返回it的下一个位置，因为原it已经被删除
		}
	private:
		Node* _head;

		//个别可以根据需要构建size参数
	};


}

