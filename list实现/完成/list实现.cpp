#define _CRT_SECURE_NO_WARNINGS 1
#include"lish实现.h"
#include"课程list.h"
using namespace su;

void test1()
{
	su::list<int> lt1;
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);
	su::list<int>::iterator it = lt1.begin();
	while (it != lt1.end())
	{
		*it -= 1;
		cout << *it << " ";
		++it;
	}
	cout << endl;

	su::list<int>::const_iterator cit = lt1.begin();
	while (cit != lt1.end())
	{
		//(*cit) += 1;
		cout << *cit << " ";
		++cit;
	}
	cout << endl;
}


void test2()
{
	su::list<int> lt1;
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);

	su::list<int>::iterator it = lt1.begin();
	while (it != lt1.end())
	{
		if (*it % 2 == 0)
		{
			lt1.insert(it, 20);
		}
		++it;
	}
	for (auto e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;

	it = lt1.begin();
	while (it != lt1.end())
	{
		if (*it % 2 == 0)
		{
			it = lt1.erase(it);//删了的话已经指向下一位了
		}
		else
		{
			++it;
		}
	}
	for (auto e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;
	lt1.pop_back();
	lt1.pop_back();
	lt1.pop_back();
	lt1.pop_back();
	for (auto e : lt1)
	{
		cout << e << " ";
	}
}

void test3()
{
	su::list<int> lt1;
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);
	for (auto e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;

	su::list<int> lt2(lt1);

	for (auto e : lt2)
	{
		cout << e << " ";
	}
	cout << endl;



	std::string s1("heellfasds");
	su::list<int> lt3(s1.begin(), s1.end());
	for (auto e : lt3)
	{
		cout << e << " ";
	}
	cout << endl;

}


struct AA
{
public:
	AA(int a1 = 0, int a2 = 0)
		:_a1(a1)
		,_a2(a2)
	{}
//private:
	int _a1;
	int _a2;

};
void test4()
{
	//指向的使用
	su::list<AA> lt1;
	lt1.push_back(AA(1, 2));
	lt1.push_back(AA(3, 4));
	lt1.push_back(AA(5, 6));
	lt1.push_back(AA(7, 8));

	su::list<AA>::iterator it = lt1.begin();
	while (it != lt1.end())
	{
		cout << it->_a1 << endl;//就是为了模拟指针访问，it就是模拟直接数据的地址，*it就是直接数据
		cout << (*it)._a2 << endl;//优先级除了括号就是.和->最快

		it++;
		
	}

}
int main()
{
	test4();

	return 0;
}


//#include<iostream>
//#include <windows.h>
//using namespace std;
//int main()
//{
//	cout << "jasdjkhf" ;
//	Sleep(2000);//毫秒单位
//
//	return 0;
//}