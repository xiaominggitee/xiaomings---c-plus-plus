#define _CRT_SECURE_NO_WARNINGS 1

//库里面单参数的reverse实现，兼容list和string，vector的ptr和Ref的方法，即迭代器萃取，原理是利用了模板的特化
//除了萃取这方法，实现三个参数的reverse_iterator，就能简单实现这一原理。
//或者就是将原生指针进行一层iterator的封装，operator*就是普通解引用，也能实现，这和萃取差不多，也是封装
template <class Iterator>
class reverse_iterator
{
protected:
	Iterator current;
public:
	typedef typename iterator_traits<Iterator>::iterator_category
		iterator_category;
	typedef typename iterator_traits<Iterator>::value_type
		value_type;
	typedef typename iterator_traits<Iterator>::difference_type
		difference_type;
	typedef typename iterator_traits<Iterator>::pointer
		pointer;
	typedef typename iterator_traits<Iterator>::reference
		reference;

	typedef Iterator iterator_type;
	typedef reverse_iterator<Iterator> self;
};

template <class Iterator>//这是有封装的迭代器，即list等
struct iterator_traits {
	typedef typename Iterator::iterator_category iterator_category;
	typedef typename Iterator::value_type        value_type;
	typedef typename Iterator::difference_type   difference_type;
	typedef typename Iterator::pointer           pointer;
	typedef typename Iterator::reference         reference;
};

template <class T>
struct iterator_traits<T*> {//这是原生指针的迭代器，利用了一层类模板的特化封装了两种不同的ptr和ref。
	typedef random_access_iterator_tag iterator_category;
	typedef T                          value_type;
	typedef ptrdiff_t                  difference_type;
	typedef T* pointer;
	typedef T& reference;
};
