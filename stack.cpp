#define _CRT_SECURE_NO_WARNINGS 1
#include <deque>
#include <stack> 
#include <iostream>
using namespace std;
//stack和queue在c++中都是用适配器实现的，适配deque这个双端队列实现


//deque是一个双端队列，拥有顺序表和链表的全部优点，但是对于（1）中间增删的数据效率较低，是用多个固定大小的连续空间实现的，再用一个指针数组管理这些空间。
//其支持随机访问是通过计算在哪个stuff，再模相对位置找到的。（2）故随机访问效率也比较低.所以一般是频繁首尾增删的如栈和队列，偶尔随机的用deque。
//毕竟中间增删也是挪动数据和随机访问需要计算的效率较慢。仅作为了解，深入学习在stl源码剖析有
namespace suzm
{

	template<class T,class container = deque<T>>
	class stack
	{
	public:
		stack()//不用写构造函数
		{}
		void push(const T& x)
		{
			st.push_back(x);
		}
		void pop()
		{
			st.pop_back();
		}

		T& top() 
		{
			return st.back();
		}
		const T& top() const 
		{
			return st.back();
		}

		int size() const 
		{
			return st.size();
		}
		bool empty() const 
		{
			return st.empty();
		}
		void swap(stack& st1) 
		{
			st.swap(st1.st);
		}
	private:
		container st;
	};


}

void test_stack1()
{
	suzm::stack<int> st;
	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);

	while (st.size())
	{
		cout << st.top() << " ";
		st.pop();
	}
	cout << endl;
	cout << st.empty() << endl;
}

void test_stack2()
{
	std::stack<int> st;
	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);
	std::stack<int> st2;
	st2.push(10);
	st2.push(20);
	st2.push(30);
	st2.push(40);

	//while (st.size())
	//{
	//	cout << st.top() << " ";
	//	st.pop();
	//}
	//cout << endl;
	//cout << st.empty() << endl;
	st.swap(st2);
}
//
//
//int main()
//{
//	test_stack1();
//	return 0;
//}