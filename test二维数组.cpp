#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
using namespace std;

//vector如何初始化

void print(int** a, int i, int j)
{
	for (int n = 0; n < i; n++)
	{
		for (int m = 0; m < j;m++)
		{
			cout << *(*(a+n)+m) << " ";
		}
		cout << endl;
	}
}
void test_1()
{
	//int i, j;
	//cin >> i >> j;
	//int a[i][j];
	//int b[i] = { 0 };
	//二维数组传参，一定要明确列是多大，不能是变量，数组初始化也不能是变量。
	//一些编译器无法支持变量定义数组，也只能是通过malloc或则new来创建空间并初始化了




	int** a;
	int i, j;
	cin >> i >> j;
	a = (int**)malloc(sizeof(int*) * i);
	for (int k = 0; k < i; k++)
	{
		a[k] = (int*)malloc(sizeof(int) * j);
	}
	for (int m = 0; m < i; m++)
	{
		for (int n = 0; n < j; n++)
		{
			a[m][n] = 1;
		}
	}
	print(a, i, j);
}

void print1(vector<vector<int>> a)
{
	for (int n = 0; n < a.size(); n++)
	{
		for (int m = 0; m < a[n].size(); m++)
		{
			cout << a[n][m] << " ";
		}
		cout << endl;
	}
}
void test_2()
{
	//vector的五个初始化方式，四个是构造
	vector<int> a = { 1,2,3,4 };//这是编译器支持
	vector<int> b(a.begin(), a.end());
	vector<int> c(10, 1);//10个1
	vector<int> d(c);
	vector<int> e;
	//空e之后用resize或者reserve开辟空间，resize可以指定参数，会改变size不会改变capacity


	//二维vector
	vector<vector<int>> aa;
	vector<vector<int>> bb = { {1,2,3,4},{1,2,3,4} };
	vector<vector<int>> cc(bb.begin(), bb.end());
	vector<vector<int>> dd(10, d);//10个vector<int>初始化,三维数组也可以用10个二维数组初始化
	vector<vector<int>> ee(dd);

	aa.resize(3);
	for (int i = 0; i < aa.size(); i++)
	{
		aa[i].resize(10, 1);
	}

	print1(aa);

}

int main()
{
	test_2();
	return 0;
}