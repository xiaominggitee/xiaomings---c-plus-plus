#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void test4()
{
	char s1[100] = { 0 };
	//gets_s(s1,13);//c11后正式删除gets因为较为危险，无论多大都要存，改为gets_s，需要指定字符个数才可，多了报错
	//printf("%s\n", s1);

	char s[100] = { 0 };
	scanf("%[^'\n']s", s);//格式控制【】获取指定字符，^异或，除了这个字符，scanf不会处理\n结束符，需要手动清缓冲区
	printf("%s\n", s);

	//gets_s(s1, 13);//c11后正式删除gets因为较为危险，无论多大都要存，改为gets_s，需要指定字符个数才可，多了报错
	//printf("%s\n", s1);

	char c;
	//c = getc(stdin);
	c = getchar();
	printf("%c\n", c);


	//char c;
	//c = getchar();
	//char s[100];
	//int i = 0;
	//while (c != '\n')
	//{
	//	s[i++] = c;
	//	c = getchar();
	//}
	//s[i] = '\0';
	//printf("%s", s);
}
int main()
{
	test4();

	return 0;
}