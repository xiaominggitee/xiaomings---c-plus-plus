#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#define MAXSIZE 100
using namespace std;//一个节点栈，一个字符栈

typedef struct BinaryTreeNode {                             //二叉树的二叉链表存储表示
	char data;
	struct BinaryTreeNode* lchild, * rchild;
}BTNode;

typedef struct {                  // 字符栈定义
	char* arr;
	int top;
	int stacksize;//容量
}SqStack;

typedef struct {                   //树栈定义
	BTNode** arr;
	int top;
	int stacksize;
}BTreeStack;


void  InitStack(SqStack& S) {             //字符栈初始化
	S.arr = new char[MAXSIZE];
	S.top = 0;
	if (!S.arr) exit(-1);
	S.stacksize = MAXSIZE;
}


void  InitBTreeStack(BTreeStack& S) {             //树栈初始化
	S.arr = new BTNode*[MAXSIZE];
	S.top = 0;
	if (!S.arr) exit(-1);
	S.stacksize = MAXSIZE;
}


void Pushchar(SqStack& S, char e)             //字符栈入栈
{
	if (S.top >= MAXSIZE)
		return;
	S.arr[S.top] = e;
	S.top++;
}
void PushTree(BTreeStack& S, BTNode* e)             //树栈入栈
{
	if(S.top>=MAXSIZE) return;
	S.arr[S.top] = e;//top指向顶元素的下一位
	S.top++;

}
void Popchar(SqStack& S, char& e)           //字符栈出栈
{
	if(S.top<=0) return;
	e = S.arr[S.top-1];
	S.top--;

}

void PopTree(BTreeStack& S, BTNode*& e)           //树栈出栈
{
	if (S.top <= 0) return;
	e = S.arr[S.top - 1];
	S.top--;
}


char GetTop(SqStack& S)                //字符栈得到栈顶函数
{
	if (S.top <=0)	exit(1);
	return S.arr[S.top - 1];
}


void CreateExpTree(BTNode*& T, BTNode* lchild, BTNode* rchild, char ch)
{
	T = new BTNode;
	T->data = ch;
	T->lchild = lchild;
	T->rchild = rchild;
}
char Precede(char t1, char t2)
{   
	/*char f=0;
	switch (t2)
	{
	case '+':if (t1 == '(' || t1 == '#')
		f = '<';
			else
		f = '>';
		break;
	case '-':if (t1 == '(' || t1 == '#')
		f = '<';
			else
		f = '>';
		break;
	case '*':if (t1 == '*' || t1 == '/' || t1 == ')')
		f = '>';
			else
		f = '<';
		break;
	case '/':if (t1 == '*' || t1 == '/' || t1 == ')')
		f = '>';
			else
		f = '<';
		break;
	case '(':if (t1 != ')')
		f = '<';
		break;
	case')':if (t1 == '(')
		f = '=';
		   else
		f = '>';
		break;
	case'#':if (t1 == '#')
		f = '=';
		   else
		f = '>';
	}*/
	//t1是栈顶，t2是ch
	if ((t1 == '#' && t2 == '#') || (t1 == '(' && t2 == ')'))
		return '=';
	else if (t1 == '(' || t1 == '#' || t2 == '(' || (t1
		== '+' || t1 == '-') && (t2 == '*' || t2 == '/')) {
		return '<';
	}
	else
		return '>';


	//return f;
}


int In(char c)//运算符判断
{ 
	switch (c)
	{
	case '+':
	case'-':
	case'*':
	case '/':
	case'#':
	case '(':
	case')':return 1; break;
	default:return 0;
	}
}
int GetValue(char theta, int a, int b)     //进行运算的函数
{
	int c = 0;
	switch (theta)
	{
	case '+':c = a + b; break;
	case'-':c = a - b; break;
	case'*':c = a * b; break;
	case'/':c = a / b; break;
	default:
		break;
	}
	return c;
}

//二叉树求值表达式
void CreateBTree(BTNode*& T)     
{
	SqStack OPTR;
	BTreeStack EXPT;
	char ch, theta, x;
	BTNode* a, *b;
	InitStack(OPTR); Pushchar(OPTR, '#');
	InitBTreeStack(EXPT); cin >> ch;
	while (ch != '#' || GetTop(OPTR) != '#') {
		if (!In(ch)) { CreateExpTree(T, NULL, NULL, ch); PushTree(EXPT, T); cin >> ch; }
		else
			switch (Precede(GetTop(OPTR), ch)) {
			case'<':
				Pushchar(OPTR, ch); cin >> ch;
				break;
			case'>':
				Popchar(OPTR, theta);
				PopTree(EXPT, b); 
				PopTree(EXPT, a);//拿出ab两子树

				CreateExpTree(T, a, b, theta);    
				PushTree(EXPT, T);
				break;
			case'=':
				Popchar(OPTR, x); cin >> ch;
				break;
			}
	}
}

int EvaluateExTree(BTNode* T) { //后序
	int lvalue, rvalue;
	lvalue = rvalue = 0;
	if (T->lchild == NULL && T->rchild == NULL)//末端数字节点
		return T->data - '0';			//化成数字
	else {
		lvalue = EvaluateExTree(T->lchild);
		rvalue = EvaluateExTree(T->rchild);
		return GetValue(T->data, lvalue, rvalue);    //最后计算自己
	}
}

int main()
{
	cout << "请输入算术表达式,并以#结束,中间计算过程要是个位数." << endl;
	BTNode* T;
	CreateBTree(T);          //创建表达式树
	cout << EvaluateExTree(T) << endl;            
}

