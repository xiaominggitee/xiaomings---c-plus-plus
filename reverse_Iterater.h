#pragma once 

//反迭代器实现，也是适配器原理
//因为其原理和迭代器相差不大，只是对应的迭代器实现的++,--的置换而已

template <class Iterator, class Ref, class Ptr>
//template <class Iterator>
class reverse_Iterator
{

	typedef reverse_Iterator<Iterator, Ref, Ptr> self;
	//typedef reverse_Iterator<Iterator> self;

	//typename关键字除了模板类型之外，其意义还有：告诉编译器我已知该类型是模板，还没实例化，但需要在该未实例化的类域中访问里面的内嵌类型
	//编译器应等待改模板实例化之后再去判别这个类型，这需要上层iterator，typedef出他的参数才能通过其类域传过来
	//而vector和string的迭代器是原生指针，没有封装，无法在里面定义其T&和T*,方法一：封装原生指针，方法二：萃取（后面知识，极少用到）

	//故为了方便可以直接引用三个参数，即把他的引用和指针都拿进来
	//typedef typename Iterator::Pointer Ptr;
	//typedef typename Iterator::Reference Ref;
public:
	explicit reverse_Iterator(Iterator it)
		:_it(it)
	{}
	reverse_Iterator() {}


	Ref operator*() const 
	{
		Iterator tmp = _it;
		return *(--tmp);
	}

	Ptr operator->() const 
	{
		return &(operator*());
	}

	self operator++() 
	{
		--_it;
		return *this;
	}

	self operator++(int) 
	{
		auto tmp = *this;
		_it--;
		return tmp;
	}

	self& operator--() 
	{
		++_it;
		return *this;
	}

	self operator--(int) 
	{
		auto tmp = *this;
		_it++;
		return tmp;
	}
	bool operator!=(const self& it) //用临时对象返回值接着比较的用法不怎么用
	{
		return _it != it._it;
	}

private:
	Iterator _it;
};