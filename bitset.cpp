#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <time.h>
using namespace std;


//实现位图，利用char，int等数组通过位运算实现。
//位图的优势，快，节省空间
//位图的缺点，只能针对整型，因为只有整型才可以进行模除运算，这是哈希的一种特殊形式，1：1映射

namespace su {

class bitset//库里面的bitset是用非类型模板参数实现的，而不是带参数进去
{
public:
	bitset(int count)
		:_bit((count/8)+1)//如果用的是整型，那么就/32,即左移五位
		,_count(count)
	{}
	//无需析构，默认生成的会自己调用vector的析构
	//拷贝构造
	bitset(const bitset& bit)
	{
		vector<char> temp(bit._bit);
		_bit = temp;
		_count = bit._count;
	}

	bitset& operator=(const bitset bit)
	{
		_bit = bit._bit;
		_count = bit._count;
		return *this;
	}

	//实现变0，变1，和判断
	bool set(size_t x)
	{
		//判断是个char，再判断在这个char的哪一位
		if (x > _count)
			return false;
		int i = 0, j = 0;
		i = x / 8;
		j = x % 8;

		_bit[i] |= (1 << j);
		return true;

	}
	bool reset(size_t x)
	{
		if (x > _count)
			return false;
		int i = 0, j = 0;
		i = x / 8;
		j = x % 8;

		_bit[i] &=(~(1 << j));
		return true;
	}
	bool test(size_t x)
	{
		if (x > _count)
			return false;
		int i = 0, j = 0;
		i = x / 8;
		j = x % 8;

		return _bit[i] & (1 << j);
	}
private:
	vector<char> _bit;
	int _count;//位图总大小
	
};

}
void test_bit_set1()
{
	su::bitset bs(100);
	bs.set(18);
	bs.set(19);

	cout << bs.test(18) << endl;
	cout << bs.test(19) << endl;
	cout << bs.test(20) << endl;
	for (int i = 0; i < 100; i++)
		cout << bs.test(i);
	cout << endl;
	bs.reset(18);
	bs.reset(18);

	cout << bs.test(18) << endl;
	cout << bs.test(19) << endl;
	cout << bs.test(20) << endl;
	for (int i = 0; i < 100; i++)
		cout << bs.test(i);
	cout << endl;



	//su::bitset bigBS(bs);
	su::bitset bigBS(10);
	bigBS = bs;
	for (int i = 0; i < 100; i++)
		cout << bigBS.test(i);
	cout << endl;

}

namespace su {
	class two_bitset
	{
		//用两个位图即可实现计数为4种情况的可能。
	public:
		two_bitset(size_t count)
			:_bit1(count)
			,_bit2(count)
		{}
		//如下面实现，判断只出现一次的数字？
		bool set(size_t x)
		{
			if (_bit1.test(x) == 0 && _bit2.test(x) == 0)
			{
				_bit2.set(x);
			}
			else if (_bit1.test(x) == 0 && _bit2.test(x) == 1)//只用00，01，11，11之后不用判断
			{
				_bit1.set(x);
			}
			else
				return true;

		}
		bool judge(size_t x)
		{
			if (_bit1.test(x) == 0 && _bit2.test(x) == 0)
			{
				return false;//判断只出现一次的
			}
			else if (_bit1.test(x) == 0 && _bit2.test(x) == 1)//只用00，01，11，11之后不用判断
			{
				return true;
			}
			else
				return false;
		}
	private:
		bitset _bit1;
		bitset _bit2;
	};
}


void test_bit_set2()
{
	su::two_bitset bit(100);
	srand((unsigned long)time(0));
	for (int i = 0; i < 10; i++)
	{
		size_t x = rand() % 100;
		cout << x << " ";
		bit.set(x);
	}
	cout << endl;

	for (int i = 0; i < 100; i++)
	{
		cout<<bit.judge(i);
	}
	cout << endl;



}
int main()
{

	test_bit_set2();
	return 0;
}