#pragma once
#include <string.h>
#include <cassert>
#include <iostream>
#include <string>
using std::string;
using std::cout;
using std::endl;
using std::cin;
//string的实现
//1.简单资源管理。2.增删查改 3.个别特殊函数

//版本一,简单实现string，解释深浅拷贝问题，传统写法
namespace su
{
	class string
	{
		friend std::ostream& operator<< (std::ostream& out, const string& s);

	public:
		string(const char* str = " ")
			:_str(new char[strlen(str)+1])
		{
			strcpy(_str, str);
		}
		~string()
		{
			//std::cout << "~string" << std::endl;
			if (_str)
			{
				delete[] _str;
				_str = nullptr;
			}
		}

		//拷贝构造
		string(const string& s)
			:_str(new char[strlen(s._str)+1])
		{
			strcpy(_str, s._str);//深拷贝，创建了空间
		}
		char* c_str()
		{
			return _str;
		}

		int size()
		{
			return strlen(_str);//不包含\0
		}

		char& operator[](size_t pos)
		{
			assert(pos < strlen(_str));
			return _str[pos];
		}

		string& operator=(const string& s)
		{
			if (this != &s) //相等会造成多次析构
			{
				//考虑的有很多，1.创建空间，拷贝数据，交换指针，销毁其他空间
				char* tmp = new char[strlen(s._str) + 1];
				strcpy(tmp, s._str);

				delete[] _str;
				_str = tmp;
			}
			return *this;//简单版本实现解释深浅拷贝
		}

	private:
		char* _str;
	};

}

namespace suzm
{
	// string需要考虑完善的增删查改和使用的string
	class string
	{
	public:
		typedef char* iterator;;
		typedef const char* const_iterator;

		//构造，实现三种，空，常字符串，以及string
		string(const char* s = " ")//缺省就无法引用
			//:_str(new char[strlen(s) + 1])
			//,_size(strlen(s))//如果像这样初始化的话，就要多次调用strlen了，可以先初始话size 和 capacity之后在复用即可
			:_size(strlen(s))
			, _capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, s);
		}
		string(const string& s)
			:_size(s._size)
			,_capacity(s._capacity)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, s._str);
		}
		//析构
		~string()
		{
			if (_str)
			{
				delete[] _str;
				_str = nullptr;
				_size = _capacity = 0;
			}
		}
		//尾插入，三种，append，+=，push_back
		string& append(const char* s)
		{
			//判断空间大小
			size_t len = strlen(s) + _size;
			if (len > _capacity)
			{
				//c++没有实现像realloc的扩容方式，只能硬阔，但是库的c++有reserve，可以复用
				char* tmp = new char[len + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = len;

			}

			//尾插字符串
			strcpy(_str + _size, s);
			_size = len;
			return *this;
		}


		//size，capacity
		size_t size() const 
		{
			return _size;
		}
		size_t capacity() const 
		{
			return _capacity;
		}
		string& push_back(char ch)
		{
			//判断空间大小
			if (_size == _capacity)
			{
				char* tmp = new char[_capacity*2+1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity *= 2;
			}

			//插入
			_str[_size++] = ch;
			return *this;
			
		}
		string& operator+=(const char* s)
		{
			return append(s);
		}

		string& operator+=(char ch)
		{
			return push_back(ch);
		}


		//任意位置插入，insert
		string& insert(size_t pos,char ch )
		{
			assert(pos <= _size);
			//判断容量
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : 2 * _capacity+1);
			}
			size_t begin = pos;
			size_t end = _size+1;
			//挪动
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				end--;
			}
			_str[pos] = ch;
			_size++;
			return *this;
		}

		string& insert(size_t pos,const char* s)
		{
			assert(pos <= _size);

			//移动len位置
			size_t len = strlen(s);
			if (len == 0)
				return *this;//插入0个，直接返回
			if (len+_size > _capacity)
			{
				reserve(len+_size + 1);
			}
			//挪动
			size_t begin = pos;
			size_t end = _size + len;
			while (end - len+1> pos)
			{
				_str[end] = _str[end - len];
				end--;
			}
			//插入
			for (size_t i = pos; i <pos+len; i++)
			{
				_str[i] = s[i - pos];
			}
			_size += len;
			return *this;
		}

		string& insert(size_t pos,const string& s)
		{
			
			assert(pos <= _size);

			//移动len位置
			size_t len = s.size();;
			if (len == 0)
				return *this;//插入0个，直接返回
			if (len + _size > _capacity)
			{
				reserve(len + _size + 1);
			}
			//挪动
			size_t begin = pos;
			size_t end = _size + len;
			while (end - len + 1 > pos)
			{
				_str[end] = _str[end - len];
				end--;
			}
			//插入
			for (size_t i = pos; i < pos + len; i++)
			{
				_str[i] = s.c_str()[i - pos];
			}
			_size += len;
			return *this;

		}


		//转换c的地址c_str
		const char* c_str() const 
		{
			return _str;
		}

		//[],赋值
		char& operator[](size_t pos)
		{
			assert(pos < _size);//不给访问\0
			return *(_str + pos);
		}

		void swap(string& tmp)
		{
			std::swap(_str, tmp._str);
			std::swap(_size, tmp._size);
			std::swap(_capacity, tmp._capacity);//这三可以直接自己写一个swap，就不需调用三次这个库里面的函数
		}
		string& operator=(const string& s)
		{
			if (this != &s)
			{
				//现代写法，直接swap
				//std::swap(_str, tmp._str);
				//std::swap(_size, tmp._size);
				//std::swap(_capacity, tmp._capacity);//这三可以直接自己写一个swap，就不需调用三次这个库里面的函数

				//tmp调用完会析构。



				string tmp(s);
				return *this;
			}
		}
		string& operator=(string s)
		{
			//现代写法，直接swap，更省事的写法
			//这样传参就不需要判断是否一样因为是拷贝，必定不一样，故不会导致两次析构，和一起被修改

			swap(s);
			return *this;
		}


		//扩容，reserve，resize
		void reserve(size_t n)
		{	
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}
		void resize(size_t n,char ch = '\0')
		{
			////容量
			//if (n > _capacity)
			//{
			//	char* tmp = new char[n + 1];
			//	strcpy(tmp, _str);
			//	delete[] _str;
			//	_str = tmp;
			//	_capacity = n;
			//	for (size_t i = _size; i < _capacity; i++)
			//	{
			//		_str[i] = ch;
			//	}
			//	_str[_capacity] = '\0';
			//	_size = _capacity;
			//}
			//else if (n <= _size)//不扩容缩字符
			//{
			//	_str[n] = '\0';
			//	_size = n;
			//}
			//else//不扩容加字符
			//{
			//	size_t i = 0;
			//	for (i = _size; i < n; i++)
			//	{
			//		_str[i] = ch;
			//	}
			//	_str[i] = '\0';
			//	_size = n;
			//}


			if (n < _size)//不扩容缩字符
			{
				_str[n] = '\0';
				_size = n;
			}
			else
			{
				if (n > _capacity)
				{
					reserve(n);
				}

		
				for (size_t i = _size; i < n; i++)
				{
					_str[i] = ch;
				}
				_str[n] = '\0';
				_size = n;
			}
			

		}



		//任意位置删除，earse
		string& earse(size_t pos = 0, size_t len = npos)
		{

			assert(pos <= _size);
			//删多删少问题，删多就是全部删除
			if (len == npos || len + pos > _size)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				//挪动
				size_t begin = pos+len;
				while (begin <= _size)
				{
					_str[begin - len] = _str[begin];
					begin++;
				}
				_size = _size - len;
			}
			return *this;
		}




		//查找，find，用获取substr实现，找串，以及字符，找不到返回啥
		size_t find(char ch, size_t pos =0)
		{
			for (size_t i = pos; i < _size; i++)
			{
				if (_str[i] == ch)
					return i;
			}
			
			return npos;
		}
		size_t find(const char* s, size_t pos = 0)
		{
			const char* p = strstr(_str, s);
			if (p == nullptr)
				return npos;
			else
				return p - _str;
		}



		//两种迭代器，反向暂时实现
		iterator begin() 
		{
			return _str;
		}
		iterator end()//有效字符的下一位
		{
			return _str + _size;
		}

		const_iterator begin() const 
		{
			return _str;
		}
		const_iterator end() const //有效字符的下一位
		{
			return _str + _size;
		}

		void clear()
		{
			_str[0] = '\0';
			_size = 0;
		}
		
		const static size_t npos;
	private:
		char* _str;
		size_t _size;
		size_t _capacity;//都是有效数据的容量和大小
	};
	const size_t string::npos = -1;//有一个npos的静态变量，是整数的最大值
	
	bool operator<(const string& s1, const string& s2)
	{
		//实现strcmp，，然后直接复用
		const char* str1 = s1.c_str();
		const char* str2 = s2.c_str();

		//while (*str1 == '\0' || *str2 == '\0') 
		//{
		//	if (*str1 > *str2)
		//		return 1;
		//	else if (*str1 < *str2)
		//		return -1;
		//	else
		//	{
		//		str1++;
		//		str2++;
		//	}
		//}

		//if (*str1 == *str2 == '\0')
		//	return 0;
		//else
		//	return *str1 > *str2 ? 1 : -1;
		int ret = 0;
		while ((ret = *str1 - *str2) == 0 && *str1)
		{
			str1++;
			str2++;
		}
		return (((-ret) < 0) - (ret < 0))<0;
	}

	bool operator==(const string& s1, const string& s2)
	{
		const char* str1 = s1.c_str();
		const char* str2 = s2.c_str();
		int ret = 0;
		while ((ret = *str1 - *str2) == 0 && *str1)
		{
			str1++;
			str2++;
		}
		return (((-ret) < 0) - (ret < 0)) == 0;
	}

	bool operator<=(const string& s1, const string& s2)
	{
		return s1 < s2 || s1 == s2;
	}


	bool operator>(const string& s1, const string& s2)
	{
		return !(s1 <= s2);
	}


	bool operator>=(const string& s1, const string& s2)
	{
		return !(s1 < s2);
	}


	bool operator!=(const string& s1, const string& s2)
	{
		return !(s1 == s2);
	}


	//流提取 >> , 流插入 << ，流提取的优化
	std::ostream& operator<< (std::ostream& out, const string& s)
	{
		//out << s.c_str() << endl;//这个写法虽然可以，但是不够完善，因为要是后面插入了很多的\0，那么\0占用的 空间就没有打印出来


		//正确写法应该是迭代器，或则范围for因为他的底层都是迭代器，按照size弄出来的
		for (auto ch : s)
		{
			out << ch;
		}
		return out;
	}


	std::istream& operator<< (std::istream& in, string& s)
	{
		//为了减少扩容的代价，可以全部先拷贝到一个数组中，在一同拷贝过去

		s.clear();
		char ch;
		ch = in.get();//对于scanf以及cin等函数或对象来说，都是以空格或者换行来获取字符的，in对象里面有个get函数是以行为结束单位的
		char buff[128] = { '\0' };
		size_t i = 0;
		while (ch != ' ' && ch != '\n')
		{
			buff[i++] = ch;
			if (i == 127)
			{
				s += buff;
				memset(buff, '\0', 128);//加满后拷贝上去，重新开始。
				i = 0;
			}

			ch = in.get();
		}

		s += buff;
		return in;

	}

}