#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

void test1()
{
	//遇到某个值停止,【】控制输入字符

	int b = 0;
	char a[100] = { 0 };
	/*scanf("%[^5]", &b);*///这样的格式控制是字符，输入的也按照字符算,遇到5停止
	scanf("%[^\n]", a);
	printf("%s", a);
}

int main()
{
	//[]里面的允许被输入，遇到第一个不是的就停止
	char a[100] = { 0 };
	//scanf("%[a-z A-Z]", a);
	//scanf("%[a-z-A-Z]", a);//里面包含-字符

	scanf("%[a-zA-Z]", a);//不空开也可以
	printf(a);
	return 0;
}
