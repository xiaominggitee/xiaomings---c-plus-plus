#define _CRT_SECURE_NO_WARNINGS 1


#include"levelOrder_ASL.h"
//c调用cpp，在cpp头文件中告诉cpp编译器用c的方式去编译，但是头文件展开原因，c不认识extern“c”，故用条件编译__cplusplus

extern "C"//cpp调用c，告诉cpp编译器用c的方式去寻找链接
{
	#include "..\..\college cpp\Queue_C_lib\queue.h"
}


//这是以前实现的层序遍历的程序
//层序遍历
void BinaryTreeLevelOrder(BTNode* root)
{
	Queue s;
	QueueInit(&s);//创建并初始化队列

	if (root)
		QueuePush(&s, root);
	while (!QueueEmpty(&s))
	{
		BTNode* temp = QueueFront(&s);//这里拿出来的是QNode的值，也就是BTNode*的节点 
		QueuePop(&s);

		printf("%d ", temp->data);
		if (temp->left)
		{
			QueuePush(&s, temp->left);
		}
		if (temp->right)
			QueuePush(&s, temp->right);
	}
}


//这是现在能够区分每层元素的层序遍历,并计算asl
//利用层序遍历计算二叉树的asl，平均查找长度
void levelOrder(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	QueuePush(&q, root);
	int level = 1;
	int levelsum = 0;
	int sum = 0;
	int count = 1;//第一层的元素是1
	double total = 0;
	while (!QueueEmpty(&q))
	{
		for (int i = 0; i < count; i++)//每一次循环把一层的元素弄完
		{
			Qdatatype temp = QueueFront(&q);
			QueuePop(&q);

			if (temp->left)
			{
				QueuePush(&q, temp->left);
				levelsum++;
			}
			if (temp->right)
			{
				QueuePush(&q, temp->right);
				levelsum++;
			}
			sum += level;
			total++;
			printf("%d ",temp->data);
		}
		//进入下一层
		count = levelsum;//更新下一层的元素个数,total 是一层的元素
		levelsum = 0;
		level++;
	}
	std::cout << sum / total << endl;
	QueueDestroy(&q);
}

int main()
{
	char a[] = "123##4##67##8##";
	int i = 0;
	BTNode* root = BinaryTreeCreate(a, sizeof(a) / sizeof(a[0]) - 1, &i);
	printf("\n");

	printf("层序：");
	levelOrder(root);
	return 0;
}