#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

//哈夫曼结构体
typedef struct HuffmanTreeNode
{
	int weight;
	char letter;
	int parent, lchild, rchild;//用下标记录表示左右孩子父亲节点，下标直接访问
}HTNode;
//typedef char** HuffmanCode;//二级指针就是可以表示指针数组了，字符数组用一级指针即可,编码直接输出，不许保存

//选两最小值,引用返回
void Select(HTNode*& HT, int n, int& s1, int& s2)
{
	int min1, min2;
	min1 = min2 = INT_MAX;//初始化最大值找最小值
	for (int i = 1; i <= n; i++)
	{
		if (HT[i].weight < min1&&HT[i].parent ==0)//没双亲节点的最小两个
		{
			min1 = HT[i].weight;
			s1 = i;//默认s1为最小
		}
	}
	for (int i = 1; i <= n; i++)
	{
		if (HT[i].weight < min2&&i!=s1&& HT[i].parent == 0)
		{
			min2 = HT[i].weight;
			s2 = i;
		}
	}
}

void CreateHuffmanTree(HTNode* &HT,int n)
{
	if (n <= 1)
		return;
	//创建并初始化哈夫曼树
	int m = 2 * n - 1;//完全二叉树，最后的哈夫曼树的节点数量
	HT = new HTNode[m+1];

	cout << "输入各个节点的字符集\n";
	for (int i = 1; i <= n; i++)
		cin >> HT[i].letter;
	cout << "输入各个节点的权值:\n";
	for (int i = 1; i <= n; i++)
	{
		cin >> HT[i].weight;//从左到右
	}
	for (int i = 0; i <= m; i++)
	{
		HT[i].parent = HT[i].rchild = HT[i].lchild = 0;//先初始化为0
	}

	//开始构建哈夫曼树
	for (int i = n + 1; i <= m; i++)//从第n+1个数开始作为父节点，创建树
	{
		//挑选两个最小的
		int s1, s2;
		Select(HT, i - 1, s1, s2);
		HT[s1].parent = i;
		HT[s2].parent = i;//设立孩子父亲

		HT[i].lchild = s1;
		HT[i].rchild = s2;
		HT[i].weight = HT[s1].weight + HT[s2].weight;
	}
	//初识化完成
}

//哈夫曼编码
void CreateHuffmanCode(HTNode*& HT, int n)
{
	//从前往后构建
	for (int i = 1; i <= n; i++)
	{
		int c = i;
		int f = HT[c].parent;//f默认为c的父亲，判断c是左右孩子节点的哪一个
		cout << HT[i].letter << "的编码是:";
		while (f != 0)//链表头初始化就是0，到第一个就结束
		{
			if (HT[f].lchild == c)
				cout << '0';			//左0右1
			else
				cout << '1';
			c = f;
			f = HT[c].parent;//向上回溯
		}
		cout << endl;

	}

}

int main()
{
	int n;
	cout << "输入叶子节点的个数\n";
	cin >> n;
	HTNode* HT;
	CreateHuffmanTree(HT,n);
	CreateHuffmanCode(HT, n);
	return 0;
}