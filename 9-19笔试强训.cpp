#define _CRT_SECURE_NO_WARNINGS 1

//返回距离最近的斐波那契数的步数，每步只能走一步
#include <iostream>
using namespace std;

//int main()
//{
//    int n;
//    int a = 0, b = 1;
//    int tmp = 0;
//    cin >> n;
//    while (1)
//    {
//        if (b >= n)
//            break;
//        tmp = a + b;
//        a = b;
//        b = tmp;
//    }
//    if (n - a > b - n)
//        cout << b - n << endl;
//    else
//        cout << n - a << endl;
//
//    return 0;
//}
#include <stack>
using namespace std;
//int main()
//{
//    stack<int> st;
//
//    return 0;
//}



//错误代码，没有考虑连续的比较问题
//bool chkParenthesis(string A, int n) {
//    // write code here
//    if (n == 0)
//        return true;
//    stack<int> st;
//    int i = 0;
//    char tmp = 0;
//    while (i<n)
//    {
//        if ( A[i] != '(' && A[i] != ')' )
//            return false;
//        if(!st.empty())
//        {
//            if (st.top() == '(' && A[i] == ')')
//            {
//                st.pop();
//                i++;
//            }
//
//        }
//       if(i<n)
//             st.push(A[i++]);
//    }
//    return st.empty();
//}
//int main()
//{
//    int a = chkParenthesis("()()(((())))", 12);//对于这个连续的括号会报错
//    cout << a << endl;
//    return 0;
//}

class Parenthesis {
public:
    bool chkParenthesis(string A, int n) {
        // write code here
        if (n == 0)
            return true;
        stack<int> st;
        int i = 0;
        char tmp = 0;
        while (i < n)
        {
            if (A[i] != '(' && A[i] != ')')
                return false;
            if (!st.empty())
            {
                if (st.top() == '(' && A[i] == ')')
                {
                    st.pop();
                    i++;
                    continue;//弹出之后不入栈，继续拿下一个比较
                }
            }
            st.push(A[i++]);//不匹配和为空可以push

        }
        return st.empty();
    }
};